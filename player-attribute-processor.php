<?php
require "vendor/autoload.php";

use PHPHtmlParser\Dom;

$time_start = microtime(true); 

$cricInfoLink = 'table.infobox a.external';
//loop through players
$entries = RawStreamQuery::create()->find();
$teamQuery = TeamsQuery::create();
foreach($entries as $key=>$entry) {
    echo "Processing ". ($key +1) ." of ".count($entries) . "\n";

    if($entry->getIsProcessed()) {
        continue;
    }

    if($entry->getIsStatsProcessed()) {
        //stat has been processed
        //clear the stats and process row
        RawStreamBattingStatsQuery::create()
            -> filterByPlayerId($entry->getId())
            -> delete();


        RawStreamBowlingStatsQuery::create()
            -> filterByPlayerId($entry->getId())
            -> delete();
    }
    // Querying the page;
    $data = UrlFetcher::LoadUrl($entry->getLink());

    //capture canonical link
    preg_match('/<link rel="canonical" href="(.+)"\/>/i', $data, $matches);
    echo "Canonical Link is " .$matches[1] . "\n";
    $entry->setLink($matches[1]);
    $entry->save();

    $team = $teamQuery->findPK($entry->getTeam());

    //Looking for cickinfo Link
    $dom = new Dom();
    $dom->load($data);
    $linkObjects = $dom->find($cricInfoLink);
    $linkObject = $linkObjects[count($linkObjects) - 1];
    $isDataCollected = false;
    if(!$linkObject) {
        echo "Link Object can't be found! \n";
    } else {
        $link = $linkObject->getAttribute("href");
        echo "CrickInfoLink is " . $link. "\n";
        //verify the link
        $cricInfo = '/https?:\/\/(.*.)?(espn)?cricinfo.*\/player\/(\d+)\.html/';
        if(preg_match($cricInfo, $link)){
            //extract player id
            preg_match('/\/(\d+)\.html/', $link, $idMatches);
            $crickInfoLink ='http://www.espncricinfo.com/ci/content/player/'.$idMatches[1].'.html';
            CricInfoParser::updatePlayerStats($crickInfoLink, $entry, $team->getTeamType(), true);
            $isDataCollected = true;
        } else {
            //data source is likely diffrent and look for cricinfo link through all the external links
            foreach($dom->find('a.external') as $externalLink) {
                $link = $externalLink->getAttribute("href");
                if(preg_match($cricInfo, $link)) {
                    preg_match('/\/(\d+)\.html/', $link, $idMatches);
                    $crickInfoLink ='http://www.espncricinfo.com/ci/content/player/'.$idMatches[1].'.html';
                    CricInfoParser::updatePlayerStats($crickInfoLink, $entry, $team->getTeamType(), true);
                    $isDataCollected = true;
                    break;
                }
            }
        }
    }

    if(!$isDataCollected) {
        WikipediaStatsParser::parserPlayerStats($dom, $entry);
    }
}
$time_end = microtime(true);

$execution_time = ($time_end - $time_start)/60;
echo "Total Execution Time: ".$execution_time." Mins \n";
?>