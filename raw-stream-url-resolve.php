<?php
include('database.php');
$wikipediaBase = "https://en.wikipedia.org";
// Create connection
$conn = new mysqli($database['host'], $database['user'], $database['password'], $database['database']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM raw_stream";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["link"]. "\n";
        //fetch the page
        $data = file_get_contents($wikipediaBase . $row["link"]);
        preg_match_all('/<link rel="canonical" href="(.+)"\/>/i', $data, $m);
        echo $m[1][0] . "\n";

        $sqlUpdate="UPDATE raw_stream SET link='".$m[1][0]."' WHERE id=".$row["id"]."";
        $update = $conn->query($sqlUpdate);

    }
} else {
    echo "0 results";
}
$conn->close();
 ?>
