<?php

require "vendor/autoload.php";
$exported_version = '1.0.0';
$conn =  \Propel\Runtime\Propel::getConnection();
$exportType = count($argv) > 1? $argv[1] : 'teams';
  /////////////////////////////////////////////////////////
 ///////////////////////  CLI SWITCH /////////////////////
/////////////////////////////////////////////////////////
switch($exportType) {
    case 'teams':
        exportTeams($conn, $exported_version);
        break;
    case 'players':
        exportPlayers($conn, $exported_version);
        break;
    default:
        echo "Invalid parameter!!\n";
        break;
}




///////////////////////////////////
function exportTeams($conn, $exported_version) {
    $sql = "SELECT `name`, `short_name` as `abbreviation`, `id`, `logo`, `country`, `batting`, `bowling`, `fielding`, `is_international` as `isInternational`, `league`, `stadium_id` as `stadiumId` FROM `".\Map\TeamsTableMap::TABLE_NAME."`;";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $export = new stdClass;
    $export->version = $exported_version;
    $export->teams = [];
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $team)  {
        $teamObj = (object)$team;
        $teamObj->players = getTeamPlayers($teamObj->id, $conn);
        $teamObj->jersey = getTeamJersey($teamObj->id, $conn);
        $teamObj->isInternational = ($teamObj->isInternational) ? true : false;
        // print_r($teamObj->players);
        array_push($export->teams, $teamObj);
    }
    writeFile(json_encode($export), 'teams.json');
}
function exportPlayers($conn, $exported_version) {
    $sql = "SELECT * FROM `".\Map\PlayersTableMap::TABLE_NAME."`;";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $export = new stdClass;
    $primaryRoleName = "";
    $export->players = [];
    $export->version = $exported_version;
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $player)  {
        unset($player['sourceUrl']);
        $playerObj = (object)$player;
        $primaryRoleName = $playerObj->primaryRole;
        $playerObj->primaryRole = getPrimaryRole($playerObj->primaryRole);
        $playerObj->jerseyName = trim($playerObj->jerseyName);
        $playerObj->battingAttributes = getBatting($playerObj->id, $conn); 
        $playerObj->bowlingAttributes = getBowling($playerObj->id, $conn); 
        $playerObj->appearance = getPlayerAppearance($playerObj->id, $conn);
        $playerObj->accessories = getPlayerAccessories($primaryRoleName, $playerObj->transferValue);
        array_push($export->players, $playerObj);
    }
    writeFile(json_encode($export), 'players.json');
}
function getPrimaryRole($role) {
    $index = 0;
    switch($role) {
        case "batsmen":
            $index = 0;
        break;
        case "bowler":
            $index = 1;
        break;
        case "allRounder":
            $index = 2;
        break;
        case "keeper":
            $index = 3;
        break;
    }
    return $index;
}
function getPlayerAccessories($primaryRole, $transferValue) {
    $acceories = new stdClass;
    $accessoryMean = (int)(($transferValue / 100) * 100);
    switch($primaryRole) {
        case "batsmen":
        case "allRounder":
        case "keeper":
            $acceories->batLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));
            $acceories->helmetLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));;
            $acceories->glovesLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));;
            $acceories->padLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));;
        break;
        case "bowler":
            $acceories->batLevel = rand(1, 5);
            $acceories->helmetLevel = rand(1, 5);
            $acceories->glovesLevel = rand(1, 5);
            $acceories->padLevel = rand(1, 5);
        break;
    }
    
    $acceories->shoesLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));;
    $acceories->wristbandLevel = max(0, min(100, rand($accessoryMean - 5, $accessoryMean + 5)));;
    
    return $acceories;
}
function getTeamJersey($teamId, $conn) {
    $query = "SELECT * FROM `". \Map\JerseysTableMap::TABLE_NAME ."` WHERE team_id=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $teamId));
    $jerseyRow = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];

    $jersey = new stdClass;
    $jersey->shirtColor = hex2rgb($jerseyRow['shirt_color']);
    $jersey->shirtPatternColor = hex2rgb($jerseyRow['shirt_pattern_color']);
    $jersey->shirtLineColor = hex2rgb($jerseyRow['shirt_line_color']);
    $jersey->shirtSleveColor = hex2rgb($jerseyRow['shirt_sleve_color']);
    $jersey->pantColor = hex2rgb($jerseyRow['pant_color']);
    $jersey->pantPatternColor = hex2rgb($jerseyRow['pant_pattern_color']);
    $jersey->shirtTexture = (int)($jerseyRow['shirt_texture']);
    $jersey->pantTexture = (int)($jerseyRow['pant_texture']);
    $jersey->shoeColor = hex2rgb($jerseyRow['shoe_color']);
    $jersey->shoeSoleColor = hex2rgb($jerseyRow['shoe_sole_color']);
    $jersey->socksColor = hex2rgb($jerseyRow['socks_color']);
    $jersey->numberColor = hex2rgb($jerseyRow['number_color']);
    $jersey->capColor = hex2rgb($jerseyRow['cap_color']);
    $jersey->studioLogoColor = hex2rgb($jerseyRow['studio_logo_color']);

    return $jersey;
}

function getPlayerAppearance($playerId, $conn)
{
    $query = "SELECT * FROM `". \Map\PlayerAppearanceTableMap::TABLE_NAME ."` WHERE player=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $playerId));
    $appearance = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    unset($appearance['id']);
    unset($appearance['player']);
    $appearance["playerFaceAppearance"] = getPlayerFaceAppearance($playerId, $conn);
    
    return $appearance;
}


function getPlayerFaceAppearance($playerId, $conn) 
{
    $query = "SELECT * FROM `". \Map\PlayerFaceAppearanceTableMap::TABLE_NAME ."` WHERE player=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $playerId));
    $faceAppearance = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    unset($faceAppearance['id']);
    unset($faceAppearance['player']);
    
    return $faceAppearance;
}


function getBatting($playerId, $conn) 
{
    $query = "SELECT * FROM `". \Map\BattingTableMap::TABLE_NAME ."` WHERE player=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $playerId));
    $battingAttrs = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    unset($battingAttrs['id']);
    unset($battingAttrs['player']);
    
    return $battingAttrs;
}

function getBowling($playerId, $conn) 
{
    $query = "SELECT * FROM `". \Map\BowlingTableMap::TABLE_NAME ."` WHERE player=:id";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $playerId));
    $bowlingAttrs = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
    unset($bowlingAttrs['id']);
    unset($bowlingAttrs['player']);

    return $bowlingAttrs;
}

function getTeamPlayers($teamId, $conn){
    $playersTable = \Map\PlayersTableMap::TABLE_NAME;
    $playersTeamTable = \Map\PlayerTeamsTableMap::TABLE_NAME;
    $query = "SELECT player, battingOrder FROM `". $playersTeamTable."` ".
    "LEFT JOIN `". \Map\PlayersTableMap::TABLE_NAME ."` ON `".$playersTable."`.`id` = `".$playersTeamTable."`.`player`".
    " WHERE team=:id  ORDER BY `".$playersTable."`.`battingOrder` ASC, `".$playersTable."`.`transferValue` DESC";
    $stmt = $conn->prepare($query);
    $stmt->execute(array(':id' => $teamId));
    $playerData = $stmt->fetchAll(PDO::FETCH_CLASS);
    $selectedPlayers = [];
    for($i=0; $i<11; $i++) {
        $playerSuitableForPositon = getPlayerForBattingPosition($i+1, $playerData, $selectedPlayers);
        array_push($selectedPlayers, $playerSuitableForPositon);
    }
    $selectedPlayers = array_merge($selectedPlayers, getAllReservePlayers($selectedPlayers, $playerData));
    return array_map("intval", $selectedPlayers);
}
function getAllReservePlayers($selectedPlayers, $allPlayerData) 
{
    $reservePlayers = [];
    foreach($allPlayerData as $playerData) {
        if(!in_array($playerData->player, $selectedPlayers)) {
            array_push($reservePlayers, $playerData->player);
        }
    }
    return $reservePlayers;
}

function getPlayerForBattingPosition($position, $playerData, $selectedPlayers) {
    foreach($playerData as $playerInfo) {
        if(!in_array($playerInfo->player, $selectedPlayers)) {
            if($playerInfo->battingOrder == $position) {
                return $playerInfo->player;
            }
        }
        
    }

    return getPlayerByClosestPosition($position, $playerData, $selectedPlayers);
}

function getPlayerByClosestPosition($position, $allPlayerData, $selectedPlayers) {
    $selectedPlayer = null;
    $closestDistance = INF;

    foreach($allPlayerData as $playerData) {
        if(!in_array($playerData->player, $selectedPlayers)) {
            if($closestDistance > abs($playerData->battingOrder - $position)) {
                $closestDistance = abs($playerData->battingOrder - $position);
                $selectedPlayer = $playerData->player;
            }
        }
    }

    return $selectedPlayer;
}

function writeFile($contents, $name) 
{
    $outputPath = './output/';
    file_put_contents($outputPath.$name, $contents);
}

function hex2rgb( $colour ) {
    if ( $colour[0] == '#' ) {
            $colour = substr( $colour, 1 );
    }
    list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
    
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );
    return (object)array( 'red' => $r, 'green' => $g, 'blue' => $b );
}
?>