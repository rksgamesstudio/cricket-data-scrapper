<?php
require "vendor/autoload.php";


$highProfilePlayers = HighProfilePlayersQuery::create()->filterByPriceRank(array('min' => 0))->orderByPriceRank('desc')->find();

foreach($highProfilePlayers as $player) {
    $currentHighestPlayer = RawStreamQuery::create()->orderByValue('desc')->findOne();
    // print_r($currentHighestPlayer);
    $entry = RawStreamQuery::create()->findOneByLink($player->getCanonicalLink());

    $entry->setValue($currentHighestPlayer->getValue() + (mt_rand() / mt_getrandmax()));
    print "value set is ".$entry->getValue();
    $entry->save();
}