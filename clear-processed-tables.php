<?php
require "vendor/autoload.php";
$conn =  \Propel\Runtime\Propel::getConnection();
$sql = "TRUNCATE TABLE `".\Map\PlayersTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\BattingTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\BowlingTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\FieldingTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\PlayerAppearanceTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\PlayerFaceAppearanceTableMap::TABLE_NAME."`;"
    ."TRUNCATE TABLE `".\Map\PlayerTeamsTableMap::TABLE_NAME."`;";

$conn->exec($sql);

?>