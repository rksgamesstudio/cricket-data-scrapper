<?php
require "vendor/autoload.php";

$rawStreamRows = RawStreamQuery::create()->find();
$playerIndex = count($argv) > 1? $argv[1] : 0;

echo "Player id is " . $rawStreamRows[$playerIndex]->getId() .", Name is -> " . $rawStreamRows[$playerIndex] -> getName(). "\n";
echo "Batting value is " . PlayerSkillEvaluator::calculateBattingSkill($rawStreamRows[$playerIndex], "T20") . " \n";
echo "Bowling value is " . PlayerSkillEvaluator::calculateBowlingSkill($rawStreamRows[$playerIndex], "T20") . " \n";
?>
