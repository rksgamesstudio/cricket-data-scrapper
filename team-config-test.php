<?php

require "vendor/autoload.php";

$teamParserConfig = new PlayerParserConfig;
$teamParserConfig->url = "https://en.wikipedia.org/wiki/South_Africa_national_cricket_team";
$teamParserConfig->tableSelector = 'table.wikitable[9]';
$teamParserConfig->nameSelector = 'td[1]';
$teamParserConfig->battingSelector = 'td[3]';
$teamParserConfig->bowlingSelector = 'td[4]';
$teamParserConfig->teamId = 0;
$insertedPlayers = PlayerParser::parsePlayers($teamParserConfig, true);
print_r($insertedPlayers);
?>