<?php
require "vendor/autoload.php";
use PHPHtmlParser\Dom;

$url = "https://en.wikipedia.org/wiki/Elmore_Hutchinson";

$rawStreamRows = RawStreamQuery::create()->find();
$playerIndex = count($argv) > 1? $argv[1] : 0;

$dom = new Dom;
$dom->load(file_get_contents($url));
WikipediaStatsParser::parserPlayerStats($dom, $rawStreamRows[$playerIndex],false);
?>