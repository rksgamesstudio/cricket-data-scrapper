# Cricket Data Scrapper

Scrapes cricket player information in Wikipedia and espncricino.


### Installation

Update Composer
```sh
php composer.phar install
```

Building models
```sh
./vendor/bin/propel model:build --output-dir ./models/ --schema-dir .
```

Update SQL file
```sh
./vendor/bin/propel sql:build --schema-dir . --overwrite
```

Copy contents of sqldb.map.backup to sqldb.map file and run following command to create database
WARNING - This will erase everything from existing database


```sh
./vendor/bin/propel sql:insert
```

### Scrapping Data

1. Download the players
```sh
php download-players.php
```

2. Download Player attributes
```sh
php player-attribute-processor.php
```

3. Evalute overall batting and bowling values based on record. Also store price
```sh
php evaluate-player-skills.php
```

4. Update pricing for high profile players
```sh
php update-high-profile-players.php
```

5. Process raw stream into tables
```sh
php process-players.php
```

6. Generate JSON for teams
```sh
php json-exporter.php teams
```

7. Generate JSON for players
```sh
php json-exporter.php players
```