<?php
class AppearanceHelper{
    public static $skinIds = array(
        "white" => array( 1, 2),
        "indian"=> array( 2, 3, 4),
        "black" => array(3, 4, 5)
    );

    public static $hairColorIds = array(
        "white" => array( 3, 4, 5),
        "indian"=> array( 1, 2),
        "black" => array(1, 2)
    );

    public static $maxHairStyleId = 7;
    public static $maxBeardStyleId = 8;
    public static $maxNeckTatooId = 10;
    public static $maxArmTatooId = 10;
    


    public static function getHairColorId(RawStream $entry) {
        return self::$hairColorIds[$entry->getRace()][array_rand(self::$hairColorIds[$entry->getRace()])];
    } 

    public static function getHairStyleId(RawStream $entry) {
        return rand(1, self::$maxHairStyleId);
    }

    public static function getBeardStyleId(RawStream $entry) {
        return rand(1, self::$maxBeardStyleId);
    }

    public static function getNeckTatooId(RawStream $entry) {
        $tatooId = 0;
        if (rand(0, 10) > 7)
        {
            $tatooId = rand(1, self::$maxNeckTatooId);
        }

        return $tatooId;
    }

    public static function getArmTatooId(RawStream $entry) {
        $tatooId = 0;
        if (rand(0, 10) > 7)
        {
            $tatooId = rand(1, self::$maxArmTatooId);
        }

        return $tatooId;
    }
    public static function getSkinColorId(RawStream $entry) {
        return self::$skinIds[$entry->getRace()][array_rand(self::$skinIds[$entry->getRace()])];
    }
}