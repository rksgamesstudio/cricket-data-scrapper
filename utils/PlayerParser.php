<?php
use PHPHtmlParser\Dom;

class PlayerParser {

    private static $indexedSelectorPattern = '/(.*)\[(\d+)\]/';
    private static $playerTypeLabels = array(
        "batsmen" =>"batsmen",
        "batsman"=> "batsmen",
        "opening batsmen"=> "batsmen",
        "middle-order batsmen"=> "batsmen",
        "all-rounders"=> "allRounder",
        "all rounders"=> "allRounder",
        "wicket-keepers"=> "keeper",
        "wicketkeepers"=> "keeper",
        "wicket-keeper batsmen" => 'keeper',
        "bowlers"=> "bowler",
        "bowler"=> "bowler",
        "pace bowlers"=> "bowler",
        "spin bowler"=> "bowler",
        "spin bowlers"=> "bowler",
        "wicket-keeper batsman" => 'keeper',
        "test, odi &amp; t20i captain; wicket-keeper batsman"=>'keeper',
        "test &amp; odi captain and middle-order batsman"=>'batsmen',
        "t20i captain, odi vice-captain and wicket-keeper batsman"=>"batsmen",
        "captain and middle-order batsman"=>'batsmen',
        "test &amp; odi captain"=>'batsmen',
        "test vice-captain and middle-order batsman" => 'batsmen',
        "odi, t20i vice-captain and opening batsman" => 'batsmen',
        "test captain and top-order batsman"=>"batsmen",
        "test captain and all-rounder"=>"batsmen",
        "t20i vice-captain" => 'batsmen',
        "captain and all-rounder" => 'allRounder',
        "test, odi captain and opening batsman"=>'batsmen',
        "test captain" => 'batsmen',
        "wicket keepers" => 'keeper',
        "middle order batsmen" => 'batsmen',
        "fast bowlers" => 'bowler',
        "odi captain" => 'batsmen',
        "t20i captain and odi vice-captain" => 'batsmen',
        "test vice-captain" => 'batsmen',
        "t20i captain and fast bowler" => 'bowler'
    );
    public static $availablePlayers = array();

    public static function parsePlayers(PlayerParserConfig $config, bool $insertData = true){
        $content = UrlFetcher::LoadUrl($config->url);
        $dom  = new Dom;
        $dom->load($content);
        $table = self::fetchElement($dom, $config->tableSelector);
        // print_r($table);
        // echo "found table " . $table->text(true) . " \n";
        if(!$table) {
            echo "No table was found by selector!!";
            return array();
        }

        $players =array();
        // echo "found table " . $table->text . " \n";
        //loop through table rows and process data accordingly
        $tableRows = $table->find('tr');
        $currentPlayerType = "";
        // echo "rows count " . count($tableRows)."\n";
        foreach($tableRows as $row) {
            if(count($row->find('td')) && count(self::fetchElement($row, $config->nameSelector)->find('a')) == 1) {
                //process the player
                $currentPlayer = new RawStream();
                $currentPlayer->setName(self::fetchElement($row, $config->nameSelector)->text(true));
                echo "Name is ".$currentPlayer->getName()."\n";
                $currentPlayer->setLink("https://en.wikipedia.org" . self::fetchElement($row, $config->nameSelector)->find('a')->getAttribute('href'));
                $bowlingElement = self::fetchElement($row, $config->bowlingSelector);
                $currentPlayer->setBattingHand(self::getPlayerHand(self::fetchElement($row, $config->battingSelector)->text(true)));
                $currentPlayer->setBowlingHand(self::getPlayerHand(self::fetchElement($row, $config->bowlingSelector)->text(true)));
                $currentPlayer->setBowlerType(self::getBowlerType($bowlingElement->text(true)));
                $currentPlayer->setSpinType(self::getSpinType($bowlingElement->text(true)));
                $currentPlayer->setPrimaryRole($currentPlayerType);
                $currentPlayer->setTeam($config->teamId);
                if($insertData) {
                    $currentPlayer->save();
                }
                
                array_push($players, $currentPlayer);
            } else {
                if(count($row->find('th')) == 1) {
                    //process player type
                    //parse current player
                    $headerText = strtolower(trim($row->text(true)));
                    // echo "Looking for header text $headerText \n";
                    if(array_key_exists($headerText, self::$playerTypeLabels)) {
                        $currentPlayerType = self::$playerTypeLabels[$headerText];
                    } else {
                        if (strpos($headerText, 'batsman') !== false) {
                            $currentPlayerType = "batsmen";
                        }
                        if (strpos($headerText, 'bowler') !== false) {
                            $currentPlayerType = "bowler";
                        }
                        if (strpos($headerText, 'rounder') !== false) {
                            $currentPlayerType = "allRounder";
                        }
                        if($currentPlayerType == "") 
                        {
                            $currentPlayerType = "batsmen";
                        }
                    }
                } else {
                    //process the player
                    // echo "Name -> ".$row->find($config->nameSelector)->text;
                }
            }
        }

        return $players;
    }

    public static function getPlayerHand($str)
    {
        $type = "";
        if(stripos($str, "Left") !== false) {
            $type = "Left";
        } else if(strripos($str, "Right") !== false) {
            $type = "Right";
        } else if(strripos($str, "l") !== false) {
            $type = "Left";
        } else {
            $type = "Right";
        }
        return $type;
    }
    public static function getBowlerType($str) {
        $type = '';
        if(stripos($str, "fast") !== false) {
          $type = 'Fast';
        } else if(stripos($str, "medium") !== false) {
          $type = 'Medium';
        } else if($str) {
          $type = 'Spin';
        }
        return $type;
    }
      
    private static function getSpinType($str) {
        $type = "";
        if(self::GetBowlerType($str) === "Spin") {
          $type = stripos($str, "leg") !== false ? "Leg" : "Off";
        }
        return $type;
      }
    private static function parsePlayerType() {
        return 0;
    }
    private static function fetchElement($parent, $selector) {
        if(preg_match(self::$indexedSelectorPattern, $selector, $matches) && count($matches) == 3) {
            $results = $parent -> find($matches[1]);
            // echo "total selctors " . count($results) . " looking for index ". $matches[2]. "\n"; 

            if(count($results) > ($matches[2] - 1))
            {
                //we should have the data
                return $results[$matches[2] -1];
            } else {
                //insufficient source
                return $parent->find($selector);

            }
        } else {
            // echo "Returning direct table \n";
            //problem with selector
            return $parent->find($selector);
        }
    }




}

class PlayerParserConfig{
    public $url;
    public $tableSelector;
    public $nameSelector;
    public $battingSelector;
    public $bowlingSelector;
    public $teamId;
}
?>
