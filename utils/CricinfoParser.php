<?php
use PHPHtmlParser\Dom;

class CricInfoParser {
    public static $battingTableIndex = 0;
    public static $bowlingTableIndex = 1;
    public static $matchInforTableIndex = 2;
    public static $dataTableSelector = "table.engineTable";
    public static $lastMatchLabelsConfig = array(
        "T20" => "Last T20s",
        "ODI" => "Last ODI"
    );
    public static $ageLabel = "Current age";
    public static $ageRegex = '/^(\d+)\syears/';
    public static $playerIdRegex = '/\/(\d+)\.html/';
    public static function updatePlayerStats(string $url, RawStream $rawStream, string $teamFormat, bool $persistData = true){
        $content = UrlFetcher::LoadUrl($url);
        $dom = new Dom;
        $dom->load($content);
        $battingStats = self::parseBattingStats($dom->find(self::$dataTableSelector, self::$battingTableIndex), $rawStream -> getId());
        // echo "\n batting Stats \n";
        if(!$persistData) {
            print_r($battingStats);
        }

        $bowlingStats = self::parseBowlingStat($dom->find(self::$dataTableSelector, self::$bowlingTableIndex), $rawStream -> getId());
        // echo "\n bowling Stats \n";
        if(!$persistData) {
            print_r($bowlingStats);
        }

        $rawStream->setAge( self::parsePlayerAge($dom));

        if($persistData) {
            $rawStream->setIsStatsProcessed(1);
            $rawStream->save();
        }
        preg_match(self::$playerIdRegex, $url, $matches);
        // print_r($matches);
        $cricinfoId = $matches[1];

        $parsedPosition = self::getBattingPosition($dom, $cricinfoId, self::$lastMatchLabelsConfig[$teamFormat]);

        if($parsedPosition < 1) {
            //position parsing failed
            switch($rawStream->getPrimaryRole()) {
                case 'batsmen':
                case 'keeper':
                    $parsedPosition = rand(1, 5);
                break;

                case 'bowler':
                    $parsedPosition == rand(7, 11);
                break;

                case 'allRounder':
                    $parsedPosition = rand(4,7);
                break;
            } 
        }
        $rawStream -> setBattingorder($parsedPosition);
        $rawStream -> setStatsLink($url);
        if($persistData) {
            $rawStream->setIsProcessed(1);
            $rawStream -> save();
        }
        else {
            print_r($rawStream);
        }

        // print_r($rawStream);

        // if(!$persistData){
        //     print_r($battingStats);
        //     print_r($bowlingStats);
        //     print_r($rawStream);
        // }

        // echo "player id is $cricinfoId \n";
        // echo "position found is " . self::getBattingPosition($dom, $cricinfoId);
        
    }

    public static function parsePlayerAge(Dom $dom) {
        $age = 0;
        $informationRows = $dom->find(".ciPlayerinformationtxt");

        foreach($informationRows as $informationRow) {
            if($informationRow->find("b", 0)->text(true) == self::$ageLabel) {
                $ageText = $informationRow->find("span", 0)->text(true);
                preg_match(self::$ageRegex, $ageText, $matches);
                $age = $matches[1];
                break;
            }
        }
        return $age;
    }

    public static function parseBattingStats($table, $rawStreamId, bool $persistData = true) {
        $statRow = $table->find('tbody tr');
        $stats = array();
        foreach($statRow as $row) {
            $stat = new RawStreamBattingStats();
            $stat->setFormat(self::parseFormat($row->find('td', 0)->text(true)));
            $stat->setPlayerId($rawStreamId);
            $stat->setMatches( $row->find('td', 1)->text(true));
            $stat->setInnings( $row->find('td', 2)->text(true));
            $stat->setNotOuts( $row->find('td', 3)->text(true));
            $stat->setRuns( $row->find('td', 4)->text(true));
            $stat->setAverage( $row->find('td', 6)->text(true));
            $stat->setStrikeRate($row->find('td', 8)->text(true));
            $stat->setCenturies( $row->find('td', 9)->text(true));
            $stat->setHalfCenturies( $row->find('td', 10)->text(true));
            $stat->setFours( $row->find('td', 11)->text(true));
            $stat->setSixes($row->find('td', 12)->text(true));
            if($persistData) {
                $stat->save();
            }

            array_push($stats, $stat);
        }
        
        return $stats;
    }

    public static function parseBowlingStat($table, $rawStreamId, bool $persistData = true) {
        $statRow = $table->find('tbody tr');
        $stats = array();
        foreach($statRow as $row) {
            $stat = new RawStreamBowlingStats();
            $stat->setPlayerId($rawStreamId);
            $stat->setFormat(self::parseFormat($row->find('td', 0)->text(true)));
            $stat->setMatches($row->find('td', 1)->text(true));
            $stat->setInnings($row->find('td', 2)->text(true));
            $stat->setBalls($row->find('td', 3)->text(true));
            $stat->setRuns($row->find('td', 4)->text(true));
            $stat->setWickets($row->find('td', 5)->text(true));
            $stat->setAverage($row->find('td', 8)->text(true));
            $stat->setEconomy($row->find('td', 9)->text(true));
            $stat->setStrikeRate($row->find('td', 10)->text(true));
            if($persistData) {
                $stat->save();
            }

            array_push($stats, $stat);
        }
        
        return $stats;
    }

    public static function parseFormat($rowText) {
        $formatType = null;
        switch($rowText) {
            case Format::T20I:
                $formatType = Format::T20I;
                break;

            case Format::T20:
                $formatType = Format::T20;
                break;

            case Format::ODI:
                $formatType = Format::ODI;
                break;

            case Format::TEST:
                $formatType = Format::TEST;
                break;

            case Format::FC:
                $formatType = Format::FC;
                break;

            case Format::A:
                $formatType = Format::A;
                break;
        }

        return $formatType;
    }

    public static function getBattingPosition(Dom $dom, $playerId, $lastMatchLabel) {
        $matchInfo = $dom->find(self::$dataTableSelector, self::$matchInforTableIndex);
        $lastMatchUrl = "http://espncricinfo.com";
        $hasFoundLastMatchUrl = false;
        foreach($matchInfo->find("tr") as $row) {
            // print_r($row->find('td'));
            if($row->find("td", 0)->text(true) == $lastMatchLabel) {
                $lastMatchUrl.= $row->find("td", 1)->find("a")->getAttribute("href");
                $hasFoundLastMatchUrl = true;
                break;
            }
        }

        if(!$hasFoundLastMatchUrl) {
            return;
        }
        
        $lastMatchUrl = self::fetchScorecardUrl($lastMatchUrl);
        // echo "Loading URL " . $lastMatchUrl . " \n";
        $content = UrlFetcher::LoadUrl($lastMatchUrl);
        $matchDom = new Dom();
        $matchDom->load($content);

        $scoreCards = $matchDom->find(".table.batsman");
        // echo "score count is " . count($scoreCards) . " \n";
        
        echo "Total scorecards found are " . count($scoreCards) ."\n" ;
        if(count($scoreCards) == 0) {
            $position = self::parseOldLayout($matchDom, $playerId);
        } else {
            $position = self::parseNewLayout($scoreCards, $playerId);
        }
        
        
        
        return $position;
    }

    public static function parseOldLayout($matchDom, $playerId)
    {
        $position = 0;
        $currentIndex = 0;
        $hasFoundPosition = 0;
        $scoreCards = array();

        // echo "Parsing old layout";

        $battingTables = $matchDom->find("table.batting-table");
        if(count($battingTables) == 0) {
            echo "Something terribly wrong with redirection!!";
            return -1;
        }
        $scoreCards[0] = new OldLayoutScorecard;
        $scoreCards[0]->battingTable = $battingTables[0];
        if(count($scoreCards[0]->battingTable->find("td.batsman-name a.playerName")) < 11){
            // echo "Loading extra data 1 \n";
            $scoreCards[0]->remainingBatsmen = $matchDom->find("div.more-match-stats div.to-bat", 0);
        }

        $scoreCards[1] = new OldLayoutScorecard;
        $scoreCards[1]->battingTable = $battingTables[1];
        if(count($scoreCards[1]->battingTable->find("td.batsman-name a.playerName")) < 11){
            // echo "Loading extra data 2 - ".($scoreCards[0]->remainingBatsmen != null ? 2 : 1)."\n";
            $scoreCards[1]->remainingBatsmen = 
                $matchDom->find("div.more-match-stats div.to-bat", 
                $scoreCards[0]->remainingBatsmen != null ? 1 : 0);
        }
        
        foreach($scoreCards as $key=>$scoreCard) {
            //find all batsman
            $currentIndex = 0;
            $batsmenLinks = $scoreCard->battingTable-> find("td.batsman-name a.playerName");
            foreach($batsmenLinks as $batsmanLink) {
                // echo "Batsman Name is " . $batsmanLink->text(true) ." \n" ;
                $currentIndex++;
                if(self::isPlayerLink($batsmanLink->getAttribute("href"), $playerId )) {
                    $hasFoundPosition = true;
                    $position = $currentIndex;
                    break;
                }
            }

            if(!$hasFoundPosition && $scoreCard->remainingBatsmen) {
                //look into remaing batsmen
                foreach( $scoreCard->remainingBatsmen->find("a.playerName") as $batsmanLink) {
                    // echo "Batsman Name is " . $batsmanLink->text(true) ." \n" ;
                    $currentIndex++;
                    if(self::isPlayerLink($batsmanLink->getAttribute("href"), $playerId )) {
                        $hasFoundPosition = true;
                        $position = $currentIndex;
                        break;
                    }
                }

            }

            if($hasFoundPosition) break;

        }

        return $position;

    }

    public static function parseNewLayout($scoreCards, $playerId) {
        $position = 0;
        $hasFoundPosition = 0;
        $currentIndex = 0;
        foreach($scoreCards as $scoreCard) {
            //find all batsman 
            $currentIndex = 0;
            foreach($scoreCard->find(".batsman-cell") as $batsmanRow)
            {
                // echo count($batsmanRow->find('a')) . " link " .  $batsmanRow -> text(true) . " \n";
                if(count($batsmanRow->find('a')) < 1) {
                    continue;
                }
                $currentIndex++;
                if(self::isPlayerLink($batsmanRow->find('a', 0)->getAttribute("href"), $playerId )) {
                    $hasFoundPosition = true;
                    $position = $currentIndex;
                    break;
                }
            }

            if(!$hasFoundPosition && count($scoreCard->find("tfoot"))) {
                foreach($scoreCard->find("tfoot")->find('tr',1)->find("a") as $batsmanLink) 
                {
                    $currentIndex ++;
                    if(self::isPlayerLink($batsmanLink->getAttribute("href"), $playerId )) {
                        $hasFoundPosition = true;
                        $position = $currentIndex;
                        break;
                    }
                }
            } else {
                break;
            }
        }
        return $position;
    }

    public static function isPlayerLink($url, $playerId) {
        echo "Looking for player $playerId in $url \n";
        preg_match(self::$playerIdRegex, $url, $matches);
        return $playerId == $matches[1];
    }

    public static function fetchScorecardUrl($url)
    {
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $html = curl_exec($ch);

        $redirectedUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

        curl_close($ch);
        
        return str_replace('/game/', '/scorecard/', $redirectedUrl);
        
    }
}
class OldLayoutScorecard{
    public $battingTable = null;
    public $remainingBatsmen = null;
}
