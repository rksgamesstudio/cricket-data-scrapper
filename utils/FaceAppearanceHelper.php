<?php
class FaceAppearanceHelper
{
    public static $appearanceConfig = array(
        "noseWidth" => array(
            "white" => array("min" => 30, "max" => 50),
            "indian" => array("min" => 30, "max" => 50),
            "black" => array("min" => 50, "max" => 80)
        ),
        "noseHeight" => array(
            "white" => array("min" => 30, "max" => 80),
            "indian" => array("min" => 30, "max" => 80),
            "black" => array("min" => 20, "max" => 50)
        ),
        "chin" =>  array(
            "white" => array("min" => 40, "max" => 60),
            "indian" => array("min" => 20, "max" => 60),
            "black" => array("min" => 20, "max" => 50)
        ),
        "cheeks" => array(
            "white" => array("min" => 40, "max" => 60),
            "indian" => array("min" => 30, "max" => 60),
            "black" => array("min" => 50, "max" => 80)
        ),
        "jaw" => array(
            "white" => array("min" => 40, "max" => 70),
            "indian" => array("min" => 30, "max" => 80),
            "black" => array("min" => 60, "max" => 80)
        ),
        "lips" => array(
            "white" => array("min" => 30, "max" => 60),
            "indian" => array("min" => 30, "max" => 70),
            "black" => array("min" => 60, "max" => 80)
        )
    );


    public static function GetFaceFeatureValue(RawStream $entry, $faceFeature)
    {
        return rand(self::$appearanceConfig[$faceFeature][$entry->getRace()]["min"], self::$appearanceConfig[$faceFeature][$entry->getRace()]["max"]);
    }
}
