<?php
class Format
{
    public const T20I = 'T20Is';
    public const T20 = 'T20s';
    public const ODI = 'ODIs';
    public const TEST = 'Tests';
    public const FC = 'First-class';
    public const A = 'List A';
}

class BasicInfo 
{
    public $battingHand;
    public $bowlingHand;
    public $age;
    public $bowlingType;
    public $spinType;
}

class BattingStat{
    public $format;
    public $matches;
    public $innings;
    public $notOuts;
    public $runs;
    public $average;
    public $strikeRate;
    public $centuries;
    public $halfCenturies;
    public $fours;
    public $sixes;
}

class BowlingStat{
    public $format;
    public $matches;
    public $innings;
    public $balls;
    public $runs;
    public $wickets;
    public $average;
    public $economy;
    public $strikeRate;
}

?>