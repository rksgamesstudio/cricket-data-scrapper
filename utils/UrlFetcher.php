<?php
class UrlFetcher{
    public static function LoadUrl($url){
        print "Loading url - $url \n";
        if(!$url) {
            return '';
        }
        $sleepBetweenTries = 10; // seconds
        $times = 10;
        $file = "";
        for ($i = 0; $i < $times; $i++ ) {
            $file = file_get_contents($url);
            if ($file!==false) {
                break;
            } else {
                echo "Failed to load URL. $i \n";
            }
            sleep($sleepBetweenTries);
        }
        // var_dump($http_response_header);
        return $file;
    }
}