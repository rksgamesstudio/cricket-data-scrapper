<?php

class PlayerSkillEvaluator {
    //Returns team based on stats and collected batting order
    
    public static function updateTeamPlayers(Teams $team) {
        $players = array();

        foreach(RawStreamQuery::create()->findByTeam($team->getId()) as $entry) {
            $currentPlayer = $entry;
            echo "Player id is " . $entry->getId() .", Name is -> " . $entry -> getName(). "\n";
            $currentPlayer->setBatting(self::calculateBattingSkill($entry, $team->getTeamType()));
            $currentPlayer->setBowling(self::calculateBowlingSkill($entry, $team->getTeamType()));
            $currentPlayer->setPlayerValue();
            $currentPlayer->save();
        }

        //all players are loaded
        //fill team position one by one by evaluating each player to position
        
    }


    public static function calculateBattingSkill(RawStream $entry, string $teamFormat){
        
        $battingStats = RawStreamBattingStatsQuery::create()
                        -> filterByPlayerId($entry->getId())
                        -> filterByFormat(constant("Format::".$teamFormat)) 
                        -> findOne();

        if($battingStats) {
            return self::getBattingSkill($battingStats, $entry->getBattingorder());
        } else {
            //desired format not found
            $allStats = RawStreamBattingStatsQuery::create()
                        -> filterByPlayerId($entry->getId())
                        -> find();
            $average = 0;
            for($i=0; $i<count($allStats); $i++) {
                $average += self::getBattingSkill($allStats[$i],  $entry->getBattingorder());
            }
            return $average / count($allStats);
        }
    }

    public static function calculateBowlingSkill(RawStream $entry, string $teamFormat){
        $bowlingStats = RawStreamBowlingStatsQuery::create()
                        -> filterByPlayerId($entry->getId())
                        -> filterByFormat(constant("Format::".$teamFormat)) 
                        -> findOne();

        if($bowlingStats) {
            return self::getBowlingSkill($bowlingStats);
        } else {
            //desired format not found
            $allStats = RawStreamBowlingStatsQuery::create()
                        -> filterByPlayerId($entry->getId())
                        -> find();
            $average = 0;
            for($i=0; $i<count($allStats); $i++) {
                $average += self::getBowlingSkill($allStats[$i]);
            }
            return ($average * 0.8) / count($allStats);
        }
    }

    public static function getBattingSkill(RawStreamBattingStats $battingStats, int $battingPosition) {
        if($battingStats->getDataSource() == "cricinfo") {
            return self::getCricinfoBattingSkill($battingStats, $battingPosition);
        } else {
            return self::getWikiBattingSkill($battingStats);
        }
    }

    public static function getBowlingSkill(RawStreamBowlingStats $bowlingStats) {
        if($bowlingStats->getDataSource() == "cricinfo") {
            return self::getCricinfoBowlingSkill($bowlingStats);
        } else {
            return self::getWikiBowlingSkill($bowlingStats);
        }
    }

    public static function getCricinfoBowlingSkill(RawStreamBowlingStats $bowlingStats){
        $economy = 1 - abs(($bowlingStats->getEconomy() - 6) / 12);
        $experience = $bowlingStats->getBalls() / 4000;
        $strikeRate = 1 - abs(($bowlingStats->getStrikeRate() - 16 ) / 25);
        $participation = $bowlingStats->getInnings() / $bowlingStats->getMatches();
        // echo "srfactor " . (($bowlingStats->getEconomy() - 6) / 12)."\n";

        $economy = ($economy < 0) ? 0 : $economy;
        $experience = ($experience > 1) ? 1 : $experience;
        $strikeRate = ($strikeRate < 0) ? 0 : $strikeRate;
        // echo "Economy -> $economy, Experiance -> $experience, Strike rate $strikeRate \n";

        return ($economy + $participation + $experience + $strikeRate) * 24;
    }

    public static function getWikiBowlingSkill(RawStreamBowlingStats $bowlingStats){
        $experience = $bowlingStats->getBalls() / 10000;
        $strikeRate = 1 - (($bowlingStats->getStrikeRate() - 16 ) / 25);
        
        $experience = ($experience > 1) ? 1 : $experience;
        $strikeRate = ($strikeRate < 1) ? 0 : $strikeRate;

        return ($experience + $strikeRate) * 32;
    }

    public static function getCricinfoBattingSkill(RawStreamBattingStats $battingStats, int $battingPosition) {
        $reliability = $battingStats->getInnings() / $battingStats->getNotOuts();
        $reliabilityFactor = ($battingPosition -4) / 7;
        $reliabilityFactor = min(max($reliabilityFactor, 0), 1);
        echo "Reliability ratio " .$reliability ." abs factor " . $reliabilityFactor."\n";
        $reliability = 1 - (abs(($reliability - 2) / 3) * $reliabilityFactor);
        $experience = $battingStats->getInnings() / 100;
        $aggression = $battingStats->getStrikeRate() / 150;
        $skill = $battingStats->getAverage() / 40;

        //make sure no value is more than 1
        $reliability = ($reliability <0 ) ? 0 : $reliability;
        $experience = ($experience > 1) ? 1 : $experience;
        $aggression = ($aggression > 1) ? 1 : $aggression;
        $skill = ($skill> 1) ? 1 : $skill;
        echo "Reliability -> $reliability, Experiance -> $experience, Aggression $aggression, Skill $skill \n";

        return (($reliability*0.4) + ($experience*0.1) + ($aggression * 0.3) + ($skill * 0.2)) * 96; //max 96
        
    }

    public static function getWikiBattingSkill(RawStreamBattingStats $battingStats){
        $experience = $battingStats->getMatches() / 100;
        $aggression = $battingStats->getStrikeRate() / 100;

        //make sure no value is more than 1
        $experience = ($experience > 1) ? 1 : $experience;
        $aggression = ($aggression > 1) ? 1 : $aggression;

        return ($experience + $aggression) * 32; //max 96
    }

    


}
?>