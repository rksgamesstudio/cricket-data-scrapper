<?php

class WikipediaStatsParser
{
    public static $labelToFormat = array(
        "FC"=> Format::FC,
        "List A" => Format::A,
        "LA" => Format::A,
        "T20" => Format::T20,
        "Test" => Format::TEST,
        "ODI" => Format::ODI,
        "T20I" => Format::T20I
    );

    public static $labelToBattingStatProperty = array(
        "Matches" => "setMatches",
        "Runs scored" => "setRuns",
        "Batting average" => "setAverage",
        "100s/50s" => "setWikiCenturyCount",

    );

    public static $labelToBowlingStatProperty = array(
        "Matches" => "setMatches",
        "Balls bowled" => "setBalls",
        "Wickets" => "setWickets",
        "Bowling average" => "setAverage",

    );
    
    public static $statsTableSelector = "table.infobox table";


    public static function parserPlayerStats($dom, RawStream $rawStream, bool $persistData = true) 
    {
        $table = $dom->find(self::$statsTableSelector, 0);

        if(!$table) {
            return;
        }
        $allBattingStats = array();
        $allBowlingStats = array();
        //count of stats
        $headerRows = $table -> find("tr");
        echo "Total rows found are " . count($headerRows)."\n";
        $formatHeaders = $headerRows[0] -> find('th');
        echo "header text ". $headerRows[0]->text(true);
        echo "Total competition types are " .count($formatHeaders)."\n";
        $formatCount = count($formatHeaders);
        for($i=1; $i < $formatCount; $i++) {
            $battingStats = new RawStreamBattingStats();
            echo "Extracting index $i   \n";
            echo "value was value is - ".$formatHeaders[$i]->text(true) ."\n";
            $battingStats->setFormat(self::$labelToFormat[$formatHeaders[$i]->text(true)]);
            $battingStats->setPlayerId($rawStream->getId());
            $battingStats->setDataSource('wiki');
            array_push($allBattingStats, $battingStats);

            $bowlingStats = new RawStreamBowlingStats();
            $bowlingStats->setFormat(self::$labelToFormat[$formatHeaders[$i]->text(true)]);
            $bowlingStats->setPlayerId($rawStream->getId());
            $bowlingStats->setDataSource('wiki');
            array_push($allBowlingStats, $bowlingStats);
        }
        echo "Bowling stats is " . count($allBattingStats) ." \n";
        //parse the data
        for($i=1; $i < count($headerRows); $i++) {
            $rowHeaderText = $headerRows[$i]->find("th")->text(true);
            
            $rowContent = $headerRows[$i]->find("td");
            
            for($j = 0; $j < $formatCount -1; $j++) {
                if(array_key_exists($rowHeaderText, self::$labelToBattingStatProperty)) {
                    $method = self::$labelToBattingStatProperty[$rowHeaderText];
                    $allBattingStats[$j]->$method(str_replace(',', '', $rowContent[$j]->text(true)));
                }

                if(array_key_exists($rowHeaderText, self::$labelToBowlingStatProperty)) {
                    $method = self::$labelToBowlingStatProperty[$rowHeaderText];
                    $allBowlingStats[$j]->$method(str_replace(',', '', $rowContent[$j]->text(true)));
                }
            }
        }
        if($persistData) {
            //update both
            for($i=0; $i < $formatCount - 1; $i++) {
                $allBattingStats[$i]->save();
                $allBowlingStats[$i]->save();
            }
        }

        print_r($allBattingStats);

    }
}