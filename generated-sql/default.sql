
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- batting
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `batting`;

CREATE TABLE `batting`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player` INTEGER,
    `shotPower` INTEGER DEFAULT 90,
    `shotAccurecy` INTEGER DEFAULT 90,
    `shotJudgement` INTEGER DEFAULT 90,
    `shotSpeed` INTEGER DEFAULT 90,
    `shotMedium` INTEGER DEFAULT 90,
    `shotSpinner` INTEGER DEFAULT 90,
    `runningSpeed` INTEGER DEFAULT 90,
    `shortBallStrength` INTEGER DEFAULT 90,
    `lengthBallStrength` INTEGER DEFAULT 90,
    `fullBallStrength` INTEGER DEFAULT 90,
    `temprament` INTEGER DEFAULT 90,
    `aggression` INTEGER,
    `straightStrength` INTEGER,
    `squareStrength` INTEGER,
    `average` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bowling
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bowling`;

CREATE TABLE `bowling`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player` INTEGER,
    `bowlerType` VARCHAR(45) DEFAULT 'Fast',
    `bowlSpeed` INTEGER DEFAULT 90,
    `bowlSwing` INTEGER DEFAULT 90,
    `bowlSpin` INTEGER DEFAULT 90,
    `spinType` VARCHAR(45) DEFAULT '90',
    `bowlAccurecy` INTEGER DEFAULT 90,
    `shortBallStrength` INTEGER DEFAULT 90,
    `lengthBallStrength` INTEGER DEFAULT 90,
    `fullBallStrength` INTEGER DEFAULT 90,
    `temprament` INTEGER DEFAULT 90,
    `average` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- fielding
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `fielding`;

CREATE TABLE `fielding`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player` INTEGER,
    `catchReliability` INTEGER DEFAULT 90,
    `throwSpeed` INTEGER DEFAULT 90,
    `throwAccurecy` INTEGER DEFAULT 90,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- jerseys
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jerseys`;

CREATE TABLE `jerseys`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `team_id` INTEGER,
    `shirt_color` VARCHAR(45),
    `shirt_pattern_color` VARCHAR(45),
    `shirt_sleve_color` VARCHAR(45),
    `pant_color` VARCHAR(45),
    `cap_color` VARCHAR(45),
    `studio_logo_color` VARCHAR(45),
    `number_color` VARCHAR(45),
    `pant_pattern_color` VARCHAR(45),
    `shirt_line_color` VARCHAR(45),
    `pant_texture` VARCHAR(45),
    `shoe_color` VARCHAR(45),
    `shoe_sole_color` VARCHAR(45),
    `socks_color` VARCHAR(45),
    `shirt_texture` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- player_teams
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `player_teams`;

CREATE TABLE `player_teams`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `team` INTEGER,
    `player` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- players
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `players`;

CREATE TABLE `players`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstName` VARCHAR(45),
    `lastName` VARCHAR(45),
    `jerseyNumber` VARCHAR(45),
    `jerseyName` VARCHAR(45),
    `battingHand` VARCHAR(45),
    `bowlingHand` VARCHAR(45),
    `fieldingHand` VARCHAR(45),
    `modelName` VARCHAR(45),
    `modelTexture` VARCHAR(45),
    `primaryRole` enum('batsmen','bowler','allRounder','keeper') DEFAULT 'allRounder',
    `battingOrder` INTEGER,
    `race` enum('indian','black','white') DEFAULT 'indian',
    `sourceUrl` VARCHAR(350),
    `transferValue` FLOAT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- players_appearance
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `players_appearance`;

CREATE TABLE `players_appearance`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player` INTEGER,
    `meshId` INTEGER,
    `hairStyleId` INTEGER,
    `beardStyleId` INTEGER,
    `hairColorId` INTEGER,
    `beardColorId` INTEGER,
    `neckTatooId` INTEGER,
    `leftArmTatooId` INTEGER,
    `rightArmTatooId` INTEGER,
    `skinColorId` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- players_face_appearance
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `players_face_appearance`;

CREATE TABLE `players_face_appearance`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player` INTEGER,
    `chin` INTEGER,
    `cheeks` INTEGER,
    `jaw` INTEGER,
    `lips` INTEGER,
    `noseHeight` INTEGER,
    `noseWidth` INTEGER,
    `earSize` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- raw_stream
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `raw_stream`;

CREATE TABLE `raw_stream`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `batting_hand` VARCHAR(50) NOT NULL,
    `bowling_hand` VARCHAR(50) NOT NULL,
    `age` INTEGER,
    `team` VARCHAR(100) NOT NULL,
    `bowler_type` VARCHAR(45),
    `spin_type` VARCHAR(45),
    `batting` INTEGER DEFAULT 80,
    `bowling` INTEGER DEFAULT 80,
    `value` FLOAT DEFAULT 80,
    `primary_role` enum('batsmen','bowler','allRounder','keeper') DEFAULT 'allRounder',
    `batting_order` INTEGER,
    `race` enum('indian','black','white') DEFAULT 'indian',
    `link` VARCHAR(250),
    `is_stats_processed` TINYINT DEFAULT 0,
    `stats_link` VARCHAR(400),
    `is_processed` TINYINT DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- raw_stream_batting_stats
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `raw_stream_batting_stats`;

CREATE TABLE `raw_stream_batting_stats`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `player_id` INTEGER,
    `data_source` enum('cricinfo','wiki') DEFAULT 'cricinfo',
    `format` VARCHAR(150),
    `matches` INTEGER,
    `innings` INTEGER,
    `not_outs` INTEGER,
    `runs` INTEGER,
    `average` FLOAT,
    `strike_rate` FLOAT,
    `centuries` INTEGER,
    `half_centuries` INTEGER,
    `fours` INTEGER,
    `sixes` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- raw_stream_bowling_stats
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `raw_stream_bowling_stats`;

CREATE TABLE `raw_stream_bowling_stats`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `data_source` enum('cricinfo','wiki') DEFAULT 'cricinfo',
    `player_id` INTEGER,
    `format` VARCHAR(150),
    `matches` INTEGER,
    `innings` INTEGER,
    `balls` INTEGER,
    `runs` INTEGER,
    `wickets` INTEGER,
    `average` FLOAT,
    `economy` FLOAT,
    `strike_rate` FLOAT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- teams
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `short_name` VARCHAR(3),
    `wiki_url` VARCHAR(255),
    `table_selector` VARCHAR(255),
    `player_name` VARCHAR(255),
    `player_batting_hand` VARCHAR(255),
    `player_bowling_hand` VARCHAR(255),
    `player_dob` VARCHAR(255),
    `logo` VARCHAR(255),
    `country` VARCHAR(255),
    `batting` INTEGER,
    `bowling` INTEGER,
    `fielding` INTEGER,
    `league` INTEGER,
    `is_international` TINYINT DEFAULT 0,
    `team_type` VARCHAR(255),
    `stadium_id` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tournaments
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tournaments`;

CREATE TABLE `tournaments`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `logo` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- high_profile_players
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `high_profile_players`;

CREATE TABLE `high_profile_players`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `canonical_link` VARCHAR(250) NOT NULL,
    `price_rank` INTEGER DEFAULT -1,
    `appearance` JSON,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
