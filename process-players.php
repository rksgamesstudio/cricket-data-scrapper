<?php
require "vendor/autoload.php";

use Base\HighProfilePlayersQuery;
use Propel\Runtime\ActiveQuery\Criteria;

$teams = TeamsQuery::create()->find();

foreach ($teams as $team) {
  //playing XI

  $players = RawStreamQuery::create()
    ->filterByTeam($team->getId())
    ->filterByBattingorder(null, Criteria::NOT_EQUAL)
    ->orderByBattingorder()
    ->find();
  processRawStream($players);

  $reserves =  RawStreamQuery::create()
    ->filterByTeam($team->getId())
    ->filterByBattingorder(null, Criteria::EQUAL)
    ->orderByBattingorder()
    ->find();

  processRawStream($reserves);
}



// $rawStreamEntries = RawStreamQuery::create()->find();
function processRawStream($rawStreamEntries)
{
  foreach ($rawStreamEntries as $entry) {
    echo "Entry " . $entry->getId() . ". " . $entry->getLink() . " \n";
    //check if player already exists
    $playerId = GetPlayerId($entry->getLink());
    if ($playerId == -1) {
      $playerId = CreateNewPlayer($entry);
    }
    //add the association
    $playerAssociation = new PlayerTeams();
    $playerAssociation->setPlayer($playerId);
    $playerAssociation->setTeam($entry->getTeam());
    $playerAssociation->save();
  }
}



function GetPlayerId($source)
{
  $player = PlayersQuery::create()->findOneBySourceurl($source);
  if ($player) {
    return $player->getId();
  } else {
    return -1;
  }
}

function CreateNewPlayer(RawStream $entry)
{
  $player = new Players();
  $player->setFirstname(trim(substr($entry->getName(), 0, strripos($entry->getName(), " "))));
  $player->setLastname(trim(substr($entry->getName(), strripos($entry->getName(), " "))));
  $player->setJerseynumber(rand(1, 99));
  $player->setJerseyname(substr($entry->getName(), strripos($entry->getName(), " ")));
  $player->setBattinghand(GetHandType($entry->getBattinghand()));
  $player->setBowlinghand(GetHandType($entry->getBowlinghand()));
  $player->setFieldinghand(GetHandType($entry->getBowlinghand()));
  $player->setPrimaryrole($entry->getPrimaryrole());
  $player->setBattingorder($entry->getBattingorder());
  $player->setSourceurl($entry->getLink());
  $player->setTransfervalue($entry->getValue());
  $player->save();
  $playerId = $player->getId();

  AddPlayerBatting($playerId, $entry);
  AddPlayerBowling($playerId, $entry);
  AddPlayerFielding($playerId, $entry);
  //check if this is a high value player and has appearance

  AddAppearance($playerId, $entry);
  return $playerId;
}

function AddAppearance($playerId, RawStream $entry)
{
  //check if the player already has a appearance_record
  $highProfileQuery =  HighProfilePlayersQuery::create()
    ->filterByCanonicalLink($entry->getLink())
    ->find();
  $appearance = null;
  if ($highProfileQuery->count()) {
    $appearance = $highProfileQuery->getFirst()->getAppearance(false);
  }

  AddPlayerAppearance($playerId, $entry, $appearance);
  AddPlayerFaceAppearance($playerId, $entry, $appearance);
}
function AddPlayerAppearance($playerId, RawStream $entry, $appearance)
{

  $playerAppearance = new PlayerAppearance();
  $playerAppearance->setPlayer($playerId);
  if ($appearance != null) {
    //TODO: Add specific player customization
    $playerAppearance->setHairStyleId($appearance->hairStyleId);
    $playerAppearance->setBeardStyleId($appearance->beardStyleId);
    $playerAppearance->setHairColorId($appearance->hairColorId);
    $playerAppearance->setBeardColorId($appearance->beardColorId);
    $playerAppearance->setNeckTatooId($appearance->neckTatooId);
    $playerAppearance->setLeftArmTatooId($appearance->leftArmTatooId);
    $playerAppearance->setRightArmTatooId($appearance->rightArmTatooId);
    $playerAppearance->setskinColorId($appearance->skinColorId);
  } else {
    $playerAppearance->setHairStyleId(AppearanceHelper::getHairStyleId($entry));
    $playerAppearance->setBeardStyleId(AppearanceHelper::getBeardStyleId($entry));
    $playerAppearance->setHairColorId(AppearanceHelper::getHairColorId($entry));
    $playerAppearance->setBeardColorId(AppearanceHelper::getHairColorId($entry));
    $playerAppearance->setNeckTatooId(AppearanceHelper::getNeckTatooId($entry));
    $playerAppearance->setLeftArmTatooId(AppearanceHelper::getArmTatooId($entry));
    $playerAppearance->setRightArmTatooId(AppearanceHelper::getArmTatooId($entry));
    $playerAppearance->setskinColorId(AppearanceHelper::getSkinColorId($entry));
  }
  $playerAppearance->save();
}

function AddPlayerFaceAppearance($playerId, $entry, $appearance)
{
  $playerFaceAppearance = new PlayerFaceAppearance();
  $playerFaceAppearance->setPlayer($playerId);
  if ($appearance != null) {
    $playerFaceAppearance->setChin($appearance->faceAppearance->chin);
    $playerFaceAppearance->setCheeks($appearance->faceAppearance->cheeks);
    $playerFaceAppearance->setJaw($appearance->faceAppearance->jaw);
    $playerFaceAppearance->setLips($appearance->faceAppearance->lips);
    $playerFaceAppearance->setNoseHeight($appearance->faceAppearance->noseHeight);
    $playerFaceAppearance->setNoseWidth($appearance->faceAppearance->noseWidth);
  } else {
    $playerFaceAppearance->setChin(FaceAppearanceHelper::GetFaceFeatureValue($entry, "chin"));
    $playerFaceAppearance->setCheeks(FaceAppearanceHelper::GetFaceFeatureValue($entry, "cheeks"));
    $playerFaceAppearance->setJaw(FaceAppearanceHelper::GetFaceFeatureValue($entry, "jaw"));
    $playerFaceAppearance->setLips(FaceAppearanceHelper::GetFaceFeatureValue($entry, "lips"));
    $playerFaceAppearance->setNoseHeight(FaceAppearanceHelper::GetFaceFeatureValue($entry, "noseHeight"));
    $playerFaceAppearance->setNoseWidth(FaceAppearanceHelper::GetFaceFeatureValue($entry, "noseWidth"));
  }

  $playerFaceAppearance->save();
}

function AddPlayerBatting($playerId, $entry)
{
  $playerBatting = new Batting();
  $playerBatting->setPlayer($playerId);
  $playerBatting->setAggression(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setAverage(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setRunningspeed(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShortballstrength(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotaccurecy(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotjudgement(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotmedium(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotpower(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotspeed(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setShotspinner(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setSquarestrength(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setStraightstrength(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->setTemprament(ShiftNumberRandom($entry->getBatting()));
  $playerBatting->save();
}

function AddPlayerBowling($playerId, $entry)
{
  $playerBowling = new Bowling();
  $playerBowling->setPlayer($playerId);
  $playerBowling->setAverage($entry->getBowling());
  $playerBowling->setBowlaccurecy(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setBowlertype(GetBowlerTypeId($entry->getBowlerType()));
  $playerBowling->setBowlspeed(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setBowlspin(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setBowlswing(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setFullballstrength(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setLengthballstrength(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setShortballstrength(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->setSpinType(GetPlayerSpinType($entry->getSpinType()));
  $playerBowling->setTemprament(ShiftNumberRandom($entry->getBowling()));
  $playerBowling->save();
}

function AddPlayerFielding($playerId, $entry)
{
  $playerFielding = new Fielding();
  $playerFielding->setPlayer($playerId);
  $playerFielding->setThrowaccurecy(ShiftNumberRandom($entry->GetOverallAverage()));
  $playerFielding->setThrowspeed(ShiftNumberRandom($entry->GetOverallAverage()));
  $playerFielding->setCatchreliability(ShiftNumberRandom($entry->GetOverallAverage()));
  $playerFielding->save();
}


function ShiftNumberRandom($value)
{
  $key =  rand($value - 10, $value + 10);
  if($key > 95){
    $key = 95;
  }
  return $key;
}

function GetHandType($hand)
{
  $type = 0;
  if ($hand == "Right") {
    $type = 1;
  }
  return $type;
}

function GetPlayerSpinType($spinType)
{
  $type = 0;
  if ($spinType == "Off") {
    $type = 1;
  }
  if ($spinType == "Leg") {
    $type = 2;
  }
  return $type;
}

function GetBowlerTypeId($bowlerType)
{
  $id = 0;
  if ($bowlerType == "Medium") {
    $id = 1;
  }
  if ($bowlerType == "Spin") {
    $id = 2;
  }
  return $id;
}
