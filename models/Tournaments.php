<?php

use Base\Tournaments as BaseTournaments;

/**
 * Skeleton subclass for representing a row from the 'tournaments' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Tournaments extends BaseTournaments
{

}
