<?php

use Base\RawStream as BaseRawStream;

/**
 * Skeleton subclass for representing a row from the 'raw_stream' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RawStream extends BaseRawStream
{
    public function GetOverallAverage() {
        return (parent::getBatting() + parent::getBowling()) / 2;
    }

    public function getBattingorder() {
        $battingOrder = parent::getBattingorder();
        if($battingOrder == 0 || $battingOrder == null) {
            switch(parent::getPrimaryRole()){
                case 'batsmen':
                    $battingOrder = rand(1, 5);
                    break;

                case 'allRounder':
                    $battingOrder = rand(5, 8);
                    break;

                case 'keeper':
                    $battingOrder = rand(1, 6);
                    break;
                
                case 'bowler':
                    $battingOrder = rand(7, 11);
                    break;

                default:
                    $battingOrder = rand(7, 11);
                    break;
            }
        }

        return $battingOrder;
    }


    public function setPlayerValue()
    {
        //check if we are dealing with which role
        switch(parent::getPrimaryRole()) {
            case 'batsmen':
                parent::setValue($this->getBatsmanValue());
                break;

            case 'allRounder':
                parent::setValue($this->getAllRounderValue());
                break;

            case 'keeper':
                parent::setValue($this->getBatsmanValue());
                break;
            
            case 'bowler':
                parent::setValue($this->getBowlerValue());
                break;

            default:
                parent::setValue($this->getAllRounderValue());
                break;
        }

    }
    

    private function getBowlerValue()
    {
        $ageFactor = 0.1;
        $paceFactor = 0.2;
        $skillFactor = 0.6;
        return ((1 - abs((parent::getAge() - 30) / 10)) * $ageFactor * 100) 
            + (parent::getBowling() * $skillFactor) 
            + ((parent::getBowlertype() == "Fast" ? 1 : 0) * $paceFactor * 100);
    }

    private function getBatsmanValue() 
    {
        $ageFactor = 0.1;
        $skillFactor = 0.8;
        return ((1 - abs((parent::getAge() - 30) / 10)) * $ageFactor * 100) + (parent::getBatting() * $skillFactor);

    }

    private function getAllRounderValue()
    {

        $ageFactor = 0.1;
        $paceFactor = 0.2;
        $battingSkillFactor = 0.32;
        $bowlingSkillFactor = 0.32;

        return ((1 - abs((parent::getAge() - 30) / 10)) * $ageFactor * 100) 
            + (parent::getBatting() * $battingSkillFactor) 
            + (parent::getBowling() * $bowlingSkillFactor) 
            + ((parent::getBowlertype() == "Fast" ? 1 : 0) * $paceFactor * 100);

    }

}
