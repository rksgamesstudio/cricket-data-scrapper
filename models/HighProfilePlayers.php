<?php

use Base\HighProfilePlayers as BaseHighProfilePlayers;

/**
 * Skeleton subclass for representing a row from the 'high_profile_players' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class HighProfilePlayers extends BaseHighProfilePlayers
{

}
