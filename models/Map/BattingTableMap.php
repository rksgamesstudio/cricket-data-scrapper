<?php

namespace Map;

use \Batting;
use \BattingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'batting' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BattingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.BattingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'batting';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Batting';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Batting';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'batting.id';

    /**
     * the column name for the player field
     */
    const COL_PLAYER = 'batting.player';

    /**
     * the column name for the shotPower field
     */
    const COL_SHOTPOWER = 'batting.shotPower';

    /**
     * the column name for the shotAccurecy field
     */
    const COL_SHOTACCURECY = 'batting.shotAccurecy';

    /**
     * the column name for the shotJudgement field
     */
    const COL_SHOTJUDGEMENT = 'batting.shotJudgement';

    /**
     * the column name for the shotSpeed field
     */
    const COL_SHOTSPEED = 'batting.shotSpeed';

    /**
     * the column name for the shotMedium field
     */
    const COL_SHOTMEDIUM = 'batting.shotMedium';

    /**
     * the column name for the shotSpinner field
     */
    const COL_SHOTSPINNER = 'batting.shotSpinner';

    /**
     * the column name for the runningSpeed field
     */
    const COL_RUNNINGSPEED = 'batting.runningSpeed';

    /**
     * the column name for the shortBallStrength field
     */
    const COL_SHORTBALLSTRENGTH = 'batting.shortBallStrength';

    /**
     * the column name for the lengthBallStrength field
     */
    const COL_LENGTHBALLSTRENGTH = 'batting.lengthBallStrength';

    /**
     * the column name for the fullBallStrength field
     */
    const COL_FULLBALLSTRENGTH = 'batting.fullBallStrength';

    /**
     * the column name for the temprament field
     */
    const COL_TEMPRAMENT = 'batting.temprament';

    /**
     * the column name for the aggression field
     */
    const COL_AGGRESSION = 'batting.aggression';

    /**
     * the column name for the straightStrength field
     */
    const COL_STRAIGHTSTRENGTH = 'batting.straightStrength';

    /**
     * the column name for the squareStrength field
     */
    const COL_SQUARESTRENGTH = 'batting.squareStrength';

    /**
     * the column name for the average field
     */
    const COL_AVERAGE = 'batting.average';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Player', 'Shotpower', 'Shotaccurecy', 'Shotjudgement', 'Shotspeed', 'Shotmedium', 'Shotspinner', 'Runningspeed', 'Shortballstrength', 'Lengthballstrength', 'Fullballstrength', 'Temprament', 'Aggression', 'Straightstrength', 'Squarestrength', 'Average', ),
        self::TYPE_CAMELNAME     => array('id', 'player', 'shotpower', 'shotaccurecy', 'shotjudgement', 'shotspeed', 'shotmedium', 'shotspinner', 'runningspeed', 'shortballstrength', 'lengthballstrength', 'fullballstrength', 'temprament', 'aggression', 'straightstrength', 'squarestrength', 'average', ),
        self::TYPE_COLNAME       => array(BattingTableMap::COL_ID, BattingTableMap::COL_PLAYER, BattingTableMap::COL_SHOTPOWER, BattingTableMap::COL_SHOTACCURECY, BattingTableMap::COL_SHOTJUDGEMENT, BattingTableMap::COL_SHOTSPEED, BattingTableMap::COL_SHOTMEDIUM, BattingTableMap::COL_SHOTSPINNER, BattingTableMap::COL_RUNNINGSPEED, BattingTableMap::COL_SHORTBALLSTRENGTH, BattingTableMap::COL_LENGTHBALLSTRENGTH, BattingTableMap::COL_FULLBALLSTRENGTH, BattingTableMap::COL_TEMPRAMENT, BattingTableMap::COL_AGGRESSION, BattingTableMap::COL_STRAIGHTSTRENGTH, BattingTableMap::COL_SQUARESTRENGTH, BattingTableMap::COL_AVERAGE, ),
        self::TYPE_FIELDNAME     => array('id', 'player', 'shotPower', 'shotAccurecy', 'shotJudgement', 'shotSpeed', 'shotMedium', 'shotSpinner', 'runningSpeed', 'shortBallStrength', 'lengthBallStrength', 'fullBallStrength', 'temprament', 'aggression', 'straightStrength', 'squareStrength', 'average', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Player' => 1, 'Shotpower' => 2, 'Shotaccurecy' => 3, 'Shotjudgement' => 4, 'Shotspeed' => 5, 'Shotmedium' => 6, 'Shotspinner' => 7, 'Runningspeed' => 8, 'Shortballstrength' => 9, 'Lengthballstrength' => 10, 'Fullballstrength' => 11, 'Temprament' => 12, 'Aggression' => 13, 'Straightstrength' => 14, 'Squarestrength' => 15, 'Average' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'player' => 1, 'shotpower' => 2, 'shotaccurecy' => 3, 'shotjudgement' => 4, 'shotspeed' => 5, 'shotmedium' => 6, 'shotspinner' => 7, 'runningspeed' => 8, 'shortballstrength' => 9, 'lengthballstrength' => 10, 'fullballstrength' => 11, 'temprament' => 12, 'aggression' => 13, 'straightstrength' => 14, 'squarestrength' => 15, 'average' => 16, ),
        self::TYPE_COLNAME       => array(BattingTableMap::COL_ID => 0, BattingTableMap::COL_PLAYER => 1, BattingTableMap::COL_SHOTPOWER => 2, BattingTableMap::COL_SHOTACCURECY => 3, BattingTableMap::COL_SHOTJUDGEMENT => 4, BattingTableMap::COL_SHOTSPEED => 5, BattingTableMap::COL_SHOTMEDIUM => 6, BattingTableMap::COL_SHOTSPINNER => 7, BattingTableMap::COL_RUNNINGSPEED => 8, BattingTableMap::COL_SHORTBALLSTRENGTH => 9, BattingTableMap::COL_LENGTHBALLSTRENGTH => 10, BattingTableMap::COL_FULLBALLSTRENGTH => 11, BattingTableMap::COL_TEMPRAMENT => 12, BattingTableMap::COL_AGGRESSION => 13, BattingTableMap::COL_STRAIGHTSTRENGTH => 14, BattingTableMap::COL_SQUARESTRENGTH => 15, BattingTableMap::COL_AVERAGE => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'player' => 1, 'shotPower' => 2, 'shotAccurecy' => 3, 'shotJudgement' => 4, 'shotSpeed' => 5, 'shotMedium' => 6, 'shotSpinner' => 7, 'runningSpeed' => 8, 'shortBallStrength' => 9, 'lengthBallStrength' => 10, 'fullBallStrength' => 11, 'temprament' => 12, 'aggression' => 13, 'straightStrength' => 14, 'squareStrength' => 15, 'average' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('batting');
        $this->setPhpName('Batting');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Batting');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('player', 'Player', 'INTEGER', false, null, null);
        $this->addColumn('shotPower', 'Shotpower', 'INTEGER', false, null, 90);
        $this->addColumn('shotAccurecy', 'Shotaccurecy', 'INTEGER', false, null, 90);
        $this->addColumn('shotJudgement', 'Shotjudgement', 'INTEGER', false, null, 90);
        $this->addColumn('shotSpeed', 'Shotspeed', 'INTEGER', false, null, 90);
        $this->addColumn('shotMedium', 'Shotmedium', 'INTEGER', false, null, 90);
        $this->addColumn('shotSpinner', 'Shotspinner', 'INTEGER', false, null, 90);
        $this->addColumn('runningSpeed', 'Runningspeed', 'INTEGER', false, null, 90);
        $this->addColumn('shortBallStrength', 'Shortballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('lengthBallStrength', 'Lengthballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('fullBallStrength', 'Fullballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('temprament', 'Temprament', 'INTEGER', false, null, 90);
        $this->addColumn('aggression', 'Aggression', 'INTEGER', false, null, null);
        $this->addColumn('straightStrength', 'Straightstrength', 'INTEGER', false, null, null);
        $this->addColumn('squareStrength', 'Squarestrength', 'INTEGER', false, null, null);
        $this->addColumn('average', 'Average', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BattingTableMap::CLASS_DEFAULT : BattingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Batting object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BattingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BattingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BattingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BattingTableMap::OM_CLASS;
            /** @var Batting $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BattingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BattingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BattingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Batting $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BattingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BattingTableMap::COL_ID);
            $criteria->addSelectColumn(BattingTableMap::COL_PLAYER);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTPOWER);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTACCURECY);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTJUDGEMENT);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTSPEED);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTMEDIUM);
            $criteria->addSelectColumn(BattingTableMap::COL_SHOTSPINNER);
            $criteria->addSelectColumn(BattingTableMap::COL_RUNNINGSPEED);
            $criteria->addSelectColumn(BattingTableMap::COL_SHORTBALLSTRENGTH);
            $criteria->addSelectColumn(BattingTableMap::COL_LENGTHBALLSTRENGTH);
            $criteria->addSelectColumn(BattingTableMap::COL_FULLBALLSTRENGTH);
            $criteria->addSelectColumn(BattingTableMap::COL_TEMPRAMENT);
            $criteria->addSelectColumn(BattingTableMap::COL_AGGRESSION);
            $criteria->addSelectColumn(BattingTableMap::COL_STRAIGHTSTRENGTH);
            $criteria->addSelectColumn(BattingTableMap::COL_SQUARESTRENGTH);
            $criteria->addSelectColumn(BattingTableMap::COL_AVERAGE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.player');
            $criteria->addSelectColumn($alias . '.shotPower');
            $criteria->addSelectColumn($alias . '.shotAccurecy');
            $criteria->addSelectColumn($alias . '.shotJudgement');
            $criteria->addSelectColumn($alias . '.shotSpeed');
            $criteria->addSelectColumn($alias . '.shotMedium');
            $criteria->addSelectColumn($alias . '.shotSpinner');
            $criteria->addSelectColumn($alias . '.runningSpeed');
            $criteria->addSelectColumn($alias . '.shortBallStrength');
            $criteria->addSelectColumn($alias . '.lengthBallStrength');
            $criteria->addSelectColumn($alias . '.fullBallStrength');
            $criteria->addSelectColumn($alias . '.temprament');
            $criteria->addSelectColumn($alias . '.aggression');
            $criteria->addSelectColumn($alias . '.straightStrength');
            $criteria->addSelectColumn($alias . '.squareStrength');
            $criteria->addSelectColumn($alias . '.average');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BattingTableMap::DATABASE_NAME)->getTable(BattingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BattingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BattingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BattingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Batting or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Batting object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Batting) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BattingTableMap::DATABASE_NAME);
            $criteria->add(BattingTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BattingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BattingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BattingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the batting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BattingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Batting or Criteria object.
     *
     * @param mixed               $criteria Criteria or Batting object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Batting object
        }

        if ($criteria->containsKey(BattingTableMap::COL_ID) && $criteria->keyContainsValue(BattingTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BattingTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BattingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BattingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BattingTableMap::buildTableMap();
