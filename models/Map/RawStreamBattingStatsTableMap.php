<?php

namespace Map;

use \RawStreamBattingStats;
use \RawStreamBattingStatsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'raw_stream_batting_stats' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RawStreamBattingStatsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RawStreamBattingStatsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'raw_stream_batting_stats';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\RawStreamBattingStats';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'RawStreamBattingStats';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'raw_stream_batting_stats.id';

    /**
     * the column name for the player_id field
     */
    const COL_PLAYER_ID = 'raw_stream_batting_stats.player_id';

    /**
     * the column name for the data_source field
     */
    const COL_DATA_SOURCE = 'raw_stream_batting_stats.data_source';

    /**
     * the column name for the format field
     */
    const COL_FORMAT = 'raw_stream_batting_stats.format';

    /**
     * the column name for the matches field
     */
    const COL_MATCHES = 'raw_stream_batting_stats.matches';

    /**
     * the column name for the innings field
     */
    const COL_INNINGS = 'raw_stream_batting_stats.innings';

    /**
     * the column name for the not_outs field
     */
    const COL_NOT_OUTS = 'raw_stream_batting_stats.not_outs';

    /**
     * the column name for the runs field
     */
    const COL_RUNS = 'raw_stream_batting_stats.runs';

    /**
     * the column name for the average field
     */
    const COL_AVERAGE = 'raw_stream_batting_stats.average';

    /**
     * the column name for the strike_rate field
     */
    const COL_STRIKE_RATE = 'raw_stream_batting_stats.strike_rate';

    /**
     * the column name for the centuries field
     */
    const COL_CENTURIES = 'raw_stream_batting_stats.centuries';

    /**
     * the column name for the half_centuries field
     */
    const COL_HALF_CENTURIES = 'raw_stream_batting_stats.half_centuries';

    /**
     * the column name for the fours field
     */
    const COL_FOURS = 'raw_stream_batting_stats.fours';

    /**
     * the column name for the sixes field
     */
    const COL_SIXES = 'raw_stream_batting_stats.sixes';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PlayerId', 'DataSource', 'Format', 'Matches', 'Innings', 'NotOuts', 'Runs', 'Average', 'StrikeRate', 'Centuries', 'HalfCenturies', 'Fours', 'Sixes', ),
        self::TYPE_CAMELNAME     => array('id', 'playerId', 'dataSource', 'format', 'matches', 'innings', 'notOuts', 'runs', 'average', 'strikeRate', 'centuries', 'halfCenturies', 'fours', 'sixes', ),
        self::TYPE_COLNAME       => array(RawStreamBattingStatsTableMap::COL_ID, RawStreamBattingStatsTableMap::COL_PLAYER_ID, RawStreamBattingStatsTableMap::COL_DATA_SOURCE, RawStreamBattingStatsTableMap::COL_FORMAT, RawStreamBattingStatsTableMap::COL_MATCHES, RawStreamBattingStatsTableMap::COL_INNINGS, RawStreamBattingStatsTableMap::COL_NOT_OUTS, RawStreamBattingStatsTableMap::COL_RUNS, RawStreamBattingStatsTableMap::COL_AVERAGE, RawStreamBattingStatsTableMap::COL_STRIKE_RATE, RawStreamBattingStatsTableMap::COL_CENTURIES, RawStreamBattingStatsTableMap::COL_HALF_CENTURIES, RawStreamBattingStatsTableMap::COL_FOURS, RawStreamBattingStatsTableMap::COL_SIXES, ),
        self::TYPE_FIELDNAME     => array('id', 'player_id', 'data_source', 'format', 'matches', 'innings', 'not_outs', 'runs', 'average', 'strike_rate', 'centuries', 'half_centuries', 'fours', 'sixes', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PlayerId' => 1, 'DataSource' => 2, 'Format' => 3, 'Matches' => 4, 'Innings' => 5, 'NotOuts' => 6, 'Runs' => 7, 'Average' => 8, 'StrikeRate' => 9, 'Centuries' => 10, 'HalfCenturies' => 11, 'Fours' => 12, 'Sixes' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'playerId' => 1, 'dataSource' => 2, 'format' => 3, 'matches' => 4, 'innings' => 5, 'notOuts' => 6, 'runs' => 7, 'average' => 8, 'strikeRate' => 9, 'centuries' => 10, 'halfCenturies' => 11, 'fours' => 12, 'sixes' => 13, ),
        self::TYPE_COLNAME       => array(RawStreamBattingStatsTableMap::COL_ID => 0, RawStreamBattingStatsTableMap::COL_PLAYER_ID => 1, RawStreamBattingStatsTableMap::COL_DATA_SOURCE => 2, RawStreamBattingStatsTableMap::COL_FORMAT => 3, RawStreamBattingStatsTableMap::COL_MATCHES => 4, RawStreamBattingStatsTableMap::COL_INNINGS => 5, RawStreamBattingStatsTableMap::COL_NOT_OUTS => 6, RawStreamBattingStatsTableMap::COL_RUNS => 7, RawStreamBattingStatsTableMap::COL_AVERAGE => 8, RawStreamBattingStatsTableMap::COL_STRIKE_RATE => 9, RawStreamBattingStatsTableMap::COL_CENTURIES => 10, RawStreamBattingStatsTableMap::COL_HALF_CENTURIES => 11, RawStreamBattingStatsTableMap::COL_FOURS => 12, RawStreamBattingStatsTableMap::COL_SIXES => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'player_id' => 1, 'data_source' => 2, 'format' => 3, 'matches' => 4, 'innings' => 5, 'not_outs' => 6, 'runs' => 7, 'average' => 8, 'strike_rate' => 9, 'centuries' => 10, 'half_centuries' => 11, 'fours' => 12, 'sixes' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('raw_stream_batting_stats');
        $this->setPhpName('RawStreamBattingStats');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\RawStreamBattingStats');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('player_id', 'PlayerId', 'INTEGER', false, null, null);
        $this->addColumn('data_source', 'DataSource', 'CHAR', false, null, 'cricinfo');
        $this->addColumn('format', 'Format', 'VARCHAR', false, 150, null);
        $this->addColumn('matches', 'Matches', 'INTEGER', false, null, null);
        $this->addColumn('innings', 'Innings', 'INTEGER', false, null, null);
        $this->addColumn('not_outs', 'NotOuts', 'INTEGER', false, null, null);
        $this->addColumn('runs', 'Runs', 'INTEGER', false, null, null);
        $this->addColumn('average', 'Average', 'FLOAT', false, null, null);
        $this->addColumn('strike_rate', 'StrikeRate', 'FLOAT', false, null, null);
        $this->addColumn('centuries', 'Centuries', 'INTEGER', false, null, null);
        $this->addColumn('half_centuries', 'HalfCenturies', 'INTEGER', false, null, null);
        $this->addColumn('fours', 'Fours', 'INTEGER', false, null, null);
        $this->addColumn('sixes', 'Sixes', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RawStreamBattingStatsTableMap::CLASS_DEFAULT : RawStreamBattingStatsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RawStreamBattingStats object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RawStreamBattingStatsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RawStreamBattingStatsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RawStreamBattingStatsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RawStreamBattingStatsTableMap::OM_CLASS;
            /** @var RawStreamBattingStats $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RawStreamBattingStatsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RawStreamBattingStatsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RawStreamBattingStatsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RawStreamBattingStats $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RawStreamBattingStatsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_ID);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_PLAYER_ID);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_DATA_SOURCE);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_FORMAT);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_MATCHES);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_INNINGS);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_NOT_OUTS);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_RUNS);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_AVERAGE);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_STRIKE_RATE);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_CENTURIES);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_HALF_CENTURIES);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_FOURS);
            $criteria->addSelectColumn(RawStreamBattingStatsTableMap::COL_SIXES);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.player_id');
            $criteria->addSelectColumn($alias . '.data_source');
            $criteria->addSelectColumn($alias . '.format');
            $criteria->addSelectColumn($alias . '.matches');
            $criteria->addSelectColumn($alias . '.innings');
            $criteria->addSelectColumn($alias . '.not_outs');
            $criteria->addSelectColumn($alias . '.runs');
            $criteria->addSelectColumn($alias . '.average');
            $criteria->addSelectColumn($alias . '.strike_rate');
            $criteria->addSelectColumn($alias . '.centuries');
            $criteria->addSelectColumn($alias . '.half_centuries');
            $criteria->addSelectColumn($alias . '.fours');
            $criteria->addSelectColumn($alias . '.sixes');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RawStreamBattingStatsTableMap::DATABASE_NAME)->getTable(RawStreamBattingStatsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RawStreamBattingStatsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RawStreamBattingStatsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RawStreamBattingStatsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RawStreamBattingStats or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RawStreamBattingStats object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBattingStatsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \RawStreamBattingStats) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RawStreamBattingStatsTableMap::DATABASE_NAME);
            $criteria->add(RawStreamBattingStatsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = RawStreamBattingStatsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RawStreamBattingStatsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RawStreamBattingStatsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the raw_stream_batting_stats table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RawStreamBattingStatsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RawStreamBattingStats or Criteria object.
     *
     * @param mixed               $criteria Criteria or RawStreamBattingStats object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBattingStatsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RawStreamBattingStats object
        }

        if ($criteria->containsKey(RawStreamBattingStatsTableMap::COL_ID) && $criteria->keyContainsValue(RawStreamBattingStatsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RawStreamBattingStatsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = RawStreamBattingStatsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RawStreamBattingStatsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RawStreamBattingStatsTableMap::buildTableMap();
