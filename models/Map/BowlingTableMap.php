<?php

namespace Map;

use \Bowling;
use \BowlingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'bowling' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BowlingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.BowlingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'bowling';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Bowling';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Bowling';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'bowling.id';

    /**
     * the column name for the player field
     */
    const COL_PLAYER = 'bowling.player';

    /**
     * the column name for the bowlerType field
     */
    const COL_BOWLERTYPE = 'bowling.bowlerType';

    /**
     * the column name for the bowlSpeed field
     */
    const COL_BOWLSPEED = 'bowling.bowlSpeed';

    /**
     * the column name for the bowlSwing field
     */
    const COL_BOWLSWING = 'bowling.bowlSwing';

    /**
     * the column name for the bowlSpin field
     */
    const COL_BOWLSPIN = 'bowling.bowlSpin';

    /**
     * the column name for the spinType field
     */
    const COL_SPINTYPE = 'bowling.spinType';

    /**
     * the column name for the bowlAccurecy field
     */
    const COL_BOWLACCURECY = 'bowling.bowlAccurecy';

    /**
     * the column name for the shortBallStrength field
     */
    const COL_SHORTBALLSTRENGTH = 'bowling.shortBallStrength';

    /**
     * the column name for the lengthBallStrength field
     */
    const COL_LENGTHBALLSTRENGTH = 'bowling.lengthBallStrength';

    /**
     * the column name for the fullBallStrength field
     */
    const COL_FULLBALLSTRENGTH = 'bowling.fullBallStrength';

    /**
     * the column name for the temprament field
     */
    const COL_TEMPRAMENT = 'bowling.temprament';

    /**
     * the column name for the average field
     */
    const COL_AVERAGE = 'bowling.average';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Player', 'Bowlertype', 'Bowlspeed', 'Bowlswing', 'Bowlspin', 'Spintype', 'Bowlaccurecy', 'Shortballstrength', 'Lengthballstrength', 'Fullballstrength', 'Temprament', 'Average', ),
        self::TYPE_CAMELNAME     => array('id', 'player', 'bowlertype', 'bowlspeed', 'bowlswing', 'bowlspin', 'spintype', 'bowlaccurecy', 'shortballstrength', 'lengthballstrength', 'fullballstrength', 'temprament', 'average', ),
        self::TYPE_COLNAME       => array(BowlingTableMap::COL_ID, BowlingTableMap::COL_PLAYER, BowlingTableMap::COL_BOWLERTYPE, BowlingTableMap::COL_BOWLSPEED, BowlingTableMap::COL_BOWLSWING, BowlingTableMap::COL_BOWLSPIN, BowlingTableMap::COL_SPINTYPE, BowlingTableMap::COL_BOWLACCURECY, BowlingTableMap::COL_SHORTBALLSTRENGTH, BowlingTableMap::COL_LENGTHBALLSTRENGTH, BowlingTableMap::COL_FULLBALLSTRENGTH, BowlingTableMap::COL_TEMPRAMENT, BowlingTableMap::COL_AVERAGE, ),
        self::TYPE_FIELDNAME     => array('id', 'player', 'bowlerType', 'bowlSpeed', 'bowlSwing', 'bowlSpin', 'spinType', 'bowlAccurecy', 'shortBallStrength', 'lengthBallStrength', 'fullBallStrength', 'temprament', 'average', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Player' => 1, 'Bowlertype' => 2, 'Bowlspeed' => 3, 'Bowlswing' => 4, 'Bowlspin' => 5, 'Spintype' => 6, 'Bowlaccurecy' => 7, 'Shortballstrength' => 8, 'Lengthballstrength' => 9, 'Fullballstrength' => 10, 'Temprament' => 11, 'Average' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'player' => 1, 'bowlertype' => 2, 'bowlspeed' => 3, 'bowlswing' => 4, 'bowlspin' => 5, 'spintype' => 6, 'bowlaccurecy' => 7, 'shortballstrength' => 8, 'lengthballstrength' => 9, 'fullballstrength' => 10, 'temprament' => 11, 'average' => 12, ),
        self::TYPE_COLNAME       => array(BowlingTableMap::COL_ID => 0, BowlingTableMap::COL_PLAYER => 1, BowlingTableMap::COL_BOWLERTYPE => 2, BowlingTableMap::COL_BOWLSPEED => 3, BowlingTableMap::COL_BOWLSWING => 4, BowlingTableMap::COL_BOWLSPIN => 5, BowlingTableMap::COL_SPINTYPE => 6, BowlingTableMap::COL_BOWLACCURECY => 7, BowlingTableMap::COL_SHORTBALLSTRENGTH => 8, BowlingTableMap::COL_LENGTHBALLSTRENGTH => 9, BowlingTableMap::COL_FULLBALLSTRENGTH => 10, BowlingTableMap::COL_TEMPRAMENT => 11, BowlingTableMap::COL_AVERAGE => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'player' => 1, 'bowlerType' => 2, 'bowlSpeed' => 3, 'bowlSwing' => 4, 'bowlSpin' => 5, 'spinType' => 6, 'bowlAccurecy' => 7, 'shortBallStrength' => 8, 'lengthBallStrength' => 9, 'fullBallStrength' => 10, 'temprament' => 11, 'average' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bowling');
        $this->setPhpName('Bowling');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Bowling');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('player', 'Player', 'INTEGER', false, null, null);
        $this->addColumn('bowlerType', 'Bowlertype', 'VARCHAR', false, 45, 'Fast');
        $this->addColumn('bowlSpeed', 'Bowlspeed', 'INTEGER', false, null, 90);
        $this->addColumn('bowlSwing', 'Bowlswing', 'INTEGER', false, null, 90);
        $this->addColumn('bowlSpin', 'Bowlspin', 'INTEGER', false, null, 90);
        $this->addColumn('spinType', 'Spintype', 'VARCHAR', false, 45, '90');
        $this->addColumn('bowlAccurecy', 'Bowlaccurecy', 'INTEGER', false, null, 90);
        $this->addColumn('shortBallStrength', 'Shortballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('lengthBallStrength', 'Lengthballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('fullBallStrength', 'Fullballstrength', 'INTEGER', false, null, 90);
        $this->addColumn('temprament', 'Temprament', 'INTEGER', false, null, 90);
        $this->addColumn('average', 'Average', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BowlingTableMap::CLASS_DEFAULT : BowlingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Bowling object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BowlingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BowlingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BowlingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BowlingTableMap::OM_CLASS;
            /** @var Bowling $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BowlingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BowlingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BowlingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Bowling $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BowlingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BowlingTableMap::COL_ID);
            $criteria->addSelectColumn(BowlingTableMap::COL_PLAYER);
            $criteria->addSelectColumn(BowlingTableMap::COL_BOWLERTYPE);
            $criteria->addSelectColumn(BowlingTableMap::COL_BOWLSPEED);
            $criteria->addSelectColumn(BowlingTableMap::COL_BOWLSWING);
            $criteria->addSelectColumn(BowlingTableMap::COL_BOWLSPIN);
            $criteria->addSelectColumn(BowlingTableMap::COL_SPINTYPE);
            $criteria->addSelectColumn(BowlingTableMap::COL_BOWLACCURECY);
            $criteria->addSelectColumn(BowlingTableMap::COL_SHORTBALLSTRENGTH);
            $criteria->addSelectColumn(BowlingTableMap::COL_LENGTHBALLSTRENGTH);
            $criteria->addSelectColumn(BowlingTableMap::COL_FULLBALLSTRENGTH);
            $criteria->addSelectColumn(BowlingTableMap::COL_TEMPRAMENT);
            $criteria->addSelectColumn(BowlingTableMap::COL_AVERAGE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.player');
            $criteria->addSelectColumn($alias . '.bowlerType');
            $criteria->addSelectColumn($alias . '.bowlSpeed');
            $criteria->addSelectColumn($alias . '.bowlSwing');
            $criteria->addSelectColumn($alias . '.bowlSpin');
            $criteria->addSelectColumn($alias . '.spinType');
            $criteria->addSelectColumn($alias . '.bowlAccurecy');
            $criteria->addSelectColumn($alias . '.shortBallStrength');
            $criteria->addSelectColumn($alias . '.lengthBallStrength');
            $criteria->addSelectColumn($alias . '.fullBallStrength');
            $criteria->addSelectColumn($alias . '.temprament');
            $criteria->addSelectColumn($alias . '.average');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BowlingTableMap::DATABASE_NAME)->getTable(BowlingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BowlingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BowlingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BowlingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Bowling or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Bowling object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BowlingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Bowling) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BowlingTableMap::DATABASE_NAME);
            $criteria->add(BowlingTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BowlingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BowlingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BowlingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the bowling table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BowlingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Bowling or Criteria object.
     *
     * @param mixed               $criteria Criteria or Bowling object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BowlingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Bowling object
        }

        if ($criteria->containsKey(BowlingTableMap::COL_ID) && $criteria->keyContainsValue(BowlingTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BowlingTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BowlingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BowlingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BowlingTableMap::buildTableMap();
