<?php

namespace Map;

use \Players;
use \PlayersQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'players' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PlayersTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PlayersTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'players';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Players';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Players';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the id field
     */
    const COL_ID = 'players.id';

    /**
     * the column name for the firstName field
     */
    const COL_FIRSTNAME = 'players.firstName';

    /**
     * the column name for the lastName field
     */
    const COL_LASTNAME = 'players.lastName';

    /**
     * the column name for the jerseyNumber field
     */
    const COL_JERSEYNUMBER = 'players.jerseyNumber';

    /**
     * the column name for the jerseyName field
     */
    const COL_JERSEYNAME = 'players.jerseyName';

    /**
     * the column name for the battingHand field
     */
    const COL_BATTINGHAND = 'players.battingHand';

    /**
     * the column name for the bowlingHand field
     */
    const COL_BOWLINGHAND = 'players.bowlingHand';

    /**
     * the column name for the fieldingHand field
     */
    const COL_FIELDINGHAND = 'players.fieldingHand';

    /**
     * the column name for the modelName field
     */
    const COL_MODELNAME = 'players.modelName';

    /**
     * the column name for the modelTexture field
     */
    const COL_MODELTEXTURE = 'players.modelTexture';

    /**
     * the column name for the primaryRole field
     */
    const COL_PRIMARYROLE = 'players.primaryRole';

    /**
     * the column name for the battingOrder field
     */
    const COL_BATTINGORDER = 'players.battingOrder';

    /**
     * the column name for the race field
     */
    const COL_RACE = 'players.race';

    /**
     * the column name for the sourceUrl field
     */
    const COL_SOURCEURL = 'players.sourceUrl';

    /**
     * the column name for the transferValue field
     */
    const COL_TRANSFERVALUE = 'players.transferValue';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Firstname', 'Lastname', 'Jerseynumber', 'Jerseyname', 'Battinghand', 'Bowlinghand', 'Fieldinghand', 'Modelname', 'Modeltexture', 'Primaryrole', 'Battingorder', 'Race', 'Sourceurl', 'Transfervalue', ),
        self::TYPE_CAMELNAME     => array('id', 'firstname', 'lastname', 'jerseynumber', 'jerseyname', 'battinghand', 'bowlinghand', 'fieldinghand', 'modelname', 'modeltexture', 'primaryrole', 'battingorder', 'race', 'sourceurl', 'transfervalue', ),
        self::TYPE_COLNAME       => array(PlayersTableMap::COL_ID, PlayersTableMap::COL_FIRSTNAME, PlayersTableMap::COL_LASTNAME, PlayersTableMap::COL_JERSEYNUMBER, PlayersTableMap::COL_JERSEYNAME, PlayersTableMap::COL_BATTINGHAND, PlayersTableMap::COL_BOWLINGHAND, PlayersTableMap::COL_FIELDINGHAND, PlayersTableMap::COL_MODELNAME, PlayersTableMap::COL_MODELTEXTURE, PlayersTableMap::COL_PRIMARYROLE, PlayersTableMap::COL_BATTINGORDER, PlayersTableMap::COL_RACE, PlayersTableMap::COL_SOURCEURL, PlayersTableMap::COL_TRANSFERVALUE, ),
        self::TYPE_FIELDNAME     => array('id', 'firstName', 'lastName', 'jerseyNumber', 'jerseyName', 'battingHand', 'bowlingHand', 'fieldingHand', 'modelName', 'modelTexture', 'primaryRole', 'battingOrder', 'race', 'sourceUrl', 'transferValue', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Firstname' => 1, 'Lastname' => 2, 'Jerseynumber' => 3, 'Jerseyname' => 4, 'Battinghand' => 5, 'Bowlinghand' => 6, 'Fieldinghand' => 7, 'Modelname' => 8, 'Modeltexture' => 9, 'Primaryrole' => 10, 'Battingorder' => 11, 'Race' => 12, 'Sourceurl' => 13, 'Transfervalue' => 14, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'firstname' => 1, 'lastname' => 2, 'jerseynumber' => 3, 'jerseyname' => 4, 'battinghand' => 5, 'bowlinghand' => 6, 'fieldinghand' => 7, 'modelname' => 8, 'modeltexture' => 9, 'primaryrole' => 10, 'battingorder' => 11, 'race' => 12, 'sourceurl' => 13, 'transfervalue' => 14, ),
        self::TYPE_COLNAME       => array(PlayersTableMap::COL_ID => 0, PlayersTableMap::COL_FIRSTNAME => 1, PlayersTableMap::COL_LASTNAME => 2, PlayersTableMap::COL_JERSEYNUMBER => 3, PlayersTableMap::COL_JERSEYNAME => 4, PlayersTableMap::COL_BATTINGHAND => 5, PlayersTableMap::COL_BOWLINGHAND => 6, PlayersTableMap::COL_FIELDINGHAND => 7, PlayersTableMap::COL_MODELNAME => 8, PlayersTableMap::COL_MODELTEXTURE => 9, PlayersTableMap::COL_PRIMARYROLE => 10, PlayersTableMap::COL_BATTINGORDER => 11, PlayersTableMap::COL_RACE => 12, PlayersTableMap::COL_SOURCEURL => 13, PlayersTableMap::COL_TRANSFERVALUE => 14, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'firstName' => 1, 'lastName' => 2, 'jerseyNumber' => 3, 'jerseyName' => 4, 'battingHand' => 5, 'bowlingHand' => 6, 'fieldingHand' => 7, 'modelName' => 8, 'modelTexture' => 9, 'primaryRole' => 10, 'battingOrder' => 11, 'race' => 12, 'sourceUrl' => 13, 'transferValue' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('players');
        $this->setPhpName('Players');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Players');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('firstName', 'Firstname', 'VARCHAR', false, 45, null);
        $this->addColumn('lastName', 'Lastname', 'VARCHAR', false, 45, null);
        $this->addColumn('jerseyNumber', 'Jerseynumber', 'VARCHAR', false, 45, null);
        $this->addColumn('jerseyName', 'Jerseyname', 'VARCHAR', false, 45, null);
        $this->addColumn('battingHand', 'Battinghand', 'VARCHAR', false, 45, null);
        $this->addColumn('bowlingHand', 'Bowlinghand', 'VARCHAR', false, 45, null);
        $this->addColumn('fieldingHand', 'Fieldinghand', 'VARCHAR', false, 45, null);
        $this->addColumn('modelName', 'Modelname', 'VARCHAR', false, 45, null);
        $this->addColumn('modelTexture', 'Modeltexture', 'VARCHAR', false, 45, null);
        $this->addColumn('primaryRole', 'Primaryrole', 'CHAR', false, null, 'allRounder');
        $this->addColumn('battingOrder', 'Battingorder', 'INTEGER', false, null, null);
        $this->addColumn('race', 'Race', 'CHAR', false, null, 'indian');
        $this->addColumn('sourceUrl', 'Sourceurl', 'VARCHAR', false, 350, null);
        $this->addColumn('transferValue', 'Transfervalue', 'FLOAT', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PlayersTableMap::CLASS_DEFAULT : PlayersTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Players object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PlayersTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PlayersTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PlayersTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PlayersTableMap::OM_CLASS;
            /** @var Players $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PlayersTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PlayersTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PlayersTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Players $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PlayersTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PlayersTableMap::COL_ID);
            $criteria->addSelectColumn(PlayersTableMap::COL_FIRSTNAME);
            $criteria->addSelectColumn(PlayersTableMap::COL_LASTNAME);
            $criteria->addSelectColumn(PlayersTableMap::COL_JERSEYNUMBER);
            $criteria->addSelectColumn(PlayersTableMap::COL_JERSEYNAME);
            $criteria->addSelectColumn(PlayersTableMap::COL_BATTINGHAND);
            $criteria->addSelectColumn(PlayersTableMap::COL_BOWLINGHAND);
            $criteria->addSelectColumn(PlayersTableMap::COL_FIELDINGHAND);
            $criteria->addSelectColumn(PlayersTableMap::COL_MODELNAME);
            $criteria->addSelectColumn(PlayersTableMap::COL_MODELTEXTURE);
            $criteria->addSelectColumn(PlayersTableMap::COL_PRIMARYROLE);
            $criteria->addSelectColumn(PlayersTableMap::COL_BATTINGORDER);
            $criteria->addSelectColumn(PlayersTableMap::COL_RACE);
            $criteria->addSelectColumn(PlayersTableMap::COL_SOURCEURL);
            $criteria->addSelectColumn(PlayersTableMap::COL_TRANSFERVALUE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.firstName');
            $criteria->addSelectColumn($alias . '.lastName');
            $criteria->addSelectColumn($alias . '.jerseyNumber');
            $criteria->addSelectColumn($alias . '.jerseyName');
            $criteria->addSelectColumn($alias . '.battingHand');
            $criteria->addSelectColumn($alias . '.bowlingHand');
            $criteria->addSelectColumn($alias . '.fieldingHand');
            $criteria->addSelectColumn($alias . '.modelName');
            $criteria->addSelectColumn($alias . '.modelTexture');
            $criteria->addSelectColumn($alias . '.primaryRole');
            $criteria->addSelectColumn($alias . '.battingOrder');
            $criteria->addSelectColumn($alias . '.race');
            $criteria->addSelectColumn($alias . '.sourceUrl');
            $criteria->addSelectColumn($alias . '.transferValue');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PlayersTableMap::DATABASE_NAME)->getTable(PlayersTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PlayersTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PlayersTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PlayersTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Players or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Players object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Players) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PlayersTableMap::DATABASE_NAME);
            $criteria->add(PlayersTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PlayersQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PlayersTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PlayersTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the players table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PlayersQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Players or Criteria object.
     *
     * @param mixed               $criteria Criteria or Players object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Players object
        }

        if ($criteria->containsKey(PlayersTableMap::COL_ID) && $criteria->keyContainsValue(PlayersTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PlayersTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PlayersQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PlayersTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PlayersTableMap::buildTableMap();
