<?php

namespace Map;

use \Jerseys;
use \JerseysQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'jerseys' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class JerseysTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.JerseysTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'jerseys';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Jerseys';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Jerseys';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'jerseys.id';

    /**
     * the column name for the team_id field
     */
    const COL_TEAM_ID = 'jerseys.team_id';

    /**
     * the column name for the shirt_color field
     */
    const COL_SHIRT_COLOR = 'jerseys.shirt_color';

    /**
     * the column name for the shirt_pattern_color field
     */
    const COL_SHIRT_PATTERN_COLOR = 'jerseys.shirt_pattern_color';

    /**
     * the column name for the shirt_sleve_color field
     */
    const COL_SHIRT_SLEVE_COLOR = 'jerseys.shirt_sleve_color';

    /**
     * the column name for the pant_color field
     */
    const COL_PANT_COLOR = 'jerseys.pant_color';

    /**
     * the column name for the cap_color field
     */
    const COL_CAP_COLOR = 'jerseys.cap_color';

    /**
     * the column name for the studio_logo_color field
     */
    const COL_STUDIO_LOGO_COLOR = 'jerseys.studio_logo_color';

    /**
     * the column name for the number_color field
     */
    const COL_NUMBER_COLOR = 'jerseys.number_color';

    /**
     * the column name for the pant_pattern_color field
     */
    const COL_PANT_PATTERN_COLOR = 'jerseys.pant_pattern_color';

    /**
     * the column name for the shirt_line_color field
     */
    const COL_SHIRT_LINE_COLOR = 'jerseys.shirt_line_color';

    /**
     * the column name for the pant_texture field
     */
    const COL_PANT_TEXTURE = 'jerseys.pant_texture';

    /**
     * the column name for the shoe_color field
     */
    const COL_SHOE_COLOR = 'jerseys.shoe_color';

    /**
     * the column name for the shoe_sole_color field
     */
    const COL_SHOE_SOLE_COLOR = 'jerseys.shoe_sole_color';

    /**
     * the column name for the socks_color field
     */
    const COL_SOCKS_COLOR = 'jerseys.socks_color';

    /**
     * the column name for the shirt_texture field
     */
    const COL_SHIRT_TEXTURE = 'jerseys.shirt_texture';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TeamId', 'ShirtColor', 'ShirtPatternColor', 'ShirtSleveColor', 'PantColor', 'CapColor', 'StudioLogoColor', 'NumberColor', 'PantPatternColor', 'ShirtLineColor', 'PantTexture', 'ShoeColor', 'ShoeSoleColor', 'SocksColor', 'ShirtTexture', ),
        self::TYPE_CAMELNAME     => array('id', 'teamId', 'shirtColor', 'shirtPatternColor', 'shirtSleveColor', 'pantColor', 'capColor', 'studioLogoColor', 'numberColor', 'pantPatternColor', 'shirtLineColor', 'pantTexture', 'shoeColor', 'shoeSoleColor', 'socksColor', 'shirtTexture', ),
        self::TYPE_COLNAME       => array(JerseysTableMap::COL_ID, JerseysTableMap::COL_TEAM_ID, JerseysTableMap::COL_SHIRT_COLOR, JerseysTableMap::COL_SHIRT_PATTERN_COLOR, JerseysTableMap::COL_SHIRT_SLEVE_COLOR, JerseysTableMap::COL_PANT_COLOR, JerseysTableMap::COL_CAP_COLOR, JerseysTableMap::COL_STUDIO_LOGO_COLOR, JerseysTableMap::COL_NUMBER_COLOR, JerseysTableMap::COL_PANT_PATTERN_COLOR, JerseysTableMap::COL_SHIRT_LINE_COLOR, JerseysTableMap::COL_PANT_TEXTURE, JerseysTableMap::COL_SHOE_COLOR, JerseysTableMap::COL_SHOE_SOLE_COLOR, JerseysTableMap::COL_SOCKS_COLOR, JerseysTableMap::COL_SHIRT_TEXTURE, ),
        self::TYPE_FIELDNAME     => array('id', 'team_id', 'shirt_color', 'shirt_pattern_color', 'shirt_sleve_color', 'pant_color', 'cap_color', 'studio_logo_color', 'number_color', 'pant_pattern_color', 'shirt_line_color', 'pant_texture', 'shoe_color', 'shoe_sole_color', 'socks_color', 'shirt_texture', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TeamId' => 1, 'ShirtColor' => 2, 'ShirtPatternColor' => 3, 'ShirtSleveColor' => 4, 'PantColor' => 5, 'CapColor' => 6, 'StudioLogoColor' => 7, 'NumberColor' => 8, 'PantPatternColor' => 9, 'ShirtLineColor' => 10, 'PantTexture' => 11, 'ShoeColor' => 12, 'ShoeSoleColor' => 13, 'SocksColor' => 14, 'ShirtTexture' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'teamId' => 1, 'shirtColor' => 2, 'shirtPatternColor' => 3, 'shirtSleveColor' => 4, 'pantColor' => 5, 'capColor' => 6, 'studioLogoColor' => 7, 'numberColor' => 8, 'pantPatternColor' => 9, 'shirtLineColor' => 10, 'pantTexture' => 11, 'shoeColor' => 12, 'shoeSoleColor' => 13, 'socksColor' => 14, 'shirtTexture' => 15, ),
        self::TYPE_COLNAME       => array(JerseysTableMap::COL_ID => 0, JerseysTableMap::COL_TEAM_ID => 1, JerseysTableMap::COL_SHIRT_COLOR => 2, JerseysTableMap::COL_SHIRT_PATTERN_COLOR => 3, JerseysTableMap::COL_SHIRT_SLEVE_COLOR => 4, JerseysTableMap::COL_PANT_COLOR => 5, JerseysTableMap::COL_CAP_COLOR => 6, JerseysTableMap::COL_STUDIO_LOGO_COLOR => 7, JerseysTableMap::COL_NUMBER_COLOR => 8, JerseysTableMap::COL_PANT_PATTERN_COLOR => 9, JerseysTableMap::COL_SHIRT_LINE_COLOR => 10, JerseysTableMap::COL_PANT_TEXTURE => 11, JerseysTableMap::COL_SHOE_COLOR => 12, JerseysTableMap::COL_SHOE_SOLE_COLOR => 13, JerseysTableMap::COL_SOCKS_COLOR => 14, JerseysTableMap::COL_SHIRT_TEXTURE => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'team_id' => 1, 'shirt_color' => 2, 'shirt_pattern_color' => 3, 'shirt_sleve_color' => 4, 'pant_color' => 5, 'cap_color' => 6, 'studio_logo_color' => 7, 'number_color' => 8, 'pant_pattern_color' => 9, 'shirt_line_color' => 10, 'pant_texture' => 11, 'shoe_color' => 12, 'shoe_sole_color' => 13, 'socks_color' => 14, 'shirt_texture' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jerseys');
        $this->setPhpName('Jerseys');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Jerseys');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('team_id', 'TeamId', 'INTEGER', false, null, null);
        $this->addColumn('shirt_color', 'ShirtColor', 'VARCHAR', false, 45, null);
        $this->addColumn('shirt_pattern_color', 'ShirtPatternColor', 'VARCHAR', false, 45, null);
        $this->addColumn('shirt_sleve_color', 'ShirtSleveColor', 'VARCHAR', false, 45, null);
        $this->addColumn('pant_color', 'PantColor', 'VARCHAR', false, 45, null);
        $this->addColumn('cap_color', 'CapColor', 'VARCHAR', false, 45, null);
        $this->addColumn('studio_logo_color', 'StudioLogoColor', 'VARCHAR', false, 45, null);
        $this->addColumn('number_color', 'NumberColor', 'VARCHAR', false, 45, null);
        $this->addColumn('pant_pattern_color', 'PantPatternColor', 'VARCHAR', false, 45, null);
        $this->addColumn('shirt_line_color', 'ShirtLineColor', 'VARCHAR', false, 45, null);
        $this->addColumn('pant_texture', 'PantTexture', 'VARCHAR', false, 45, null);
        $this->addColumn('shoe_color', 'ShoeColor', 'VARCHAR', false, 45, null);
        $this->addColumn('shoe_sole_color', 'ShoeSoleColor', 'VARCHAR', false, 45, null);
        $this->addColumn('socks_color', 'SocksColor', 'VARCHAR', false, 45, null);
        $this->addColumn('shirt_texture', 'ShirtTexture', 'VARCHAR', false, 45, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? JerseysTableMap::CLASS_DEFAULT : JerseysTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Jerseys object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = JerseysTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = JerseysTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + JerseysTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = JerseysTableMap::OM_CLASS;
            /** @var Jerseys $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            JerseysTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = JerseysTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = JerseysTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Jerseys $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                JerseysTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(JerseysTableMap::COL_ID);
            $criteria->addSelectColumn(JerseysTableMap::COL_TEAM_ID);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHIRT_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHIRT_PATTERN_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHIRT_SLEVE_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_PANT_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_CAP_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_STUDIO_LOGO_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_NUMBER_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_PANT_PATTERN_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHIRT_LINE_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_PANT_TEXTURE);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHOE_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHOE_SOLE_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SOCKS_COLOR);
            $criteria->addSelectColumn(JerseysTableMap::COL_SHIRT_TEXTURE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.team_id');
            $criteria->addSelectColumn($alias . '.shirt_color');
            $criteria->addSelectColumn($alias . '.shirt_pattern_color');
            $criteria->addSelectColumn($alias . '.shirt_sleve_color');
            $criteria->addSelectColumn($alias . '.pant_color');
            $criteria->addSelectColumn($alias . '.cap_color');
            $criteria->addSelectColumn($alias . '.studio_logo_color');
            $criteria->addSelectColumn($alias . '.number_color');
            $criteria->addSelectColumn($alias . '.pant_pattern_color');
            $criteria->addSelectColumn($alias . '.shirt_line_color');
            $criteria->addSelectColumn($alias . '.pant_texture');
            $criteria->addSelectColumn($alias . '.shoe_color');
            $criteria->addSelectColumn($alias . '.shoe_sole_color');
            $criteria->addSelectColumn($alias . '.socks_color');
            $criteria->addSelectColumn($alias . '.shirt_texture');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(JerseysTableMap::DATABASE_NAME)->getTable(JerseysTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(JerseysTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(JerseysTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new JerseysTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Jerseys or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Jerseys object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Jerseys) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(JerseysTableMap::DATABASE_NAME);
            $criteria->add(JerseysTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = JerseysQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            JerseysTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                JerseysTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the jerseys table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return JerseysQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Jerseys or Criteria object.
     *
     * @param mixed               $criteria Criteria or Jerseys object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Jerseys object
        }

        if ($criteria->containsKey(JerseysTableMap::COL_ID) && $criteria->keyContainsValue(JerseysTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.JerseysTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = JerseysQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // JerseysTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
JerseysTableMap::buildTableMap();
