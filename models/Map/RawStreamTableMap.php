<?php

namespace Map;

use \RawStream;
use \RawStreamQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'raw_stream' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RawStreamTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RawStreamTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'raw_stream';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\RawStream';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'RawStream';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id field
     */
    const COL_ID = 'raw_stream.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'raw_stream.name';

    /**
     * the column name for the batting_hand field
     */
    const COL_BATTING_HAND = 'raw_stream.batting_hand';

    /**
     * the column name for the bowling_hand field
     */
    const COL_BOWLING_HAND = 'raw_stream.bowling_hand';

    /**
     * the column name for the age field
     */
    const COL_AGE = 'raw_stream.age';

    /**
     * the column name for the team field
     */
    const COL_TEAM = 'raw_stream.team';

    /**
     * the column name for the bowler_type field
     */
    const COL_BOWLER_TYPE = 'raw_stream.bowler_type';

    /**
     * the column name for the spin_type field
     */
    const COL_SPIN_TYPE = 'raw_stream.spin_type';

    /**
     * the column name for the batting field
     */
    const COL_BATTING = 'raw_stream.batting';

    /**
     * the column name for the bowling field
     */
    const COL_BOWLING = 'raw_stream.bowling';

    /**
     * the column name for the value field
     */
    const COL_VALUE = 'raw_stream.value';

    /**
     * the column name for the primary_role field
     */
    const COL_PRIMARY_ROLE = 'raw_stream.primary_role';

    /**
     * the column name for the batting_order field
     */
    const COL_BATTING_ORDER = 'raw_stream.batting_order';

    /**
     * the column name for the race field
     */
    const COL_RACE = 'raw_stream.race';

    /**
     * the column name for the link field
     */
    const COL_LINK = 'raw_stream.link';

    /**
     * the column name for the is_stats_processed field
     */
    const COL_IS_STATS_PROCESSED = 'raw_stream.is_stats_processed';

    /**
     * the column name for the stats_link field
     */
    const COL_STATS_LINK = 'raw_stream.stats_link';

    /**
     * the column name for the is_processed field
     */
    const COL_IS_PROCESSED = 'raw_stream.is_processed';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'BattingHand', 'BowlingHand', 'Age', 'Team', 'Bowlertype', 'Spintype', 'Batting', 'Bowling', 'Value', 'PrimaryRole', 'Battingorder', 'Race', 'Link', 'IsStatsProcessed', 'StatsLink', 'IsProcessed', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'battingHand', 'bowlingHand', 'age', 'team', 'bowlertype', 'spintype', 'batting', 'bowling', 'value', 'primaryRole', 'battingorder', 'race', 'link', 'isStatsProcessed', 'statsLink', 'isProcessed', ),
        self::TYPE_COLNAME       => array(RawStreamTableMap::COL_ID, RawStreamTableMap::COL_NAME, RawStreamTableMap::COL_BATTING_HAND, RawStreamTableMap::COL_BOWLING_HAND, RawStreamTableMap::COL_AGE, RawStreamTableMap::COL_TEAM, RawStreamTableMap::COL_BOWLER_TYPE, RawStreamTableMap::COL_SPIN_TYPE, RawStreamTableMap::COL_BATTING, RawStreamTableMap::COL_BOWLING, RawStreamTableMap::COL_VALUE, RawStreamTableMap::COL_PRIMARY_ROLE, RawStreamTableMap::COL_BATTING_ORDER, RawStreamTableMap::COL_RACE, RawStreamTableMap::COL_LINK, RawStreamTableMap::COL_IS_STATS_PROCESSED, RawStreamTableMap::COL_STATS_LINK, RawStreamTableMap::COL_IS_PROCESSED, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'batting_hand', 'bowling_hand', 'age', 'team', 'bowler_type', 'spin_type', 'batting', 'bowling', 'value', 'primary_role', 'batting_order', 'race', 'link', 'is_stats_processed', 'stats_link', 'is_processed', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'BattingHand' => 2, 'BowlingHand' => 3, 'Age' => 4, 'Team' => 5, 'Bowlertype' => 6, 'Spintype' => 7, 'Batting' => 8, 'Bowling' => 9, 'Value' => 10, 'PrimaryRole' => 11, 'Battingorder' => 12, 'Race' => 13, 'Link' => 14, 'IsStatsProcessed' => 15, 'StatsLink' => 16, 'IsProcessed' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'battingHand' => 2, 'bowlingHand' => 3, 'age' => 4, 'team' => 5, 'bowlertype' => 6, 'spintype' => 7, 'batting' => 8, 'bowling' => 9, 'value' => 10, 'primaryRole' => 11, 'battingorder' => 12, 'race' => 13, 'link' => 14, 'isStatsProcessed' => 15, 'statsLink' => 16, 'isProcessed' => 17, ),
        self::TYPE_COLNAME       => array(RawStreamTableMap::COL_ID => 0, RawStreamTableMap::COL_NAME => 1, RawStreamTableMap::COL_BATTING_HAND => 2, RawStreamTableMap::COL_BOWLING_HAND => 3, RawStreamTableMap::COL_AGE => 4, RawStreamTableMap::COL_TEAM => 5, RawStreamTableMap::COL_BOWLER_TYPE => 6, RawStreamTableMap::COL_SPIN_TYPE => 7, RawStreamTableMap::COL_BATTING => 8, RawStreamTableMap::COL_BOWLING => 9, RawStreamTableMap::COL_VALUE => 10, RawStreamTableMap::COL_PRIMARY_ROLE => 11, RawStreamTableMap::COL_BATTING_ORDER => 12, RawStreamTableMap::COL_RACE => 13, RawStreamTableMap::COL_LINK => 14, RawStreamTableMap::COL_IS_STATS_PROCESSED => 15, RawStreamTableMap::COL_STATS_LINK => 16, RawStreamTableMap::COL_IS_PROCESSED => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'batting_hand' => 2, 'bowling_hand' => 3, 'age' => 4, 'team' => 5, 'bowler_type' => 6, 'spin_type' => 7, 'batting' => 8, 'bowling' => 9, 'value' => 10, 'primary_role' => 11, 'batting_order' => 12, 'race' => 13, 'link' => 14, 'is_stats_processed' => 15, 'stats_link' => 16, 'is_processed' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('raw_stream');
        $this->setPhpName('RawStream');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\RawStream');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 200, null);
        $this->addColumn('batting_hand', 'BattingHand', 'VARCHAR', true, 50, null);
        $this->addColumn('bowling_hand', 'BowlingHand', 'VARCHAR', true, 50, null);
        $this->addColumn('age', 'Age', 'INTEGER', false, null, null);
        $this->addColumn('team', 'Team', 'VARCHAR', true, 100, null);
        $this->addColumn('bowler_type', 'Bowlertype', 'VARCHAR', false, 45, null);
        $this->addColumn('spin_type', 'Spintype', 'VARCHAR', false, 45, null);
        $this->addColumn('batting', 'Batting', 'INTEGER', false, null, 80);
        $this->addColumn('bowling', 'Bowling', 'INTEGER', false, null, 80);
        $this->addColumn('value', 'Value', 'FLOAT', false, null, 80);
        $this->addColumn('primary_role', 'PrimaryRole', 'CHAR', false, null, 'allRounder');
        $this->addColumn('batting_order', 'Battingorder', 'INTEGER', false, null, null);
        $this->addColumn('race', 'Race', 'CHAR', false, null, 'indian');
        $this->addColumn('link', 'Link', 'VARCHAR', false, 250, null);
        $this->addColumn('is_stats_processed', 'IsStatsProcessed', 'TINYINT', false, null, 0);
        $this->addColumn('stats_link', 'StatsLink', 'VARCHAR', false, 400, null);
        $this->addColumn('is_processed', 'IsProcessed', 'TINYINT', false, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RawStreamTableMap::CLASS_DEFAULT : RawStreamTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RawStream object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RawStreamTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RawStreamTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RawStreamTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RawStreamTableMap::OM_CLASS;
            /** @var RawStream $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RawStreamTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RawStreamTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RawStreamTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RawStream $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RawStreamTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RawStreamTableMap::COL_ID);
            $criteria->addSelectColumn(RawStreamTableMap::COL_NAME);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BATTING_HAND);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BOWLING_HAND);
            $criteria->addSelectColumn(RawStreamTableMap::COL_AGE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_TEAM);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BOWLER_TYPE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_SPIN_TYPE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BATTING);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BOWLING);
            $criteria->addSelectColumn(RawStreamTableMap::COL_VALUE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_PRIMARY_ROLE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_BATTING_ORDER);
            $criteria->addSelectColumn(RawStreamTableMap::COL_RACE);
            $criteria->addSelectColumn(RawStreamTableMap::COL_LINK);
            $criteria->addSelectColumn(RawStreamTableMap::COL_IS_STATS_PROCESSED);
            $criteria->addSelectColumn(RawStreamTableMap::COL_STATS_LINK);
            $criteria->addSelectColumn(RawStreamTableMap::COL_IS_PROCESSED);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.batting_hand');
            $criteria->addSelectColumn($alias . '.bowling_hand');
            $criteria->addSelectColumn($alias . '.age');
            $criteria->addSelectColumn($alias . '.team');
            $criteria->addSelectColumn($alias . '.bowler_type');
            $criteria->addSelectColumn($alias . '.spin_type');
            $criteria->addSelectColumn($alias . '.batting');
            $criteria->addSelectColumn($alias . '.bowling');
            $criteria->addSelectColumn($alias . '.value');
            $criteria->addSelectColumn($alias . '.primary_role');
            $criteria->addSelectColumn($alias . '.batting_order');
            $criteria->addSelectColumn($alias . '.race');
            $criteria->addSelectColumn($alias . '.link');
            $criteria->addSelectColumn($alias . '.is_stats_processed');
            $criteria->addSelectColumn($alias . '.stats_link');
            $criteria->addSelectColumn($alias . '.is_processed');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RawStreamTableMap::DATABASE_NAME)->getTable(RawStreamTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RawStreamTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RawStreamTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RawStreamTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RawStream or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RawStream object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \RawStream) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RawStreamTableMap::DATABASE_NAME);
            $criteria->add(RawStreamTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = RawStreamQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RawStreamTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RawStreamTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the raw_stream table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RawStreamQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RawStream or Criteria object.
     *
     * @param mixed               $criteria Criteria or RawStream object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RawStream object
        }

        if ($criteria->containsKey(RawStreamTableMap::COL_ID) && $criteria->keyContainsValue(RawStreamTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RawStreamTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = RawStreamQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RawStreamTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RawStreamTableMap::buildTableMap();
