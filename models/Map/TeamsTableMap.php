<?php

namespace Map;

use \Teams;
use \TeamsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'teams' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TeamsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TeamsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'teams';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Teams';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Teams';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id field
     */
    const COL_ID = 'teams.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'teams.name';

    /**
     * the column name for the short_name field
     */
    const COL_SHORT_NAME = 'teams.short_name';

    /**
     * the column name for the wiki_url field
     */
    const COL_WIKI_URL = 'teams.wiki_url';

    /**
     * the column name for the table_selector field
     */
    const COL_TABLE_SELECTOR = 'teams.table_selector';

    /**
     * the column name for the player_name field
     */
    const COL_PLAYER_NAME = 'teams.player_name';

    /**
     * the column name for the player_batting_hand field
     */
    const COL_PLAYER_BATTING_HAND = 'teams.player_batting_hand';

    /**
     * the column name for the player_bowling_hand field
     */
    const COL_PLAYER_BOWLING_HAND = 'teams.player_bowling_hand';

    /**
     * the column name for the player_dob field
     */
    const COL_PLAYER_DOB = 'teams.player_dob';

    /**
     * the column name for the logo field
     */
    const COL_LOGO = 'teams.logo';

    /**
     * the column name for the country field
     */
    const COL_COUNTRY = 'teams.country';

    /**
     * the column name for the batting field
     */
    const COL_BATTING = 'teams.batting';

    /**
     * the column name for the bowling field
     */
    const COL_BOWLING = 'teams.bowling';

    /**
     * the column name for the fielding field
     */
    const COL_FIELDING = 'teams.fielding';

    /**
     * the column name for the league field
     */
    const COL_LEAGUE = 'teams.league';

    /**
     * the column name for the is_international field
     */
    const COL_IS_INTERNATIONAL = 'teams.is_international';

    /**
     * the column name for the team_type field
     */
    const COL_TEAM_TYPE = 'teams.team_type';

    /**
     * the column name for the stadium_id field
     */
    const COL_STADIUM_ID = 'teams.stadium_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'ShortName', 'WikiUrl', 'TableSelector', 'PlayerName', 'PlayerBattingHand', 'PlayerBowlingHand', 'PlayerDob', 'Logo', 'Country', 'Batting', 'Bowling', 'Fielding', 'League', 'IsInternational', 'TeamType', 'StadiumID', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'shortName', 'wikiUrl', 'tableSelector', 'playerName', 'playerBattingHand', 'playerBowlingHand', 'playerDob', 'logo', 'country', 'batting', 'bowling', 'fielding', 'league', 'isInternational', 'teamType', 'stadiumID', ),
        self::TYPE_COLNAME       => array(TeamsTableMap::COL_ID, TeamsTableMap::COL_NAME, TeamsTableMap::COL_SHORT_NAME, TeamsTableMap::COL_WIKI_URL, TeamsTableMap::COL_TABLE_SELECTOR, TeamsTableMap::COL_PLAYER_NAME, TeamsTableMap::COL_PLAYER_BATTING_HAND, TeamsTableMap::COL_PLAYER_BOWLING_HAND, TeamsTableMap::COL_PLAYER_DOB, TeamsTableMap::COL_LOGO, TeamsTableMap::COL_COUNTRY, TeamsTableMap::COL_BATTING, TeamsTableMap::COL_BOWLING, TeamsTableMap::COL_FIELDING, TeamsTableMap::COL_LEAGUE, TeamsTableMap::COL_IS_INTERNATIONAL, TeamsTableMap::COL_TEAM_TYPE, TeamsTableMap::COL_STADIUM_ID, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'short_name', 'wiki_url', 'table_selector', 'player_name', 'player_batting_hand', 'player_bowling_hand', 'player_dob', 'logo', 'country', 'batting', 'bowling', 'fielding', 'league', 'is_international', 'team_type', 'stadium_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'ShortName' => 2, 'WikiUrl' => 3, 'TableSelector' => 4, 'PlayerName' => 5, 'PlayerBattingHand' => 6, 'PlayerBowlingHand' => 7, 'PlayerDob' => 8, 'Logo' => 9, 'Country' => 10, 'Batting' => 11, 'Bowling' => 12, 'Fielding' => 13, 'League' => 14, 'IsInternational' => 15, 'TeamType' => 16, 'StadiumID' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'shortName' => 2, 'wikiUrl' => 3, 'tableSelector' => 4, 'playerName' => 5, 'playerBattingHand' => 6, 'playerBowlingHand' => 7, 'playerDob' => 8, 'logo' => 9, 'country' => 10, 'batting' => 11, 'bowling' => 12, 'fielding' => 13, 'league' => 14, 'isInternational' => 15, 'teamType' => 16, 'stadiumID' => 17, ),
        self::TYPE_COLNAME       => array(TeamsTableMap::COL_ID => 0, TeamsTableMap::COL_NAME => 1, TeamsTableMap::COL_SHORT_NAME => 2, TeamsTableMap::COL_WIKI_URL => 3, TeamsTableMap::COL_TABLE_SELECTOR => 4, TeamsTableMap::COL_PLAYER_NAME => 5, TeamsTableMap::COL_PLAYER_BATTING_HAND => 6, TeamsTableMap::COL_PLAYER_BOWLING_HAND => 7, TeamsTableMap::COL_PLAYER_DOB => 8, TeamsTableMap::COL_LOGO => 9, TeamsTableMap::COL_COUNTRY => 10, TeamsTableMap::COL_BATTING => 11, TeamsTableMap::COL_BOWLING => 12, TeamsTableMap::COL_FIELDING => 13, TeamsTableMap::COL_LEAGUE => 14, TeamsTableMap::COL_IS_INTERNATIONAL => 15, TeamsTableMap::COL_TEAM_TYPE => 16, TeamsTableMap::COL_STADIUM_ID => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'short_name' => 2, 'wiki_url' => 3, 'table_selector' => 4, 'player_name' => 5, 'player_batting_hand' => 6, 'player_bowling_hand' => 7, 'player_dob' => 8, 'logo' => 9, 'country' => 10, 'batting' => 11, 'bowling' => 12, 'fielding' => 13, 'league' => 14, 'is_international' => 15, 'team_type' => 16, 'stadium_id' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('teams');
        $this->setPhpName('Teams');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Teams');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 45, null);
        $this->addColumn('short_name', 'ShortName', 'VARCHAR', false, 3, null);
        $this->addColumn('wiki_url', 'WikiUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('table_selector', 'TableSelector', 'VARCHAR', false, 255, null);
        $this->addColumn('player_name', 'PlayerName', 'VARCHAR', false, 255, null);
        $this->addColumn('player_batting_hand', 'PlayerBattingHand', 'VARCHAR', false, 255, null);
        $this->addColumn('player_bowling_hand', 'PlayerBowlingHand', 'VARCHAR', false, 255, null);
        $this->addColumn('player_dob', 'PlayerDob', 'VARCHAR', false, 255, null);
        $this->addColumn('logo', 'Logo', 'VARCHAR', false, 255, null);
        $this->addColumn('country', 'Country', 'VARCHAR', false, 255, null);
        $this->addColumn('batting', 'Batting', 'INTEGER', false, null, null);
        $this->addColumn('bowling', 'Bowling', 'INTEGER', false, null, null);
        $this->addColumn('fielding', 'Fielding', 'INTEGER', false, null, null);
        $this->addColumn('league', 'League', 'INTEGER', false, null, null);
        $this->addColumn('is_international', 'IsInternational', 'TINYINT', false, null, 0);
        $this->addColumn('team_type', 'TeamType', 'VARCHAR', false, 255, null);
        $this->addColumn('stadium_id', 'StadiumID', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TeamsTableMap::CLASS_DEFAULT : TeamsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Teams object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TeamsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TeamsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TeamsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TeamsTableMap::OM_CLASS;
            /** @var Teams $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TeamsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TeamsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TeamsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Teams $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TeamsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TeamsTableMap::COL_ID);
            $criteria->addSelectColumn(TeamsTableMap::COL_NAME);
            $criteria->addSelectColumn(TeamsTableMap::COL_SHORT_NAME);
            $criteria->addSelectColumn(TeamsTableMap::COL_WIKI_URL);
            $criteria->addSelectColumn(TeamsTableMap::COL_TABLE_SELECTOR);
            $criteria->addSelectColumn(TeamsTableMap::COL_PLAYER_NAME);
            $criteria->addSelectColumn(TeamsTableMap::COL_PLAYER_BATTING_HAND);
            $criteria->addSelectColumn(TeamsTableMap::COL_PLAYER_BOWLING_HAND);
            $criteria->addSelectColumn(TeamsTableMap::COL_PLAYER_DOB);
            $criteria->addSelectColumn(TeamsTableMap::COL_LOGO);
            $criteria->addSelectColumn(TeamsTableMap::COL_COUNTRY);
            $criteria->addSelectColumn(TeamsTableMap::COL_BATTING);
            $criteria->addSelectColumn(TeamsTableMap::COL_BOWLING);
            $criteria->addSelectColumn(TeamsTableMap::COL_FIELDING);
            $criteria->addSelectColumn(TeamsTableMap::COL_LEAGUE);
            $criteria->addSelectColumn(TeamsTableMap::COL_IS_INTERNATIONAL);
            $criteria->addSelectColumn(TeamsTableMap::COL_TEAM_TYPE);
            $criteria->addSelectColumn(TeamsTableMap::COL_STADIUM_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.short_name');
            $criteria->addSelectColumn($alias . '.wiki_url');
            $criteria->addSelectColumn($alias . '.table_selector');
            $criteria->addSelectColumn($alias . '.player_name');
            $criteria->addSelectColumn($alias . '.player_batting_hand');
            $criteria->addSelectColumn($alias . '.player_bowling_hand');
            $criteria->addSelectColumn($alias . '.player_dob');
            $criteria->addSelectColumn($alias . '.logo');
            $criteria->addSelectColumn($alias . '.country');
            $criteria->addSelectColumn($alias . '.batting');
            $criteria->addSelectColumn($alias . '.bowling');
            $criteria->addSelectColumn($alias . '.fielding');
            $criteria->addSelectColumn($alias . '.league');
            $criteria->addSelectColumn($alias . '.is_international');
            $criteria->addSelectColumn($alias . '.team_type');
            $criteria->addSelectColumn($alias . '.stadium_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TeamsTableMap::DATABASE_NAME)->getTable(TeamsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TeamsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TeamsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TeamsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Teams or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Teams object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Teams) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TeamsTableMap::DATABASE_NAME);
            $criteria->add(TeamsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = TeamsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TeamsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TeamsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the teams table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TeamsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Teams or Criteria object.
     *
     * @param mixed               $criteria Criteria or Teams object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Teams object
        }

        if ($criteria->containsKey(TeamsTableMap::COL_ID) && $criteria->keyContainsValue(TeamsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TeamsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = TeamsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TeamsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TeamsTableMap::buildTableMap();
