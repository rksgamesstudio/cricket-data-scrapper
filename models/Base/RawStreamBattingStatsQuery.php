<?php

namespace Base;

use \RawStreamBattingStats as ChildRawStreamBattingStats;
use \RawStreamBattingStatsQuery as ChildRawStreamBattingStatsQuery;
use \Exception;
use \PDO;
use Map\RawStreamBattingStatsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'raw_stream_batting_stats' table.
 *
 *
 *
 * @method     ChildRawStreamBattingStatsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRawStreamBattingStatsQuery orderByPlayerId($order = Criteria::ASC) Order by the player_id column
 * @method     ChildRawStreamBattingStatsQuery orderByDataSource($order = Criteria::ASC) Order by the data_source column
 * @method     ChildRawStreamBattingStatsQuery orderByFormat($order = Criteria::ASC) Order by the format column
 * @method     ChildRawStreamBattingStatsQuery orderByMatches($order = Criteria::ASC) Order by the matches column
 * @method     ChildRawStreamBattingStatsQuery orderByInnings($order = Criteria::ASC) Order by the innings column
 * @method     ChildRawStreamBattingStatsQuery orderByNotOuts($order = Criteria::ASC) Order by the not_outs column
 * @method     ChildRawStreamBattingStatsQuery orderByRuns($order = Criteria::ASC) Order by the runs column
 * @method     ChildRawStreamBattingStatsQuery orderByAverage($order = Criteria::ASC) Order by the average column
 * @method     ChildRawStreamBattingStatsQuery orderByStrikeRate($order = Criteria::ASC) Order by the strike_rate column
 * @method     ChildRawStreamBattingStatsQuery orderByCenturies($order = Criteria::ASC) Order by the centuries column
 * @method     ChildRawStreamBattingStatsQuery orderByHalfCenturies($order = Criteria::ASC) Order by the half_centuries column
 * @method     ChildRawStreamBattingStatsQuery orderByFours($order = Criteria::ASC) Order by the fours column
 * @method     ChildRawStreamBattingStatsQuery orderBySixes($order = Criteria::ASC) Order by the sixes column
 *
 * @method     ChildRawStreamBattingStatsQuery groupById() Group by the id column
 * @method     ChildRawStreamBattingStatsQuery groupByPlayerId() Group by the player_id column
 * @method     ChildRawStreamBattingStatsQuery groupByDataSource() Group by the data_source column
 * @method     ChildRawStreamBattingStatsQuery groupByFormat() Group by the format column
 * @method     ChildRawStreamBattingStatsQuery groupByMatches() Group by the matches column
 * @method     ChildRawStreamBattingStatsQuery groupByInnings() Group by the innings column
 * @method     ChildRawStreamBattingStatsQuery groupByNotOuts() Group by the not_outs column
 * @method     ChildRawStreamBattingStatsQuery groupByRuns() Group by the runs column
 * @method     ChildRawStreamBattingStatsQuery groupByAverage() Group by the average column
 * @method     ChildRawStreamBattingStatsQuery groupByStrikeRate() Group by the strike_rate column
 * @method     ChildRawStreamBattingStatsQuery groupByCenturies() Group by the centuries column
 * @method     ChildRawStreamBattingStatsQuery groupByHalfCenturies() Group by the half_centuries column
 * @method     ChildRawStreamBattingStatsQuery groupByFours() Group by the fours column
 * @method     ChildRawStreamBattingStatsQuery groupBySixes() Group by the sixes column
 *
 * @method     ChildRawStreamBattingStatsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRawStreamBattingStatsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRawStreamBattingStatsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRawStreamBattingStatsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRawStreamBattingStatsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRawStreamBattingStatsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRawStreamBattingStats findOne(ConnectionInterface $con = null) Return the first ChildRawStreamBattingStats matching the query
 * @method     ChildRawStreamBattingStats findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRawStreamBattingStats matching the query, or a new ChildRawStreamBattingStats object populated from the query conditions when no match is found
 *
 * @method     ChildRawStreamBattingStats findOneById(int $id) Return the first ChildRawStreamBattingStats filtered by the id column
 * @method     ChildRawStreamBattingStats findOneByPlayerId(int $player_id) Return the first ChildRawStreamBattingStats filtered by the player_id column
 * @method     ChildRawStreamBattingStats findOneByDataSource(string $data_source) Return the first ChildRawStreamBattingStats filtered by the data_source column
 * @method     ChildRawStreamBattingStats findOneByFormat(string $format) Return the first ChildRawStreamBattingStats filtered by the format column
 * @method     ChildRawStreamBattingStats findOneByMatches(int $matches) Return the first ChildRawStreamBattingStats filtered by the matches column
 * @method     ChildRawStreamBattingStats findOneByInnings(int $innings) Return the first ChildRawStreamBattingStats filtered by the innings column
 * @method     ChildRawStreamBattingStats findOneByNotOuts(int $not_outs) Return the first ChildRawStreamBattingStats filtered by the not_outs column
 * @method     ChildRawStreamBattingStats findOneByRuns(int $runs) Return the first ChildRawStreamBattingStats filtered by the runs column
 * @method     ChildRawStreamBattingStats findOneByAverage(double $average) Return the first ChildRawStreamBattingStats filtered by the average column
 * @method     ChildRawStreamBattingStats findOneByStrikeRate(double $strike_rate) Return the first ChildRawStreamBattingStats filtered by the strike_rate column
 * @method     ChildRawStreamBattingStats findOneByCenturies(int $centuries) Return the first ChildRawStreamBattingStats filtered by the centuries column
 * @method     ChildRawStreamBattingStats findOneByHalfCenturies(int $half_centuries) Return the first ChildRawStreamBattingStats filtered by the half_centuries column
 * @method     ChildRawStreamBattingStats findOneByFours(int $fours) Return the first ChildRawStreamBattingStats filtered by the fours column
 * @method     ChildRawStreamBattingStats findOneBySixes(int $sixes) Return the first ChildRawStreamBattingStats filtered by the sixes column *

 * @method     ChildRawStreamBattingStats requirePk($key, ConnectionInterface $con = null) Return the ChildRawStreamBattingStats by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOne(ConnectionInterface $con = null) Return the first ChildRawStreamBattingStats matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStreamBattingStats requireOneById(int $id) Return the first ChildRawStreamBattingStats filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByPlayerId(int $player_id) Return the first ChildRawStreamBattingStats filtered by the player_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByDataSource(string $data_source) Return the first ChildRawStreamBattingStats filtered by the data_source column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByFormat(string $format) Return the first ChildRawStreamBattingStats filtered by the format column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByMatches(int $matches) Return the first ChildRawStreamBattingStats filtered by the matches column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByInnings(int $innings) Return the first ChildRawStreamBattingStats filtered by the innings column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByNotOuts(int $not_outs) Return the first ChildRawStreamBattingStats filtered by the not_outs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByRuns(int $runs) Return the first ChildRawStreamBattingStats filtered by the runs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByAverage(double $average) Return the first ChildRawStreamBattingStats filtered by the average column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByStrikeRate(double $strike_rate) Return the first ChildRawStreamBattingStats filtered by the strike_rate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByCenturies(int $centuries) Return the first ChildRawStreamBattingStats filtered by the centuries column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByHalfCenturies(int $half_centuries) Return the first ChildRawStreamBattingStats filtered by the half_centuries column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneByFours(int $fours) Return the first ChildRawStreamBattingStats filtered by the fours column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBattingStats requireOneBySixes(int $sixes) Return the first ChildRawStreamBattingStats filtered by the sixes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStreamBattingStats[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRawStreamBattingStats objects based on current ModelCriteria
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findById(int $id) Return ChildRawStreamBattingStats objects filtered by the id column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByPlayerId(int $player_id) Return ChildRawStreamBattingStats objects filtered by the player_id column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByDataSource(string $data_source) Return ChildRawStreamBattingStats objects filtered by the data_source column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByFormat(string $format) Return ChildRawStreamBattingStats objects filtered by the format column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByMatches(int $matches) Return ChildRawStreamBattingStats objects filtered by the matches column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByInnings(int $innings) Return ChildRawStreamBattingStats objects filtered by the innings column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByNotOuts(int $not_outs) Return ChildRawStreamBattingStats objects filtered by the not_outs column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByRuns(int $runs) Return ChildRawStreamBattingStats objects filtered by the runs column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByAverage(double $average) Return ChildRawStreamBattingStats objects filtered by the average column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByStrikeRate(double $strike_rate) Return ChildRawStreamBattingStats objects filtered by the strike_rate column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByCenturies(int $centuries) Return ChildRawStreamBattingStats objects filtered by the centuries column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByHalfCenturies(int $half_centuries) Return ChildRawStreamBattingStats objects filtered by the half_centuries column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findByFours(int $fours) Return ChildRawStreamBattingStats objects filtered by the fours column
 * @method     ChildRawStreamBattingStats[]|ObjectCollection findBySixes(int $sixes) Return ChildRawStreamBattingStats objects filtered by the sixes column
 * @method     ChildRawStreamBattingStats[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RawStreamBattingStatsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RawStreamBattingStatsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RawStreamBattingStats', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRawStreamBattingStatsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRawStreamBattingStatsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRawStreamBattingStatsQuery) {
            return $criteria;
        }
        $query = new ChildRawStreamBattingStatsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRawStreamBattingStats|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RawStreamBattingStatsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RawStreamBattingStatsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRawStreamBattingStats A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, player_id, data_source, format, matches, innings, not_outs, runs, average, strike_rate, centuries, half_centuries, fours, sixes FROM raw_stream_batting_stats WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRawStreamBattingStats $obj */
            $obj = new ChildRawStreamBattingStats();
            $obj->hydrate($row);
            RawStreamBattingStatsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRawStreamBattingStats|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the player_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerId(1234); // WHERE player_id = 1234
     * $query->filterByPlayerId(array(12, 34)); // WHERE player_id IN (12, 34)
     * $query->filterByPlayerId(array('min' => 12)); // WHERE player_id > 12
     * </code>
     *
     * @param     mixed $playerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByPlayerId($playerId = null, $comparison = null)
    {
        if (is_array($playerId)) {
            $useMinMax = false;
            if (isset($playerId['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_PLAYER_ID, $playerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($playerId['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_PLAYER_ID, $playerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_PLAYER_ID, $playerId, $comparison);
    }

    /**
     * Filter the query on the data_source column
     *
     * Example usage:
     * <code>
     * $query->filterByDataSource('fooValue');   // WHERE data_source = 'fooValue'
     * $query->filterByDataSource('%fooValue%', Criteria::LIKE); // WHERE data_source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataSource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByDataSource($dataSource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataSource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_DATA_SOURCE, $dataSource, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%', Criteria::LIKE); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_FORMAT, $format, $comparison);
    }

    /**
     * Filter the query on the matches column
     *
     * Example usage:
     * <code>
     * $query->filterByMatches(1234); // WHERE matches = 1234
     * $query->filterByMatches(array(12, 34)); // WHERE matches IN (12, 34)
     * $query->filterByMatches(array('min' => 12)); // WHERE matches > 12
     * </code>
     *
     * @param     mixed $matches The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByMatches($matches = null, $comparison = null)
    {
        if (is_array($matches)) {
            $useMinMax = false;
            if (isset($matches['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_MATCHES, $matches['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($matches['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_MATCHES, $matches['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_MATCHES, $matches, $comparison);
    }

    /**
     * Filter the query on the innings column
     *
     * Example usage:
     * <code>
     * $query->filterByInnings(1234); // WHERE innings = 1234
     * $query->filterByInnings(array(12, 34)); // WHERE innings IN (12, 34)
     * $query->filterByInnings(array('min' => 12)); // WHERE innings > 12
     * </code>
     *
     * @param     mixed $innings The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByInnings($innings = null, $comparison = null)
    {
        if (is_array($innings)) {
            $useMinMax = false;
            if (isset($innings['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_INNINGS, $innings['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($innings['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_INNINGS, $innings['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_INNINGS, $innings, $comparison);
    }

    /**
     * Filter the query on the not_outs column
     *
     * Example usage:
     * <code>
     * $query->filterByNotOuts(1234); // WHERE not_outs = 1234
     * $query->filterByNotOuts(array(12, 34)); // WHERE not_outs IN (12, 34)
     * $query->filterByNotOuts(array('min' => 12)); // WHERE not_outs > 12
     * </code>
     *
     * @param     mixed $notOuts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByNotOuts($notOuts = null, $comparison = null)
    {
        if (is_array($notOuts)) {
            $useMinMax = false;
            if (isset($notOuts['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_NOT_OUTS, $notOuts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($notOuts['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_NOT_OUTS, $notOuts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_NOT_OUTS, $notOuts, $comparison);
    }

    /**
     * Filter the query on the runs column
     *
     * Example usage:
     * <code>
     * $query->filterByRuns(1234); // WHERE runs = 1234
     * $query->filterByRuns(array(12, 34)); // WHERE runs IN (12, 34)
     * $query->filterByRuns(array('min' => 12)); // WHERE runs > 12
     * </code>
     *
     * @param     mixed $runs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByRuns($runs = null, $comparison = null)
    {
        if (is_array($runs)) {
            $useMinMax = false;
            if (isset($runs['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_RUNS, $runs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($runs['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_RUNS, $runs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_RUNS, $runs, $comparison);
    }

    /**
     * Filter the query on the average column
     *
     * Example usage:
     * <code>
     * $query->filterByAverage(1234); // WHERE average = 1234
     * $query->filterByAverage(array(12, 34)); // WHERE average IN (12, 34)
     * $query->filterByAverage(array('min' => 12)); // WHERE average > 12
     * </code>
     *
     * @param     mixed $average The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByAverage($average = null, $comparison = null)
    {
        if (is_array($average)) {
            $useMinMax = false;
            if (isset($average['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_AVERAGE, $average['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($average['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_AVERAGE, $average['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_AVERAGE, $average, $comparison);
    }

    /**
     * Filter the query on the strike_rate column
     *
     * Example usage:
     * <code>
     * $query->filterByStrikeRate(1234); // WHERE strike_rate = 1234
     * $query->filterByStrikeRate(array(12, 34)); // WHERE strike_rate IN (12, 34)
     * $query->filterByStrikeRate(array('min' => 12)); // WHERE strike_rate > 12
     * </code>
     *
     * @param     mixed $strikeRate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByStrikeRate($strikeRate = null, $comparison = null)
    {
        if (is_array($strikeRate)) {
            $useMinMax = false;
            if (isset($strikeRate['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_STRIKE_RATE, $strikeRate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($strikeRate['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_STRIKE_RATE, $strikeRate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_STRIKE_RATE, $strikeRate, $comparison);
    }

    /**
     * Filter the query on the centuries column
     *
     * Example usage:
     * <code>
     * $query->filterByCenturies(1234); // WHERE centuries = 1234
     * $query->filterByCenturies(array(12, 34)); // WHERE centuries IN (12, 34)
     * $query->filterByCenturies(array('min' => 12)); // WHERE centuries > 12
     * </code>
     *
     * @param     mixed $centuries The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByCenturies($centuries = null, $comparison = null)
    {
        if (is_array($centuries)) {
            $useMinMax = false;
            if (isset($centuries['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_CENTURIES, $centuries['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($centuries['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_CENTURIES, $centuries['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_CENTURIES, $centuries, $comparison);
    }

    /**
     * Filter the query on the half_centuries column
     *
     * Example usage:
     * <code>
     * $query->filterByHalfCenturies(1234); // WHERE half_centuries = 1234
     * $query->filterByHalfCenturies(array(12, 34)); // WHERE half_centuries IN (12, 34)
     * $query->filterByHalfCenturies(array('min' => 12)); // WHERE half_centuries > 12
     * </code>
     *
     * @param     mixed $halfCenturies The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByHalfCenturies($halfCenturies = null, $comparison = null)
    {
        if (is_array($halfCenturies)) {
            $useMinMax = false;
            if (isset($halfCenturies['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_HALF_CENTURIES, $halfCenturies['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($halfCenturies['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_HALF_CENTURIES, $halfCenturies['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_HALF_CENTURIES, $halfCenturies, $comparison);
    }

    /**
     * Filter the query on the fours column
     *
     * Example usage:
     * <code>
     * $query->filterByFours(1234); // WHERE fours = 1234
     * $query->filterByFours(array(12, 34)); // WHERE fours IN (12, 34)
     * $query->filterByFours(array('min' => 12)); // WHERE fours > 12
     * </code>
     *
     * @param     mixed $fours The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterByFours($fours = null, $comparison = null)
    {
        if (is_array($fours)) {
            $useMinMax = false;
            if (isset($fours['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_FOURS, $fours['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fours['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_FOURS, $fours['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_FOURS, $fours, $comparison);
    }

    /**
     * Filter the query on the sixes column
     *
     * Example usage:
     * <code>
     * $query->filterBySixes(1234); // WHERE sixes = 1234
     * $query->filterBySixes(array(12, 34)); // WHERE sixes IN (12, 34)
     * $query->filterBySixes(array('min' => 12)); // WHERE sixes > 12
     * </code>
     *
     * @param     mixed $sixes The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function filterBySixes($sixes = null, $comparison = null)
    {
        if (is_array($sixes)) {
            $useMinMax = false;
            if (isset($sixes['min'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_SIXES, $sixes['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sixes['max'])) {
                $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_SIXES, $sixes['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_SIXES, $sixes, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRawStreamBattingStats $rawStreamBattingStats Object to remove from the list of results
     *
     * @return $this|ChildRawStreamBattingStatsQuery The current query, for fluid interface
     */
    public function prune($rawStreamBattingStats = null)
    {
        if ($rawStreamBattingStats) {
            $this->addUsingAlias(RawStreamBattingStatsTableMap::COL_ID, $rawStreamBattingStats->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the raw_stream_batting_stats table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBattingStatsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RawStreamBattingStatsTableMap::clearInstancePool();
            RawStreamBattingStatsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBattingStatsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RawStreamBattingStatsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RawStreamBattingStatsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RawStreamBattingStatsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RawStreamBattingStatsQuery
