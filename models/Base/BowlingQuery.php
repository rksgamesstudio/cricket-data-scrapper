<?php

namespace Base;

use \Bowling as ChildBowling;
use \BowlingQuery as ChildBowlingQuery;
use \Exception;
use \PDO;
use Map\BowlingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bowling' table.
 *
 *
 *
 * @method     ChildBowlingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBowlingQuery orderByPlayer($order = Criteria::ASC) Order by the player column
 * @method     ChildBowlingQuery orderByBowlertype($order = Criteria::ASC) Order by the bowlerType column
 * @method     ChildBowlingQuery orderByBowlspeed($order = Criteria::ASC) Order by the bowlSpeed column
 * @method     ChildBowlingQuery orderByBowlswing($order = Criteria::ASC) Order by the bowlSwing column
 * @method     ChildBowlingQuery orderByBowlspin($order = Criteria::ASC) Order by the bowlSpin column
 * @method     ChildBowlingQuery orderBySpintype($order = Criteria::ASC) Order by the spinType column
 * @method     ChildBowlingQuery orderByBowlaccurecy($order = Criteria::ASC) Order by the bowlAccurecy column
 * @method     ChildBowlingQuery orderByShortballstrength($order = Criteria::ASC) Order by the shortBallStrength column
 * @method     ChildBowlingQuery orderByLengthballstrength($order = Criteria::ASC) Order by the lengthBallStrength column
 * @method     ChildBowlingQuery orderByFullballstrength($order = Criteria::ASC) Order by the fullBallStrength column
 * @method     ChildBowlingQuery orderByTemprament($order = Criteria::ASC) Order by the temprament column
 * @method     ChildBowlingQuery orderByAverage($order = Criteria::ASC) Order by the average column
 *
 * @method     ChildBowlingQuery groupById() Group by the id column
 * @method     ChildBowlingQuery groupByPlayer() Group by the player column
 * @method     ChildBowlingQuery groupByBowlertype() Group by the bowlerType column
 * @method     ChildBowlingQuery groupByBowlspeed() Group by the bowlSpeed column
 * @method     ChildBowlingQuery groupByBowlswing() Group by the bowlSwing column
 * @method     ChildBowlingQuery groupByBowlspin() Group by the bowlSpin column
 * @method     ChildBowlingQuery groupBySpintype() Group by the spinType column
 * @method     ChildBowlingQuery groupByBowlaccurecy() Group by the bowlAccurecy column
 * @method     ChildBowlingQuery groupByShortballstrength() Group by the shortBallStrength column
 * @method     ChildBowlingQuery groupByLengthballstrength() Group by the lengthBallStrength column
 * @method     ChildBowlingQuery groupByFullballstrength() Group by the fullBallStrength column
 * @method     ChildBowlingQuery groupByTemprament() Group by the temprament column
 * @method     ChildBowlingQuery groupByAverage() Group by the average column
 *
 * @method     ChildBowlingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBowlingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBowlingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBowlingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBowlingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBowlingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBowling findOne(ConnectionInterface $con = null) Return the first ChildBowling matching the query
 * @method     ChildBowling findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBowling matching the query, or a new ChildBowling object populated from the query conditions when no match is found
 *
 * @method     ChildBowling findOneById(int $id) Return the first ChildBowling filtered by the id column
 * @method     ChildBowling findOneByPlayer(int $player) Return the first ChildBowling filtered by the player column
 * @method     ChildBowling findOneByBowlertype(string $bowlerType) Return the first ChildBowling filtered by the bowlerType column
 * @method     ChildBowling findOneByBowlspeed(int $bowlSpeed) Return the first ChildBowling filtered by the bowlSpeed column
 * @method     ChildBowling findOneByBowlswing(int $bowlSwing) Return the first ChildBowling filtered by the bowlSwing column
 * @method     ChildBowling findOneByBowlspin(int $bowlSpin) Return the first ChildBowling filtered by the bowlSpin column
 * @method     ChildBowling findOneBySpintype(string $spinType) Return the first ChildBowling filtered by the spinType column
 * @method     ChildBowling findOneByBowlaccurecy(int $bowlAccurecy) Return the first ChildBowling filtered by the bowlAccurecy column
 * @method     ChildBowling findOneByShortballstrength(int $shortBallStrength) Return the first ChildBowling filtered by the shortBallStrength column
 * @method     ChildBowling findOneByLengthballstrength(int $lengthBallStrength) Return the first ChildBowling filtered by the lengthBallStrength column
 * @method     ChildBowling findOneByFullballstrength(int $fullBallStrength) Return the first ChildBowling filtered by the fullBallStrength column
 * @method     ChildBowling findOneByTemprament(int $temprament) Return the first ChildBowling filtered by the temprament column
 * @method     ChildBowling findOneByAverage(int $average) Return the first ChildBowling filtered by the average column *

 * @method     ChildBowling requirePk($key, ConnectionInterface $con = null) Return the ChildBowling by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOne(ConnectionInterface $con = null) Return the first ChildBowling matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBowling requireOneById(int $id) Return the first ChildBowling filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByPlayer(int $player) Return the first ChildBowling filtered by the player column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByBowlertype(string $bowlerType) Return the first ChildBowling filtered by the bowlerType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByBowlspeed(int $bowlSpeed) Return the first ChildBowling filtered by the bowlSpeed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByBowlswing(int $bowlSwing) Return the first ChildBowling filtered by the bowlSwing column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByBowlspin(int $bowlSpin) Return the first ChildBowling filtered by the bowlSpin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneBySpintype(string $spinType) Return the first ChildBowling filtered by the spinType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByBowlaccurecy(int $bowlAccurecy) Return the first ChildBowling filtered by the bowlAccurecy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByShortballstrength(int $shortBallStrength) Return the first ChildBowling filtered by the shortBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByLengthballstrength(int $lengthBallStrength) Return the first ChildBowling filtered by the lengthBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByFullballstrength(int $fullBallStrength) Return the first ChildBowling filtered by the fullBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByTemprament(int $temprament) Return the first ChildBowling filtered by the temprament column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBowling requireOneByAverage(int $average) Return the first ChildBowling filtered by the average column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBowling[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBowling objects based on current ModelCriteria
 * @method     ChildBowling[]|ObjectCollection findById(int $id) Return ChildBowling objects filtered by the id column
 * @method     ChildBowling[]|ObjectCollection findByPlayer(int $player) Return ChildBowling objects filtered by the player column
 * @method     ChildBowling[]|ObjectCollection findByBowlertype(string $bowlerType) Return ChildBowling objects filtered by the bowlerType column
 * @method     ChildBowling[]|ObjectCollection findByBowlspeed(int $bowlSpeed) Return ChildBowling objects filtered by the bowlSpeed column
 * @method     ChildBowling[]|ObjectCollection findByBowlswing(int $bowlSwing) Return ChildBowling objects filtered by the bowlSwing column
 * @method     ChildBowling[]|ObjectCollection findByBowlspin(int $bowlSpin) Return ChildBowling objects filtered by the bowlSpin column
 * @method     ChildBowling[]|ObjectCollection findBySpintype(string $spinType) Return ChildBowling objects filtered by the spinType column
 * @method     ChildBowling[]|ObjectCollection findByBowlaccurecy(int $bowlAccurecy) Return ChildBowling objects filtered by the bowlAccurecy column
 * @method     ChildBowling[]|ObjectCollection findByShortballstrength(int $shortBallStrength) Return ChildBowling objects filtered by the shortBallStrength column
 * @method     ChildBowling[]|ObjectCollection findByLengthballstrength(int $lengthBallStrength) Return ChildBowling objects filtered by the lengthBallStrength column
 * @method     ChildBowling[]|ObjectCollection findByFullballstrength(int $fullBallStrength) Return ChildBowling objects filtered by the fullBallStrength column
 * @method     ChildBowling[]|ObjectCollection findByTemprament(int $temprament) Return ChildBowling objects filtered by the temprament column
 * @method     ChildBowling[]|ObjectCollection findByAverage(int $average) Return ChildBowling objects filtered by the average column
 * @method     ChildBowling[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BowlingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BowlingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Bowling', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBowlingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBowlingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBowlingQuery) {
            return $criteria;
        }
        $query = new ChildBowlingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBowling|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BowlingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BowlingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBowling A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, player, bowlerType, bowlSpeed, bowlSwing, bowlSpin, spinType, bowlAccurecy, shortBallStrength, lengthBallStrength, fullBallStrength, temprament, average FROM bowling WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBowling $obj */
            $obj = new ChildBowling();
            $obj->hydrate($row);
            BowlingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBowling|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BowlingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BowlingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the player column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayer(1234); // WHERE player = 1234
     * $query->filterByPlayer(array(12, 34)); // WHERE player IN (12, 34)
     * $query->filterByPlayer(array('min' => 12)); // WHERE player > 12
     * </code>
     *
     * @param     mixed $player The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByPlayer($player = null, $comparison = null)
    {
        if (is_array($player)) {
            $useMinMax = false;
            if (isset($player['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_PLAYER, $player['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($player['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_PLAYER, $player['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_PLAYER, $player, $comparison);
    }

    /**
     * Filter the query on the bowlerType column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlertype('fooValue');   // WHERE bowlerType = 'fooValue'
     * $query->filterByBowlertype('%fooValue%', Criteria::LIKE); // WHERE bowlerType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bowlertype The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByBowlertype($bowlertype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bowlertype)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_BOWLERTYPE, $bowlertype, $comparison);
    }

    /**
     * Filter the query on the bowlSpeed column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlspeed(1234); // WHERE bowlSpeed = 1234
     * $query->filterByBowlspeed(array(12, 34)); // WHERE bowlSpeed IN (12, 34)
     * $query->filterByBowlspeed(array('min' => 12)); // WHERE bowlSpeed > 12
     * </code>
     *
     * @param     mixed $bowlspeed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByBowlspeed($bowlspeed = null, $comparison = null)
    {
        if (is_array($bowlspeed)) {
            $useMinMax = false;
            if (isset($bowlspeed['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSPEED, $bowlspeed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowlspeed['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSPEED, $bowlspeed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_BOWLSPEED, $bowlspeed, $comparison);
    }

    /**
     * Filter the query on the bowlSwing column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlswing(1234); // WHERE bowlSwing = 1234
     * $query->filterByBowlswing(array(12, 34)); // WHERE bowlSwing IN (12, 34)
     * $query->filterByBowlswing(array('min' => 12)); // WHERE bowlSwing > 12
     * </code>
     *
     * @param     mixed $bowlswing The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByBowlswing($bowlswing = null, $comparison = null)
    {
        if (is_array($bowlswing)) {
            $useMinMax = false;
            if (isset($bowlswing['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSWING, $bowlswing['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowlswing['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSWING, $bowlswing['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_BOWLSWING, $bowlswing, $comparison);
    }

    /**
     * Filter the query on the bowlSpin column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlspin(1234); // WHERE bowlSpin = 1234
     * $query->filterByBowlspin(array(12, 34)); // WHERE bowlSpin IN (12, 34)
     * $query->filterByBowlspin(array('min' => 12)); // WHERE bowlSpin > 12
     * </code>
     *
     * @param     mixed $bowlspin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByBowlspin($bowlspin = null, $comparison = null)
    {
        if (is_array($bowlspin)) {
            $useMinMax = false;
            if (isset($bowlspin['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSPIN, $bowlspin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowlspin['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLSPIN, $bowlspin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_BOWLSPIN, $bowlspin, $comparison);
    }

    /**
     * Filter the query on the spinType column
     *
     * Example usage:
     * <code>
     * $query->filterBySpintype('fooValue');   // WHERE spinType = 'fooValue'
     * $query->filterBySpintype('%fooValue%', Criteria::LIKE); // WHERE spinType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $spintype The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterBySpintype($spintype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($spintype)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_SPINTYPE, $spintype, $comparison);
    }

    /**
     * Filter the query on the bowlAccurecy column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlaccurecy(1234); // WHERE bowlAccurecy = 1234
     * $query->filterByBowlaccurecy(array(12, 34)); // WHERE bowlAccurecy IN (12, 34)
     * $query->filterByBowlaccurecy(array('min' => 12)); // WHERE bowlAccurecy > 12
     * </code>
     *
     * @param     mixed $bowlaccurecy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByBowlaccurecy($bowlaccurecy = null, $comparison = null)
    {
        if (is_array($bowlaccurecy)) {
            $useMinMax = false;
            if (isset($bowlaccurecy['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLACCURECY, $bowlaccurecy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowlaccurecy['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_BOWLACCURECY, $bowlaccurecy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_BOWLACCURECY, $bowlaccurecy, $comparison);
    }

    /**
     * Filter the query on the shortBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByShortballstrength(1234); // WHERE shortBallStrength = 1234
     * $query->filterByShortballstrength(array(12, 34)); // WHERE shortBallStrength IN (12, 34)
     * $query->filterByShortballstrength(array('min' => 12)); // WHERE shortBallStrength > 12
     * </code>
     *
     * @param     mixed $shortballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByShortballstrength($shortballstrength = null, $comparison = null)
    {
        if (is_array($shortballstrength)) {
            $useMinMax = false;
            if (isset($shortballstrength['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shortballstrength['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength, $comparison);
    }

    /**
     * Filter the query on the lengthBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByLengthballstrength(1234); // WHERE lengthBallStrength = 1234
     * $query->filterByLengthballstrength(array(12, 34)); // WHERE lengthBallStrength IN (12, 34)
     * $query->filterByLengthballstrength(array('min' => 12)); // WHERE lengthBallStrength > 12
     * </code>
     *
     * @param     mixed $lengthballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByLengthballstrength($lengthballstrength = null, $comparison = null)
    {
        if (is_array($lengthballstrength)) {
            $useMinMax = false;
            if (isset($lengthballstrength['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lengthballstrength['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength, $comparison);
    }

    /**
     * Filter the query on the fullBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByFullballstrength(1234); // WHERE fullBallStrength = 1234
     * $query->filterByFullballstrength(array(12, 34)); // WHERE fullBallStrength IN (12, 34)
     * $query->filterByFullballstrength(array('min' => 12)); // WHERE fullBallStrength > 12
     * </code>
     *
     * @param     mixed $fullballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByFullballstrength($fullballstrength = null, $comparison = null)
    {
        if (is_array($fullballstrength)) {
            $useMinMax = false;
            if (isset($fullballstrength['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fullballstrength['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength, $comparison);
    }

    /**
     * Filter the query on the temprament column
     *
     * Example usage:
     * <code>
     * $query->filterByTemprament(1234); // WHERE temprament = 1234
     * $query->filterByTemprament(array(12, 34)); // WHERE temprament IN (12, 34)
     * $query->filterByTemprament(array('min' => 12)); // WHERE temprament > 12
     * </code>
     *
     * @param     mixed $temprament The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByTemprament($temprament = null, $comparison = null)
    {
        if (is_array($temprament)) {
            $useMinMax = false;
            if (isset($temprament['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_TEMPRAMENT, $temprament['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($temprament['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_TEMPRAMENT, $temprament['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_TEMPRAMENT, $temprament, $comparison);
    }

    /**
     * Filter the query on the average column
     *
     * Example usage:
     * <code>
     * $query->filterByAverage(1234); // WHERE average = 1234
     * $query->filterByAverage(array(12, 34)); // WHERE average IN (12, 34)
     * $query->filterByAverage(array('min' => 12)); // WHERE average > 12
     * </code>
     *
     * @param     mixed $average The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function filterByAverage($average = null, $comparison = null)
    {
        if (is_array($average)) {
            $useMinMax = false;
            if (isset($average['min'])) {
                $this->addUsingAlias(BowlingTableMap::COL_AVERAGE, $average['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($average['max'])) {
                $this->addUsingAlias(BowlingTableMap::COL_AVERAGE, $average['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BowlingTableMap::COL_AVERAGE, $average, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBowling $bowling Object to remove from the list of results
     *
     * @return $this|ChildBowlingQuery The current query, for fluid interface
     */
    public function prune($bowling = null)
    {
        if ($bowling) {
            $this->addUsingAlias(BowlingTableMap::COL_ID, $bowling->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bowling table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BowlingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BowlingTableMap::clearInstancePool();
            BowlingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BowlingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BowlingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BowlingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BowlingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BowlingQuery
