<?php

namespace Base;

use \Jerseys as ChildJerseys;
use \JerseysQuery as ChildJerseysQuery;
use \Exception;
use \PDO;
use Map\JerseysTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'jerseys' table.
 *
 *
 *
 * @method     ChildJerseysQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildJerseysQuery orderByTeamId($order = Criteria::ASC) Order by the team_id column
 * @method     ChildJerseysQuery orderByShirtColor($order = Criteria::ASC) Order by the shirt_color column
 * @method     ChildJerseysQuery orderByShirtPatternColor($order = Criteria::ASC) Order by the shirt_pattern_color column
 * @method     ChildJerseysQuery orderByShirtSleveColor($order = Criteria::ASC) Order by the shirt_sleve_color column
 * @method     ChildJerseysQuery orderByPantColor($order = Criteria::ASC) Order by the pant_color column
 * @method     ChildJerseysQuery orderByCapColor($order = Criteria::ASC) Order by the cap_color column
 * @method     ChildJerseysQuery orderByStudioLogoColor($order = Criteria::ASC) Order by the studio_logo_color column
 * @method     ChildJerseysQuery orderByNumberColor($order = Criteria::ASC) Order by the number_color column
 * @method     ChildJerseysQuery orderByPantPatternColor($order = Criteria::ASC) Order by the pant_pattern_color column
 * @method     ChildJerseysQuery orderByShirtLineColor($order = Criteria::ASC) Order by the shirt_line_color column
 * @method     ChildJerseysQuery orderByPantTexture($order = Criteria::ASC) Order by the pant_texture column
 * @method     ChildJerseysQuery orderByShoeColor($order = Criteria::ASC) Order by the shoe_color column
 * @method     ChildJerseysQuery orderByShoeSoleColor($order = Criteria::ASC) Order by the shoe_sole_color column
 * @method     ChildJerseysQuery orderBySocksColor($order = Criteria::ASC) Order by the socks_color column
 * @method     ChildJerseysQuery orderByShirtTexture($order = Criteria::ASC) Order by the shirt_texture column
 *
 * @method     ChildJerseysQuery groupById() Group by the id column
 * @method     ChildJerseysQuery groupByTeamId() Group by the team_id column
 * @method     ChildJerseysQuery groupByShirtColor() Group by the shirt_color column
 * @method     ChildJerseysQuery groupByShirtPatternColor() Group by the shirt_pattern_color column
 * @method     ChildJerseysQuery groupByShirtSleveColor() Group by the shirt_sleve_color column
 * @method     ChildJerseysQuery groupByPantColor() Group by the pant_color column
 * @method     ChildJerseysQuery groupByCapColor() Group by the cap_color column
 * @method     ChildJerseysQuery groupByStudioLogoColor() Group by the studio_logo_color column
 * @method     ChildJerseysQuery groupByNumberColor() Group by the number_color column
 * @method     ChildJerseysQuery groupByPantPatternColor() Group by the pant_pattern_color column
 * @method     ChildJerseysQuery groupByShirtLineColor() Group by the shirt_line_color column
 * @method     ChildJerseysQuery groupByPantTexture() Group by the pant_texture column
 * @method     ChildJerseysQuery groupByShoeColor() Group by the shoe_color column
 * @method     ChildJerseysQuery groupByShoeSoleColor() Group by the shoe_sole_color column
 * @method     ChildJerseysQuery groupBySocksColor() Group by the socks_color column
 * @method     ChildJerseysQuery groupByShirtTexture() Group by the shirt_texture column
 *
 * @method     ChildJerseysQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildJerseysQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildJerseysQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildJerseysQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildJerseysQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildJerseysQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildJerseys findOne(ConnectionInterface $con = null) Return the first ChildJerseys matching the query
 * @method     ChildJerseys findOneOrCreate(ConnectionInterface $con = null) Return the first ChildJerseys matching the query, or a new ChildJerseys object populated from the query conditions when no match is found
 *
 * @method     ChildJerseys findOneById(int $id) Return the first ChildJerseys filtered by the id column
 * @method     ChildJerseys findOneByTeamId(int $team_id) Return the first ChildJerseys filtered by the team_id column
 * @method     ChildJerseys findOneByShirtColor(string $shirt_color) Return the first ChildJerseys filtered by the shirt_color column
 * @method     ChildJerseys findOneByShirtPatternColor(string $shirt_pattern_color) Return the first ChildJerseys filtered by the shirt_pattern_color column
 * @method     ChildJerseys findOneByShirtSleveColor(string $shirt_sleve_color) Return the first ChildJerseys filtered by the shirt_sleve_color column
 * @method     ChildJerseys findOneByPantColor(string $pant_color) Return the first ChildJerseys filtered by the pant_color column
 * @method     ChildJerseys findOneByCapColor(string $cap_color) Return the first ChildJerseys filtered by the cap_color column
 * @method     ChildJerseys findOneByStudioLogoColor(string $studio_logo_color) Return the first ChildJerseys filtered by the studio_logo_color column
 * @method     ChildJerseys findOneByNumberColor(string $number_color) Return the first ChildJerseys filtered by the number_color column
 * @method     ChildJerseys findOneByPantPatternColor(string $pant_pattern_color) Return the first ChildJerseys filtered by the pant_pattern_color column
 * @method     ChildJerseys findOneByShirtLineColor(string $shirt_line_color) Return the first ChildJerseys filtered by the shirt_line_color column
 * @method     ChildJerseys findOneByPantTexture(string $pant_texture) Return the first ChildJerseys filtered by the pant_texture column
 * @method     ChildJerseys findOneByShoeColor(string $shoe_color) Return the first ChildJerseys filtered by the shoe_color column
 * @method     ChildJerseys findOneByShoeSoleColor(string $shoe_sole_color) Return the first ChildJerseys filtered by the shoe_sole_color column
 * @method     ChildJerseys findOneBySocksColor(string $socks_color) Return the first ChildJerseys filtered by the socks_color column
 * @method     ChildJerseys findOneByShirtTexture(string $shirt_texture) Return the first ChildJerseys filtered by the shirt_texture column *

 * @method     ChildJerseys requirePk($key, ConnectionInterface $con = null) Return the ChildJerseys by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOne(ConnectionInterface $con = null) Return the first ChildJerseys matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildJerseys requireOneById(int $id) Return the first ChildJerseys filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByTeamId(int $team_id) Return the first ChildJerseys filtered by the team_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShirtColor(string $shirt_color) Return the first ChildJerseys filtered by the shirt_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShirtPatternColor(string $shirt_pattern_color) Return the first ChildJerseys filtered by the shirt_pattern_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShirtSleveColor(string $shirt_sleve_color) Return the first ChildJerseys filtered by the shirt_sleve_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByPantColor(string $pant_color) Return the first ChildJerseys filtered by the pant_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByCapColor(string $cap_color) Return the first ChildJerseys filtered by the cap_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByStudioLogoColor(string $studio_logo_color) Return the first ChildJerseys filtered by the studio_logo_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByNumberColor(string $number_color) Return the first ChildJerseys filtered by the number_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByPantPatternColor(string $pant_pattern_color) Return the first ChildJerseys filtered by the pant_pattern_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShirtLineColor(string $shirt_line_color) Return the first ChildJerseys filtered by the shirt_line_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByPantTexture(string $pant_texture) Return the first ChildJerseys filtered by the pant_texture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShoeColor(string $shoe_color) Return the first ChildJerseys filtered by the shoe_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShoeSoleColor(string $shoe_sole_color) Return the first ChildJerseys filtered by the shoe_sole_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneBySocksColor(string $socks_color) Return the first ChildJerseys filtered by the socks_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildJerseys requireOneByShirtTexture(string $shirt_texture) Return the first ChildJerseys filtered by the shirt_texture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildJerseys[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildJerseys objects based on current ModelCriteria
 * @method     ChildJerseys[]|ObjectCollection findById(int $id) Return ChildJerseys objects filtered by the id column
 * @method     ChildJerseys[]|ObjectCollection findByTeamId(int $team_id) Return ChildJerseys objects filtered by the team_id column
 * @method     ChildJerseys[]|ObjectCollection findByShirtColor(string $shirt_color) Return ChildJerseys objects filtered by the shirt_color column
 * @method     ChildJerseys[]|ObjectCollection findByShirtPatternColor(string $shirt_pattern_color) Return ChildJerseys objects filtered by the shirt_pattern_color column
 * @method     ChildJerseys[]|ObjectCollection findByShirtSleveColor(string $shirt_sleve_color) Return ChildJerseys objects filtered by the shirt_sleve_color column
 * @method     ChildJerseys[]|ObjectCollection findByPantColor(string $pant_color) Return ChildJerseys objects filtered by the pant_color column
 * @method     ChildJerseys[]|ObjectCollection findByCapColor(string $cap_color) Return ChildJerseys objects filtered by the cap_color column
 * @method     ChildJerseys[]|ObjectCollection findByStudioLogoColor(string $studio_logo_color) Return ChildJerseys objects filtered by the studio_logo_color column
 * @method     ChildJerseys[]|ObjectCollection findByNumberColor(string $number_color) Return ChildJerseys objects filtered by the number_color column
 * @method     ChildJerseys[]|ObjectCollection findByPantPatternColor(string $pant_pattern_color) Return ChildJerseys objects filtered by the pant_pattern_color column
 * @method     ChildJerseys[]|ObjectCollection findByShirtLineColor(string $shirt_line_color) Return ChildJerseys objects filtered by the shirt_line_color column
 * @method     ChildJerseys[]|ObjectCollection findByPantTexture(string $pant_texture) Return ChildJerseys objects filtered by the pant_texture column
 * @method     ChildJerseys[]|ObjectCollection findByShoeColor(string $shoe_color) Return ChildJerseys objects filtered by the shoe_color column
 * @method     ChildJerseys[]|ObjectCollection findByShoeSoleColor(string $shoe_sole_color) Return ChildJerseys objects filtered by the shoe_sole_color column
 * @method     ChildJerseys[]|ObjectCollection findBySocksColor(string $socks_color) Return ChildJerseys objects filtered by the socks_color column
 * @method     ChildJerseys[]|ObjectCollection findByShirtTexture(string $shirt_texture) Return ChildJerseys objects filtered by the shirt_texture column
 * @method     ChildJerseys[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class JerseysQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\JerseysQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Jerseys', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildJerseysQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildJerseysQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildJerseysQuery) {
            return $criteria;
        }
        $query = new ChildJerseysQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildJerseys|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(JerseysTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = JerseysTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildJerseys A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, team_id, shirt_color, shirt_pattern_color, shirt_sleve_color, pant_color, cap_color, studio_logo_color, number_color, pant_pattern_color, shirt_line_color, pant_texture, shoe_color, shoe_sole_color, socks_color, shirt_texture FROM jerseys WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildJerseys $obj */
            $obj = new ChildJerseys();
            $obj->hydrate($row);
            JerseysTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildJerseys|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JerseysTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JerseysTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(JerseysTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(JerseysTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the team_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamId(1234); // WHERE team_id = 1234
     * $query->filterByTeamId(array(12, 34)); // WHERE team_id IN (12, 34)
     * $query->filterByTeamId(array('min' => 12)); // WHERE team_id > 12
     * </code>
     *
     * @param     mixed $teamId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByTeamId($teamId = null, $comparison = null)
    {
        if (is_array($teamId)) {
            $useMinMax = false;
            if (isset($teamId['min'])) {
                $this->addUsingAlias(JerseysTableMap::COL_TEAM_ID, $teamId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($teamId['max'])) {
                $this->addUsingAlias(JerseysTableMap::COL_TEAM_ID, $teamId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_TEAM_ID, $teamId, $comparison);
    }

    /**
     * Filter the query on the shirt_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShirtColor('fooValue');   // WHERE shirt_color = 'fooValue'
     * $query->filterByShirtColor('%fooValue%', Criteria::LIKE); // WHERE shirt_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shirtColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShirtColor($shirtColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shirtColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHIRT_COLOR, $shirtColor, $comparison);
    }

    /**
     * Filter the query on the shirt_pattern_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShirtPatternColor('fooValue');   // WHERE shirt_pattern_color = 'fooValue'
     * $query->filterByShirtPatternColor('%fooValue%', Criteria::LIKE); // WHERE shirt_pattern_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shirtPatternColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShirtPatternColor($shirtPatternColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shirtPatternColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHIRT_PATTERN_COLOR, $shirtPatternColor, $comparison);
    }

    /**
     * Filter the query on the shirt_sleve_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShirtSleveColor('fooValue');   // WHERE shirt_sleve_color = 'fooValue'
     * $query->filterByShirtSleveColor('%fooValue%', Criteria::LIKE); // WHERE shirt_sleve_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shirtSleveColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShirtSleveColor($shirtSleveColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shirtSleveColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHIRT_SLEVE_COLOR, $shirtSleveColor, $comparison);
    }

    /**
     * Filter the query on the pant_color column
     *
     * Example usage:
     * <code>
     * $query->filterByPantColor('fooValue');   // WHERE pant_color = 'fooValue'
     * $query->filterByPantColor('%fooValue%', Criteria::LIKE); // WHERE pant_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pantColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByPantColor($pantColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pantColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_PANT_COLOR, $pantColor, $comparison);
    }

    /**
     * Filter the query on the cap_color column
     *
     * Example usage:
     * <code>
     * $query->filterByCapColor('fooValue');   // WHERE cap_color = 'fooValue'
     * $query->filterByCapColor('%fooValue%', Criteria::LIKE); // WHERE cap_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $capColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByCapColor($capColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($capColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_CAP_COLOR, $capColor, $comparison);
    }

    /**
     * Filter the query on the studio_logo_color column
     *
     * Example usage:
     * <code>
     * $query->filterByStudioLogoColor('fooValue');   // WHERE studio_logo_color = 'fooValue'
     * $query->filterByStudioLogoColor('%fooValue%', Criteria::LIKE); // WHERE studio_logo_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $studioLogoColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByStudioLogoColor($studioLogoColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($studioLogoColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_STUDIO_LOGO_COLOR, $studioLogoColor, $comparison);
    }

    /**
     * Filter the query on the number_color column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberColor('fooValue');   // WHERE number_color = 'fooValue'
     * $query->filterByNumberColor('%fooValue%', Criteria::LIKE); // WHERE number_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numberColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByNumberColor($numberColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numberColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_NUMBER_COLOR, $numberColor, $comparison);
    }

    /**
     * Filter the query on the pant_pattern_color column
     *
     * Example usage:
     * <code>
     * $query->filterByPantPatternColor('fooValue');   // WHERE pant_pattern_color = 'fooValue'
     * $query->filterByPantPatternColor('%fooValue%', Criteria::LIKE); // WHERE pant_pattern_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pantPatternColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByPantPatternColor($pantPatternColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pantPatternColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_PANT_PATTERN_COLOR, $pantPatternColor, $comparison);
    }

    /**
     * Filter the query on the shirt_line_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShirtLineColor('fooValue');   // WHERE shirt_line_color = 'fooValue'
     * $query->filterByShirtLineColor('%fooValue%', Criteria::LIKE); // WHERE shirt_line_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shirtLineColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShirtLineColor($shirtLineColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shirtLineColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHIRT_LINE_COLOR, $shirtLineColor, $comparison);
    }

    /**
     * Filter the query on the pant_texture column
     *
     * Example usage:
     * <code>
     * $query->filterByPantTexture('fooValue');   // WHERE pant_texture = 'fooValue'
     * $query->filterByPantTexture('%fooValue%', Criteria::LIKE); // WHERE pant_texture LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pantTexture The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByPantTexture($pantTexture = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pantTexture)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_PANT_TEXTURE, $pantTexture, $comparison);
    }

    /**
     * Filter the query on the shoe_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShoeColor('fooValue');   // WHERE shoe_color = 'fooValue'
     * $query->filterByShoeColor('%fooValue%', Criteria::LIKE); // WHERE shoe_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shoeColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShoeColor($shoeColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shoeColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHOE_COLOR, $shoeColor, $comparison);
    }

    /**
     * Filter the query on the shoe_sole_color column
     *
     * Example usage:
     * <code>
     * $query->filterByShoeSoleColor('fooValue');   // WHERE shoe_sole_color = 'fooValue'
     * $query->filterByShoeSoleColor('%fooValue%', Criteria::LIKE); // WHERE shoe_sole_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shoeSoleColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShoeSoleColor($shoeSoleColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shoeSoleColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHOE_SOLE_COLOR, $shoeSoleColor, $comparison);
    }

    /**
     * Filter the query on the socks_color column
     *
     * Example usage:
     * <code>
     * $query->filterBySocksColor('fooValue');   // WHERE socks_color = 'fooValue'
     * $query->filterBySocksColor('%fooValue%', Criteria::LIKE); // WHERE socks_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $socksColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterBySocksColor($socksColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($socksColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SOCKS_COLOR, $socksColor, $comparison);
    }

    /**
     * Filter the query on the shirt_texture column
     *
     * Example usage:
     * <code>
     * $query->filterByShirtTexture('fooValue');   // WHERE shirt_texture = 'fooValue'
     * $query->filterByShirtTexture('%fooValue%', Criteria::LIKE); // WHERE shirt_texture LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shirtTexture The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function filterByShirtTexture($shirtTexture = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shirtTexture)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JerseysTableMap::COL_SHIRT_TEXTURE, $shirtTexture, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildJerseys $jerseys Object to remove from the list of results
     *
     * @return $this|ChildJerseysQuery The current query, for fluid interface
     */
    public function prune($jerseys = null)
    {
        if ($jerseys) {
            $this->addUsingAlias(JerseysTableMap::COL_ID, $jerseys->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the jerseys table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            JerseysTableMap::clearInstancePool();
            JerseysTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(JerseysTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            JerseysTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            JerseysTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // JerseysQuery
