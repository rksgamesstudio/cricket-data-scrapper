<?php

namespace Base;

use \Players as ChildPlayers;
use \PlayersQuery as ChildPlayersQuery;
use \Exception;
use \PDO;
use Map\PlayersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'players' table.
 *
 *
 *
 * @method     ChildPlayersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPlayersQuery orderByFirstname($order = Criteria::ASC) Order by the firstName column
 * @method     ChildPlayersQuery orderByLastname($order = Criteria::ASC) Order by the lastName column
 * @method     ChildPlayersQuery orderByJerseynumber($order = Criteria::ASC) Order by the jerseyNumber column
 * @method     ChildPlayersQuery orderByJerseyname($order = Criteria::ASC) Order by the jerseyName column
 * @method     ChildPlayersQuery orderByBattinghand($order = Criteria::ASC) Order by the battingHand column
 * @method     ChildPlayersQuery orderByBowlinghand($order = Criteria::ASC) Order by the bowlingHand column
 * @method     ChildPlayersQuery orderByFieldinghand($order = Criteria::ASC) Order by the fieldingHand column
 * @method     ChildPlayersQuery orderByModelname($order = Criteria::ASC) Order by the modelName column
 * @method     ChildPlayersQuery orderByModeltexture($order = Criteria::ASC) Order by the modelTexture column
 * @method     ChildPlayersQuery orderByPrimaryrole($order = Criteria::ASC) Order by the primaryRole column
 * @method     ChildPlayersQuery orderByBattingorder($order = Criteria::ASC) Order by the battingOrder column
 * @method     ChildPlayersQuery orderByRace($order = Criteria::ASC) Order by the race column
 * @method     ChildPlayersQuery orderBySourceurl($order = Criteria::ASC) Order by the sourceUrl column
 * @method     ChildPlayersQuery orderByTransfervalue($order = Criteria::ASC) Order by the transferValue column
 *
 * @method     ChildPlayersQuery groupById() Group by the id column
 * @method     ChildPlayersQuery groupByFirstname() Group by the firstName column
 * @method     ChildPlayersQuery groupByLastname() Group by the lastName column
 * @method     ChildPlayersQuery groupByJerseynumber() Group by the jerseyNumber column
 * @method     ChildPlayersQuery groupByJerseyname() Group by the jerseyName column
 * @method     ChildPlayersQuery groupByBattinghand() Group by the battingHand column
 * @method     ChildPlayersQuery groupByBowlinghand() Group by the bowlingHand column
 * @method     ChildPlayersQuery groupByFieldinghand() Group by the fieldingHand column
 * @method     ChildPlayersQuery groupByModelname() Group by the modelName column
 * @method     ChildPlayersQuery groupByModeltexture() Group by the modelTexture column
 * @method     ChildPlayersQuery groupByPrimaryrole() Group by the primaryRole column
 * @method     ChildPlayersQuery groupByBattingorder() Group by the battingOrder column
 * @method     ChildPlayersQuery groupByRace() Group by the race column
 * @method     ChildPlayersQuery groupBySourceurl() Group by the sourceUrl column
 * @method     ChildPlayersQuery groupByTransfervalue() Group by the transferValue column
 *
 * @method     ChildPlayersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlayersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlayersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlayersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlayersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlayersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlayers findOne(ConnectionInterface $con = null) Return the first ChildPlayers matching the query
 * @method     ChildPlayers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlayers matching the query, or a new ChildPlayers object populated from the query conditions when no match is found
 *
 * @method     ChildPlayers findOneById(int $id) Return the first ChildPlayers filtered by the id column
 * @method     ChildPlayers findOneByFirstname(string $firstName) Return the first ChildPlayers filtered by the firstName column
 * @method     ChildPlayers findOneByLastname(string $lastName) Return the first ChildPlayers filtered by the lastName column
 * @method     ChildPlayers findOneByJerseynumber(string $jerseyNumber) Return the first ChildPlayers filtered by the jerseyNumber column
 * @method     ChildPlayers findOneByJerseyname(string $jerseyName) Return the first ChildPlayers filtered by the jerseyName column
 * @method     ChildPlayers findOneByBattinghand(string $battingHand) Return the first ChildPlayers filtered by the battingHand column
 * @method     ChildPlayers findOneByBowlinghand(string $bowlingHand) Return the first ChildPlayers filtered by the bowlingHand column
 * @method     ChildPlayers findOneByFieldinghand(string $fieldingHand) Return the first ChildPlayers filtered by the fieldingHand column
 * @method     ChildPlayers findOneByModelname(string $modelName) Return the first ChildPlayers filtered by the modelName column
 * @method     ChildPlayers findOneByModeltexture(string $modelTexture) Return the first ChildPlayers filtered by the modelTexture column
 * @method     ChildPlayers findOneByPrimaryrole(string $primaryRole) Return the first ChildPlayers filtered by the primaryRole column
 * @method     ChildPlayers findOneByBattingorder(int $battingOrder) Return the first ChildPlayers filtered by the battingOrder column
 * @method     ChildPlayers findOneByRace(string $race) Return the first ChildPlayers filtered by the race column
 * @method     ChildPlayers findOneBySourceurl(string $sourceUrl) Return the first ChildPlayers filtered by the sourceUrl column
 * @method     ChildPlayers findOneByTransfervalue(double $transferValue) Return the first ChildPlayers filtered by the transferValue column *

 * @method     ChildPlayers requirePk($key, ConnectionInterface $con = null) Return the ChildPlayers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOne(ConnectionInterface $con = null) Return the first ChildPlayers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayers requireOneById(int $id) Return the first ChildPlayers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByFirstname(string $firstName) Return the first ChildPlayers filtered by the firstName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByLastname(string $lastName) Return the first ChildPlayers filtered by the lastName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByJerseynumber(string $jerseyNumber) Return the first ChildPlayers filtered by the jerseyNumber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByJerseyname(string $jerseyName) Return the first ChildPlayers filtered by the jerseyName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByBattinghand(string $battingHand) Return the first ChildPlayers filtered by the battingHand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByBowlinghand(string $bowlingHand) Return the first ChildPlayers filtered by the bowlingHand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByFieldinghand(string $fieldingHand) Return the first ChildPlayers filtered by the fieldingHand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByModelname(string $modelName) Return the first ChildPlayers filtered by the modelName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByModeltexture(string $modelTexture) Return the first ChildPlayers filtered by the modelTexture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByPrimaryrole(string $primaryRole) Return the first ChildPlayers filtered by the primaryRole column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByBattingorder(int $battingOrder) Return the first ChildPlayers filtered by the battingOrder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByRace(string $race) Return the first ChildPlayers filtered by the race column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneBySourceurl(string $sourceUrl) Return the first ChildPlayers filtered by the sourceUrl column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayers requireOneByTransfervalue(double $transferValue) Return the first ChildPlayers filtered by the transferValue column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlayers objects based on current ModelCriteria
 * @method     ChildPlayers[]|ObjectCollection findById(int $id) Return ChildPlayers objects filtered by the id column
 * @method     ChildPlayers[]|ObjectCollection findByFirstname(string $firstName) Return ChildPlayers objects filtered by the firstName column
 * @method     ChildPlayers[]|ObjectCollection findByLastname(string $lastName) Return ChildPlayers objects filtered by the lastName column
 * @method     ChildPlayers[]|ObjectCollection findByJerseynumber(string $jerseyNumber) Return ChildPlayers objects filtered by the jerseyNumber column
 * @method     ChildPlayers[]|ObjectCollection findByJerseyname(string $jerseyName) Return ChildPlayers objects filtered by the jerseyName column
 * @method     ChildPlayers[]|ObjectCollection findByBattinghand(string $battingHand) Return ChildPlayers objects filtered by the battingHand column
 * @method     ChildPlayers[]|ObjectCollection findByBowlinghand(string $bowlingHand) Return ChildPlayers objects filtered by the bowlingHand column
 * @method     ChildPlayers[]|ObjectCollection findByFieldinghand(string $fieldingHand) Return ChildPlayers objects filtered by the fieldingHand column
 * @method     ChildPlayers[]|ObjectCollection findByModelname(string $modelName) Return ChildPlayers objects filtered by the modelName column
 * @method     ChildPlayers[]|ObjectCollection findByModeltexture(string $modelTexture) Return ChildPlayers objects filtered by the modelTexture column
 * @method     ChildPlayers[]|ObjectCollection findByPrimaryrole(string $primaryRole) Return ChildPlayers objects filtered by the primaryRole column
 * @method     ChildPlayers[]|ObjectCollection findByBattingorder(int $battingOrder) Return ChildPlayers objects filtered by the battingOrder column
 * @method     ChildPlayers[]|ObjectCollection findByRace(string $race) Return ChildPlayers objects filtered by the race column
 * @method     ChildPlayers[]|ObjectCollection findBySourceurl(string $sourceUrl) Return ChildPlayers objects filtered by the sourceUrl column
 * @method     ChildPlayers[]|ObjectCollection findByTransfervalue(double $transferValue) Return ChildPlayers objects filtered by the transferValue column
 * @method     ChildPlayers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlayersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PlayersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Players', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlayersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlayersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlayersQuery) {
            return $criteria;
        }
        $query = new ChildPlayersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlayers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlayersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlayersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlayers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, firstName, lastName, jerseyNumber, jerseyName, battingHand, bowlingHand, fieldingHand, modelName, modelTexture, primaryRole, battingOrder, race, sourceUrl, transferValue FROM players WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlayers $obj */
            $obj = new ChildPlayers();
            $obj->hydrate($row);
            PlayersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlayers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlayersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlayersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PlayersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PlayersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the firstName column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstName = 'fooValue'
     * $query->filterByFirstname('%fooValue%', Criteria::LIKE); // WHERE firstName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the lastName column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastName = 'fooValue'
     * $query->filterByLastname('%fooValue%', Criteria::LIKE); // WHERE lastName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the jerseyNumber column
     *
     * Example usage:
     * <code>
     * $query->filterByJerseynumber('fooValue');   // WHERE jerseyNumber = 'fooValue'
     * $query->filterByJerseynumber('%fooValue%', Criteria::LIKE); // WHERE jerseyNumber LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jerseynumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByJerseynumber($jerseynumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jerseynumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_JERSEYNUMBER, $jerseynumber, $comparison);
    }

    /**
     * Filter the query on the jerseyName column
     *
     * Example usage:
     * <code>
     * $query->filterByJerseyname('fooValue');   // WHERE jerseyName = 'fooValue'
     * $query->filterByJerseyname('%fooValue%', Criteria::LIKE); // WHERE jerseyName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jerseyname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByJerseyname($jerseyname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jerseyname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_JERSEYNAME, $jerseyname, $comparison);
    }

    /**
     * Filter the query on the battingHand column
     *
     * Example usage:
     * <code>
     * $query->filterByBattinghand('fooValue');   // WHERE battingHand = 'fooValue'
     * $query->filterByBattinghand('%fooValue%', Criteria::LIKE); // WHERE battingHand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $battinghand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByBattinghand($battinghand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($battinghand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_BATTINGHAND, $battinghand, $comparison);
    }

    /**
     * Filter the query on the bowlingHand column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlinghand('fooValue');   // WHERE bowlingHand = 'fooValue'
     * $query->filterByBowlinghand('%fooValue%', Criteria::LIKE); // WHERE bowlingHand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bowlinghand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByBowlinghand($bowlinghand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bowlinghand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_BOWLINGHAND, $bowlinghand, $comparison);
    }

    /**
     * Filter the query on the fieldingHand column
     *
     * Example usage:
     * <code>
     * $query->filterByFieldinghand('fooValue');   // WHERE fieldingHand = 'fooValue'
     * $query->filterByFieldinghand('%fooValue%', Criteria::LIKE); // WHERE fieldingHand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fieldinghand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByFieldinghand($fieldinghand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fieldinghand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_FIELDINGHAND, $fieldinghand, $comparison);
    }

    /**
     * Filter the query on the modelName column
     *
     * Example usage:
     * <code>
     * $query->filterByModelname('fooValue');   // WHERE modelName = 'fooValue'
     * $query->filterByModelname('%fooValue%', Criteria::LIKE); // WHERE modelName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modelname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByModelname($modelname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modelname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_MODELNAME, $modelname, $comparison);
    }

    /**
     * Filter the query on the modelTexture column
     *
     * Example usage:
     * <code>
     * $query->filterByModeltexture('fooValue');   // WHERE modelTexture = 'fooValue'
     * $query->filterByModeltexture('%fooValue%', Criteria::LIKE); // WHERE modelTexture LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modeltexture The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByModeltexture($modeltexture = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modeltexture)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_MODELTEXTURE, $modeltexture, $comparison);
    }

    /**
     * Filter the query on the primaryRole column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimaryrole('fooValue');   // WHERE primaryRole = 'fooValue'
     * $query->filterByPrimaryrole('%fooValue%', Criteria::LIKE); // WHERE primaryRole LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primaryrole The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByPrimaryrole($primaryrole = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primaryrole)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_PRIMARYROLE, $primaryrole, $comparison);
    }

    /**
     * Filter the query on the battingOrder column
     *
     * Example usage:
     * <code>
     * $query->filterByBattingorder(1234); // WHERE battingOrder = 1234
     * $query->filterByBattingorder(array(12, 34)); // WHERE battingOrder IN (12, 34)
     * $query->filterByBattingorder(array('min' => 12)); // WHERE battingOrder > 12
     * </code>
     *
     * @param     mixed $battingorder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByBattingorder($battingorder = null, $comparison = null)
    {
        if (is_array($battingorder)) {
            $useMinMax = false;
            if (isset($battingorder['min'])) {
                $this->addUsingAlias(PlayersTableMap::COL_BATTINGORDER, $battingorder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($battingorder['max'])) {
                $this->addUsingAlias(PlayersTableMap::COL_BATTINGORDER, $battingorder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_BATTINGORDER, $battingorder, $comparison);
    }

    /**
     * Filter the query on the race column
     *
     * Example usage:
     * <code>
     * $query->filterByRace('fooValue');   // WHERE race = 'fooValue'
     * $query->filterByRace('%fooValue%', Criteria::LIKE); // WHERE race LIKE '%fooValue%'
     * </code>
     *
     * @param     string $race The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByRace($race = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($race)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_RACE, $race, $comparison);
    }

    /**
     * Filter the query on the sourceUrl column
     *
     * Example usage:
     * <code>
     * $query->filterBySourceurl('fooValue');   // WHERE sourceUrl = 'fooValue'
     * $query->filterBySourceurl('%fooValue%', Criteria::LIKE); // WHERE sourceUrl LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourceurl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterBySourceurl($sourceurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourceurl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_SOURCEURL, $sourceurl, $comparison);
    }

    /**
     * Filter the query on the transferValue column
     *
     * Example usage:
     * <code>
     * $query->filterByTransfervalue(1234); // WHERE transferValue = 1234
     * $query->filterByTransfervalue(array(12, 34)); // WHERE transferValue IN (12, 34)
     * $query->filterByTransfervalue(array('min' => 12)); // WHERE transferValue > 12
     * </code>
     *
     * @param     mixed $transfervalue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function filterByTransfervalue($transfervalue = null, $comparison = null)
    {
        if (is_array($transfervalue)) {
            $useMinMax = false;
            if (isset($transfervalue['min'])) {
                $this->addUsingAlias(PlayersTableMap::COL_TRANSFERVALUE, $transfervalue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($transfervalue['max'])) {
                $this->addUsingAlias(PlayersTableMap::COL_TRANSFERVALUE, $transfervalue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayersTableMap::COL_TRANSFERVALUE, $transfervalue, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlayers $players Object to remove from the list of results
     *
     * @return $this|ChildPlayersQuery The current query, for fluid interface
     */
    public function prune($players = null)
    {
        if ($players) {
            $this->addUsingAlias(PlayersTableMap::COL_ID, $players->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the players table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlayersTableMap::clearInstancePool();
            PlayersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlayersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlayersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlayersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlayersQuery
