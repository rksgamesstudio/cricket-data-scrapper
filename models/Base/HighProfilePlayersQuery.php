<?php

namespace Base;

use \HighProfilePlayers as ChildHighProfilePlayers;
use \HighProfilePlayersQuery as ChildHighProfilePlayersQuery;
use \Exception;
use \PDO;
use Map\HighProfilePlayersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'high_profile_players' table.
 *
 *
 *
 * @method     ChildHighProfilePlayersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildHighProfilePlayersQuery orderByCanonicalLink($order = Criteria::ASC) Order by the canonical_link column
 * @method     ChildHighProfilePlayersQuery orderByPriceRank($order = Criteria::ASC) Order by the price_rank column
 * @method     ChildHighProfilePlayersQuery orderByAppearance($order = Criteria::ASC) Order by the appearance column
 *
 * @method     ChildHighProfilePlayersQuery groupById() Group by the id column
 * @method     ChildHighProfilePlayersQuery groupByCanonicalLink() Group by the canonical_link column
 * @method     ChildHighProfilePlayersQuery groupByPriceRank() Group by the price_rank column
 * @method     ChildHighProfilePlayersQuery groupByAppearance() Group by the appearance column
 *
 * @method     ChildHighProfilePlayersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildHighProfilePlayersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildHighProfilePlayersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildHighProfilePlayersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildHighProfilePlayersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildHighProfilePlayersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildHighProfilePlayers findOne(ConnectionInterface $con = null) Return the first ChildHighProfilePlayers matching the query
 * @method     ChildHighProfilePlayers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildHighProfilePlayers matching the query, or a new ChildHighProfilePlayers object populated from the query conditions when no match is found
 *
 * @method     ChildHighProfilePlayers findOneById(int $id) Return the first ChildHighProfilePlayers filtered by the id column
 * @method     ChildHighProfilePlayers findOneByCanonicalLink(string $canonical_link) Return the first ChildHighProfilePlayers filtered by the canonical_link column
 * @method     ChildHighProfilePlayers findOneByPriceRank(int $price_rank) Return the first ChildHighProfilePlayers filtered by the price_rank column
 * @method     ChildHighProfilePlayers findOneByAppearance(string $appearance) Return the first ChildHighProfilePlayers filtered by the appearance column *

 * @method     ChildHighProfilePlayers requirePk($key, ConnectionInterface $con = null) Return the ChildHighProfilePlayers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildHighProfilePlayers requireOne(ConnectionInterface $con = null) Return the first ChildHighProfilePlayers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildHighProfilePlayers requireOneById(int $id) Return the first ChildHighProfilePlayers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildHighProfilePlayers requireOneByCanonicalLink(string $canonical_link) Return the first ChildHighProfilePlayers filtered by the canonical_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildHighProfilePlayers requireOneByPriceRank(int $price_rank) Return the first ChildHighProfilePlayers filtered by the price_rank column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildHighProfilePlayers requireOneByAppearance(string $appearance) Return the first ChildHighProfilePlayers filtered by the appearance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildHighProfilePlayers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildHighProfilePlayers objects based on current ModelCriteria
 * @method     ChildHighProfilePlayers[]|ObjectCollection findById(int $id) Return ChildHighProfilePlayers objects filtered by the id column
 * @method     ChildHighProfilePlayers[]|ObjectCollection findByCanonicalLink(string $canonical_link) Return ChildHighProfilePlayers objects filtered by the canonical_link column
 * @method     ChildHighProfilePlayers[]|ObjectCollection findByPriceRank(int $price_rank) Return ChildHighProfilePlayers objects filtered by the price_rank column
 * @method     ChildHighProfilePlayers[]|ObjectCollection findByAppearance(string $appearance) Return ChildHighProfilePlayers objects filtered by the appearance column
 * @method     ChildHighProfilePlayers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class HighProfilePlayersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\HighProfilePlayersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\HighProfilePlayers', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildHighProfilePlayersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildHighProfilePlayersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildHighProfilePlayersQuery) {
            return $criteria;
        }
        $query = new ChildHighProfilePlayersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildHighProfilePlayers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(HighProfilePlayersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = HighProfilePlayersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildHighProfilePlayers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, canonical_link, price_rank, appearance FROM high_profile_players WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildHighProfilePlayers $obj */
            $obj = new ChildHighProfilePlayers();
            $obj->hydrate($row);
            HighProfilePlayersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildHighProfilePlayers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the canonical_link column
     *
     * Example usage:
     * <code>
     * $query->filterByCanonicalLink('fooValue');   // WHERE canonical_link = 'fooValue'
     * $query->filterByCanonicalLink('%fooValue%', Criteria::LIKE); // WHERE canonical_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $canonicalLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterByCanonicalLink($canonicalLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($canonicalLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_CANONICAL_LINK, $canonicalLink, $comparison);
    }

    /**
     * Filter the query on the price_rank column
     *
     * Example usage:
     * <code>
     * $query->filterByPriceRank(1234); // WHERE price_rank = 1234
     * $query->filterByPriceRank(array(12, 34)); // WHERE price_rank IN (12, 34)
     * $query->filterByPriceRank(array('min' => 12)); // WHERE price_rank > 12
     * </code>
     *
     * @param     mixed $priceRank The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterByPriceRank($priceRank = null, $comparison = null)
    {
        if (is_array($priceRank)) {
            $useMinMax = false;
            if (isset($priceRank['min'])) {
                $this->addUsingAlias(HighProfilePlayersTableMap::COL_PRICE_RANK, $priceRank['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priceRank['max'])) {
                $this->addUsingAlias(HighProfilePlayersTableMap::COL_PRICE_RANK, $priceRank['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_PRICE_RANK, $priceRank, $comparison);
    }

    /**
     * Filter the query on the appearance column
     *
     * Example usage:
     * <code>
     * $query->filterByAppearance('fooValue');   // WHERE appearance = 'fooValue'
     * $query->filterByAppearance('%fooValue%', Criteria::LIKE); // WHERE appearance LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appearance The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function filterByAppearance($appearance = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appearance)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(HighProfilePlayersTableMap::COL_APPEARANCE, $appearance, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildHighProfilePlayers $highProfilePlayers Object to remove from the list of results
     *
     * @return $this|ChildHighProfilePlayersQuery The current query, for fluid interface
     */
    public function prune($highProfilePlayers = null)
    {
        if ($highProfilePlayers) {
            $this->addUsingAlias(HighProfilePlayersTableMap::COL_ID, $highProfilePlayers->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the high_profile_players table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(HighProfilePlayersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            HighProfilePlayersTableMap::clearInstancePool();
            HighProfilePlayersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(HighProfilePlayersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(HighProfilePlayersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            HighProfilePlayersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            HighProfilePlayersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // HighProfilePlayersQuery
