<?php

namespace Base;

use \PlayerAppearance as ChildPlayerAppearance;
use \PlayerAppearanceQuery as ChildPlayerAppearanceQuery;
use \Exception;
use \PDO;
use Map\PlayerAppearanceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'players_appearance' table.
 *
 *
 *
 * @method     ChildPlayerAppearanceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPlayerAppearanceQuery orderByPlayer($order = Criteria::ASC) Order by the player column
 * @method     ChildPlayerAppearanceQuery orderByMeshId($order = Criteria::ASC) Order by the meshId column
 * @method     ChildPlayerAppearanceQuery orderByHairStyleId($order = Criteria::ASC) Order by the hairStyleId column
 * @method     ChildPlayerAppearanceQuery orderByBeardStyleId($order = Criteria::ASC) Order by the beardStyleId column
 * @method     ChildPlayerAppearanceQuery orderByHairColorId($order = Criteria::ASC) Order by the hairColorId column
 * @method     ChildPlayerAppearanceQuery orderByBeardColorId($order = Criteria::ASC) Order by the beardColorId column
 * @method     ChildPlayerAppearanceQuery orderByNeckTatooId($order = Criteria::ASC) Order by the neckTatooId column
 * @method     ChildPlayerAppearanceQuery orderByLeftArmTatooId($order = Criteria::ASC) Order by the leftArmTatooId column
 * @method     ChildPlayerAppearanceQuery orderByRightArmTatooId($order = Criteria::ASC) Order by the rightArmTatooId column
 * @method     ChildPlayerAppearanceQuery orderByskinColorId($order = Criteria::ASC) Order by the skinColorId column
 *
 * @method     ChildPlayerAppearanceQuery groupById() Group by the id column
 * @method     ChildPlayerAppearanceQuery groupByPlayer() Group by the player column
 * @method     ChildPlayerAppearanceQuery groupByMeshId() Group by the meshId column
 * @method     ChildPlayerAppearanceQuery groupByHairStyleId() Group by the hairStyleId column
 * @method     ChildPlayerAppearanceQuery groupByBeardStyleId() Group by the beardStyleId column
 * @method     ChildPlayerAppearanceQuery groupByHairColorId() Group by the hairColorId column
 * @method     ChildPlayerAppearanceQuery groupByBeardColorId() Group by the beardColorId column
 * @method     ChildPlayerAppearanceQuery groupByNeckTatooId() Group by the neckTatooId column
 * @method     ChildPlayerAppearanceQuery groupByLeftArmTatooId() Group by the leftArmTatooId column
 * @method     ChildPlayerAppearanceQuery groupByRightArmTatooId() Group by the rightArmTatooId column
 * @method     ChildPlayerAppearanceQuery groupByskinColorId() Group by the skinColorId column
 *
 * @method     ChildPlayerAppearanceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlayerAppearanceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlayerAppearanceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlayerAppearanceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlayerAppearanceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlayerAppearanceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlayerAppearance findOne(ConnectionInterface $con = null) Return the first ChildPlayerAppearance matching the query
 * @method     ChildPlayerAppearance findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlayerAppearance matching the query, or a new ChildPlayerAppearance object populated from the query conditions when no match is found
 *
 * @method     ChildPlayerAppearance findOneById(int $id) Return the first ChildPlayerAppearance filtered by the id column
 * @method     ChildPlayerAppearance findOneByPlayer(int $player) Return the first ChildPlayerAppearance filtered by the player column
 * @method     ChildPlayerAppearance findOneByMeshId(int $meshId) Return the first ChildPlayerAppearance filtered by the meshId column
 * @method     ChildPlayerAppearance findOneByHairStyleId(int $hairStyleId) Return the first ChildPlayerAppearance filtered by the hairStyleId column
 * @method     ChildPlayerAppearance findOneByBeardStyleId(int $beardStyleId) Return the first ChildPlayerAppearance filtered by the beardStyleId column
 * @method     ChildPlayerAppearance findOneByHairColorId(int $hairColorId) Return the first ChildPlayerAppearance filtered by the hairColorId column
 * @method     ChildPlayerAppearance findOneByBeardColorId(int $beardColorId) Return the first ChildPlayerAppearance filtered by the beardColorId column
 * @method     ChildPlayerAppearance findOneByNeckTatooId(int $neckTatooId) Return the first ChildPlayerAppearance filtered by the neckTatooId column
 * @method     ChildPlayerAppearance findOneByLeftArmTatooId(int $leftArmTatooId) Return the first ChildPlayerAppearance filtered by the leftArmTatooId column
 * @method     ChildPlayerAppearance findOneByRightArmTatooId(int $rightArmTatooId) Return the first ChildPlayerAppearance filtered by the rightArmTatooId column
 * @method     ChildPlayerAppearance findOneByskinColorId(int $skinColorId) Return the first ChildPlayerAppearance filtered by the skinColorId column *

 * @method     ChildPlayerAppearance requirePk($key, ConnectionInterface $con = null) Return the ChildPlayerAppearance by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOne(ConnectionInterface $con = null) Return the first ChildPlayerAppearance matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerAppearance requireOneById(int $id) Return the first ChildPlayerAppearance filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByPlayer(int $player) Return the first ChildPlayerAppearance filtered by the player column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByMeshId(int $meshId) Return the first ChildPlayerAppearance filtered by the meshId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByHairStyleId(int $hairStyleId) Return the first ChildPlayerAppearance filtered by the hairStyleId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByBeardStyleId(int $beardStyleId) Return the first ChildPlayerAppearance filtered by the beardStyleId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByHairColorId(int $hairColorId) Return the first ChildPlayerAppearance filtered by the hairColorId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByBeardColorId(int $beardColorId) Return the first ChildPlayerAppearance filtered by the beardColorId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByNeckTatooId(int $neckTatooId) Return the first ChildPlayerAppearance filtered by the neckTatooId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByLeftArmTatooId(int $leftArmTatooId) Return the first ChildPlayerAppearance filtered by the leftArmTatooId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByRightArmTatooId(int $rightArmTatooId) Return the first ChildPlayerAppearance filtered by the rightArmTatooId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerAppearance requireOneByskinColorId(int $skinColorId) Return the first ChildPlayerAppearance filtered by the skinColorId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerAppearance[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlayerAppearance objects based on current ModelCriteria
 * @method     ChildPlayerAppearance[]|ObjectCollection findById(int $id) Return ChildPlayerAppearance objects filtered by the id column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByPlayer(int $player) Return ChildPlayerAppearance objects filtered by the player column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByMeshId(int $meshId) Return ChildPlayerAppearance objects filtered by the meshId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByHairStyleId(int $hairStyleId) Return ChildPlayerAppearance objects filtered by the hairStyleId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByBeardStyleId(int $beardStyleId) Return ChildPlayerAppearance objects filtered by the beardStyleId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByHairColorId(int $hairColorId) Return ChildPlayerAppearance objects filtered by the hairColorId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByBeardColorId(int $beardColorId) Return ChildPlayerAppearance objects filtered by the beardColorId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByNeckTatooId(int $neckTatooId) Return ChildPlayerAppearance objects filtered by the neckTatooId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByLeftArmTatooId(int $leftArmTatooId) Return ChildPlayerAppearance objects filtered by the leftArmTatooId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByRightArmTatooId(int $rightArmTatooId) Return ChildPlayerAppearance objects filtered by the rightArmTatooId column
 * @method     ChildPlayerAppearance[]|ObjectCollection findByskinColorId(int $skinColorId) Return ChildPlayerAppearance objects filtered by the skinColorId column
 * @method     ChildPlayerAppearance[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlayerAppearanceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PlayerAppearanceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PlayerAppearance', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlayerAppearanceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlayerAppearanceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlayerAppearanceQuery) {
            return $criteria;
        }
        $query = new ChildPlayerAppearanceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlayerAppearance|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlayerAppearanceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlayerAppearanceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlayerAppearance A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, player, meshId, hairStyleId, beardStyleId, hairColorId, beardColorId, neckTatooId, leftArmTatooId, rightArmTatooId, skinColorId FROM players_appearance WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlayerAppearance $obj */
            $obj = new ChildPlayerAppearance();
            $obj->hydrate($row);
            PlayerAppearanceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlayerAppearance|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the player column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayer(1234); // WHERE player = 1234
     * $query->filterByPlayer(array(12, 34)); // WHERE player IN (12, 34)
     * $query->filterByPlayer(array('min' => 12)); // WHERE player > 12
     * </code>
     *
     * @param     mixed $player The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByPlayer($player = null, $comparison = null)
    {
        if (is_array($player)) {
            $useMinMax = false;
            if (isset($player['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_PLAYER, $player['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($player['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_PLAYER, $player['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_PLAYER, $player, $comparison);
    }

    /**
     * Filter the query on the meshId column
     *
     * Example usage:
     * <code>
     * $query->filterByMeshId(1234); // WHERE meshId = 1234
     * $query->filterByMeshId(array(12, 34)); // WHERE meshId IN (12, 34)
     * $query->filterByMeshId(array('min' => 12)); // WHERE meshId > 12
     * </code>
     *
     * @param     mixed $meshId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByMeshId($meshId = null, $comparison = null)
    {
        if (is_array($meshId)) {
            $useMinMax = false;
            if (isset($meshId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_MESHID, $meshId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($meshId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_MESHID, $meshId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_MESHID, $meshId, $comparison);
    }

    /**
     * Filter the query on the hairStyleId column
     *
     * Example usage:
     * <code>
     * $query->filterByHairStyleId(1234); // WHERE hairStyleId = 1234
     * $query->filterByHairStyleId(array(12, 34)); // WHERE hairStyleId IN (12, 34)
     * $query->filterByHairStyleId(array('min' => 12)); // WHERE hairStyleId > 12
     * </code>
     *
     * @param     mixed $hairStyleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByHairStyleId($hairStyleId = null, $comparison = null)
    {
        if (is_array($hairStyleId)) {
            $useMinMax = false;
            if (isset($hairStyleId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRSTYLEID, $hairStyleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hairStyleId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRSTYLEID, $hairStyleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRSTYLEID, $hairStyleId, $comparison);
    }

    /**
     * Filter the query on the beardStyleId column
     *
     * Example usage:
     * <code>
     * $query->filterByBeardStyleId(1234); // WHERE beardStyleId = 1234
     * $query->filterByBeardStyleId(array(12, 34)); // WHERE beardStyleId IN (12, 34)
     * $query->filterByBeardStyleId(array('min' => 12)); // WHERE beardStyleId > 12
     * </code>
     *
     * @param     mixed $beardStyleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByBeardStyleId($beardStyleId = null, $comparison = null)
    {
        if (is_array($beardStyleId)) {
            $useMinMax = false;
            if (isset($beardStyleId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDSTYLEID, $beardStyleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($beardStyleId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDSTYLEID, $beardStyleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDSTYLEID, $beardStyleId, $comparison);
    }

    /**
     * Filter the query on the hairColorId column
     *
     * Example usage:
     * <code>
     * $query->filterByHairColorId(1234); // WHERE hairColorId = 1234
     * $query->filterByHairColorId(array(12, 34)); // WHERE hairColorId IN (12, 34)
     * $query->filterByHairColorId(array('min' => 12)); // WHERE hairColorId > 12
     * </code>
     *
     * @param     mixed $hairColorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByHairColorId($hairColorId = null, $comparison = null)
    {
        if (is_array($hairColorId)) {
            $useMinMax = false;
            if (isset($hairColorId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRCOLORID, $hairColorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hairColorId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRCOLORID, $hairColorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_HAIRCOLORID, $hairColorId, $comparison);
    }

    /**
     * Filter the query on the beardColorId column
     *
     * Example usage:
     * <code>
     * $query->filterByBeardColorId(1234); // WHERE beardColorId = 1234
     * $query->filterByBeardColorId(array(12, 34)); // WHERE beardColorId IN (12, 34)
     * $query->filterByBeardColorId(array('min' => 12)); // WHERE beardColorId > 12
     * </code>
     *
     * @param     mixed $beardColorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByBeardColorId($beardColorId = null, $comparison = null)
    {
        if (is_array($beardColorId)) {
            $useMinMax = false;
            if (isset($beardColorId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDCOLORID, $beardColorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($beardColorId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDCOLORID, $beardColorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_BEARDCOLORID, $beardColorId, $comparison);
    }

    /**
     * Filter the query on the neckTatooId column
     *
     * Example usage:
     * <code>
     * $query->filterByNeckTatooId(1234); // WHERE neckTatooId = 1234
     * $query->filterByNeckTatooId(array(12, 34)); // WHERE neckTatooId IN (12, 34)
     * $query->filterByNeckTatooId(array('min' => 12)); // WHERE neckTatooId > 12
     * </code>
     *
     * @param     mixed $neckTatooId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByNeckTatooId($neckTatooId = null, $comparison = null)
    {
        if (is_array($neckTatooId)) {
            $useMinMax = false;
            if (isset($neckTatooId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_NECKTATOOID, $neckTatooId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($neckTatooId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_NECKTATOOID, $neckTatooId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_NECKTATOOID, $neckTatooId, $comparison);
    }

    /**
     * Filter the query on the leftArmTatooId column
     *
     * Example usage:
     * <code>
     * $query->filterByLeftArmTatooId(1234); // WHERE leftArmTatooId = 1234
     * $query->filterByLeftArmTatooId(array(12, 34)); // WHERE leftArmTatooId IN (12, 34)
     * $query->filterByLeftArmTatooId(array('min' => 12)); // WHERE leftArmTatooId > 12
     * </code>
     *
     * @param     mixed $leftArmTatooId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByLeftArmTatooId($leftArmTatooId = null, $comparison = null)
    {
        if (is_array($leftArmTatooId)) {
            $useMinMax = false;
            if (isset($leftArmTatooId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_LEFTARMTATOOID, $leftArmTatooId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leftArmTatooId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_LEFTARMTATOOID, $leftArmTatooId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_LEFTARMTATOOID, $leftArmTatooId, $comparison);
    }

    /**
     * Filter the query on the rightArmTatooId column
     *
     * Example usage:
     * <code>
     * $query->filterByRightArmTatooId(1234); // WHERE rightArmTatooId = 1234
     * $query->filterByRightArmTatooId(array(12, 34)); // WHERE rightArmTatooId IN (12, 34)
     * $query->filterByRightArmTatooId(array('min' => 12)); // WHERE rightArmTatooId > 12
     * </code>
     *
     * @param     mixed $rightArmTatooId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByRightArmTatooId($rightArmTatooId = null, $comparison = null)
    {
        if (is_array($rightArmTatooId)) {
            $useMinMax = false;
            if (isset($rightArmTatooId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_RIGHTARMTATOOID, $rightArmTatooId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rightArmTatooId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_RIGHTARMTATOOID, $rightArmTatooId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_RIGHTARMTATOOID, $rightArmTatooId, $comparison);
    }

    /**
     * Filter the query on the skinColorId column
     *
     * Example usage:
     * <code>
     * $query->filterByskinColorId(1234); // WHERE skinColorId = 1234
     * $query->filterByskinColorId(array(12, 34)); // WHERE skinColorId IN (12, 34)
     * $query->filterByskinColorId(array('min' => 12)); // WHERE skinColorId > 12
     * </code>
     *
     * @param     mixed $skinColorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function filterByskinColorId($skinColorId = null, $comparison = null)
    {
        if (is_array($skinColorId)) {
            $useMinMax = false;
            if (isset($skinColorId['min'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_SKINCOLORID, $skinColorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($skinColorId['max'])) {
                $this->addUsingAlias(PlayerAppearanceTableMap::COL_SKINCOLORID, $skinColorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerAppearanceTableMap::COL_SKINCOLORID, $skinColorId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlayerAppearance $playerAppearance Object to remove from the list of results
     *
     * @return $this|ChildPlayerAppearanceQuery The current query, for fluid interface
     */
    public function prune($playerAppearance = null)
    {
        if ($playerAppearance) {
            $this->addUsingAlias(PlayerAppearanceTableMap::COL_ID, $playerAppearance->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the players_appearance table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerAppearanceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlayerAppearanceTableMap::clearInstancePool();
            PlayerAppearanceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerAppearanceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlayerAppearanceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlayerAppearanceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlayerAppearanceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlayerAppearanceQuery
