<?php

namespace Base;

use \PlayerFaceAppearance as ChildPlayerFaceAppearance;
use \PlayerFaceAppearanceQuery as ChildPlayerFaceAppearanceQuery;
use \Exception;
use \PDO;
use Map\PlayerFaceAppearanceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'players_face_appearance' table.
 *
 *
 *
 * @method     ChildPlayerFaceAppearanceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPlayerFaceAppearanceQuery orderByPlayer($order = Criteria::ASC) Order by the player column
 * @method     ChildPlayerFaceAppearanceQuery orderByChin($order = Criteria::ASC) Order by the chin column
 * @method     ChildPlayerFaceAppearanceQuery orderByCheeks($order = Criteria::ASC) Order by the cheeks column
 * @method     ChildPlayerFaceAppearanceQuery orderByJaw($order = Criteria::ASC) Order by the jaw column
 * @method     ChildPlayerFaceAppearanceQuery orderByLips($order = Criteria::ASC) Order by the lips column
 * @method     ChildPlayerFaceAppearanceQuery orderByNoseHeight($order = Criteria::ASC) Order by the noseHeight column
 * @method     ChildPlayerFaceAppearanceQuery orderByNoseWidth($order = Criteria::ASC) Order by the noseWidth column
 * @method     ChildPlayerFaceAppearanceQuery orderByEarSize($order = Criteria::ASC) Order by the earSize column
 *
 * @method     ChildPlayerFaceAppearanceQuery groupById() Group by the id column
 * @method     ChildPlayerFaceAppearanceQuery groupByPlayer() Group by the player column
 * @method     ChildPlayerFaceAppearanceQuery groupByChin() Group by the chin column
 * @method     ChildPlayerFaceAppearanceQuery groupByCheeks() Group by the cheeks column
 * @method     ChildPlayerFaceAppearanceQuery groupByJaw() Group by the jaw column
 * @method     ChildPlayerFaceAppearanceQuery groupByLips() Group by the lips column
 * @method     ChildPlayerFaceAppearanceQuery groupByNoseHeight() Group by the noseHeight column
 * @method     ChildPlayerFaceAppearanceQuery groupByNoseWidth() Group by the noseWidth column
 * @method     ChildPlayerFaceAppearanceQuery groupByEarSize() Group by the earSize column
 *
 * @method     ChildPlayerFaceAppearanceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlayerFaceAppearanceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlayerFaceAppearanceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlayerFaceAppearanceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlayerFaceAppearanceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlayerFaceAppearanceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlayerFaceAppearance findOne(ConnectionInterface $con = null) Return the first ChildPlayerFaceAppearance matching the query
 * @method     ChildPlayerFaceAppearance findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlayerFaceAppearance matching the query, or a new ChildPlayerFaceAppearance object populated from the query conditions when no match is found
 *
 * @method     ChildPlayerFaceAppearance findOneById(int $id) Return the first ChildPlayerFaceAppearance filtered by the id column
 * @method     ChildPlayerFaceAppearance findOneByPlayer(int $player) Return the first ChildPlayerFaceAppearance filtered by the player column
 * @method     ChildPlayerFaceAppearance findOneByChin(int $chin) Return the first ChildPlayerFaceAppearance filtered by the chin column
 * @method     ChildPlayerFaceAppearance findOneByCheeks(int $cheeks) Return the first ChildPlayerFaceAppearance filtered by the cheeks column
 * @method     ChildPlayerFaceAppearance findOneByJaw(int $jaw) Return the first ChildPlayerFaceAppearance filtered by the jaw column
 * @method     ChildPlayerFaceAppearance findOneByLips(int $lips) Return the first ChildPlayerFaceAppearance filtered by the lips column
 * @method     ChildPlayerFaceAppearance findOneByNoseHeight(int $noseHeight) Return the first ChildPlayerFaceAppearance filtered by the noseHeight column
 * @method     ChildPlayerFaceAppearance findOneByNoseWidth(int $noseWidth) Return the first ChildPlayerFaceAppearance filtered by the noseWidth column
 * @method     ChildPlayerFaceAppearance findOneByEarSize(int $earSize) Return the first ChildPlayerFaceAppearance filtered by the earSize column *

 * @method     ChildPlayerFaceAppearance requirePk($key, ConnectionInterface $con = null) Return the ChildPlayerFaceAppearance by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOne(ConnectionInterface $con = null) Return the first ChildPlayerFaceAppearance matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerFaceAppearance requireOneById(int $id) Return the first ChildPlayerFaceAppearance filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByPlayer(int $player) Return the first ChildPlayerFaceAppearance filtered by the player column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByChin(int $chin) Return the first ChildPlayerFaceAppearance filtered by the chin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByCheeks(int $cheeks) Return the first ChildPlayerFaceAppearance filtered by the cheeks column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByJaw(int $jaw) Return the first ChildPlayerFaceAppearance filtered by the jaw column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByLips(int $lips) Return the first ChildPlayerFaceAppearance filtered by the lips column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByNoseHeight(int $noseHeight) Return the first ChildPlayerFaceAppearance filtered by the noseHeight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByNoseWidth(int $noseWidth) Return the first ChildPlayerFaceAppearance filtered by the noseWidth column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerFaceAppearance requireOneByEarSize(int $earSize) Return the first ChildPlayerFaceAppearance filtered by the earSize column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlayerFaceAppearance objects based on current ModelCriteria
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findById(int $id) Return ChildPlayerFaceAppearance objects filtered by the id column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByPlayer(int $player) Return ChildPlayerFaceAppearance objects filtered by the player column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByChin(int $chin) Return ChildPlayerFaceAppearance objects filtered by the chin column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByCheeks(int $cheeks) Return ChildPlayerFaceAppearance objects filtered by the cheeks column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByJaw(int $jaw) Return ChildPlayerFaceAppearance objects filtered by the jaw column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByLips(int $lips) Return ChildPlayerFaceAppearance objects filtered by the lips column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByNoseHeight(int $noseHeight) Return ChildPlayerFaceAppearance objects filtered by the noseHeight column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByNoseWidth(int $noseWidth) Return ChildPlayerFaceAppearance objects filtered by the noseWidth column
 * @method     ChildPlayerFaceAppearance[]|ObjectCollection findByEarSize(int $earSize) Return ChildPlayerFaceAppearance objects filtered by the earSize column
 * @method     ChildPlayerFaceAppearance[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlayerFaceAppearanceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PlayerFaceAppearanceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PlayerFaceAppearance', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlayerFaceAppearanceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlayerFaceAppearanceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlayerFaceAppearanceQuery) {
            return $criteria;
        }
        $query = new ChildPlayerFaceAppearanceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlayerFaceAppearance|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlayerFaceAppearanceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlayerFaceAppearanceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlayerFaceAppearance A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, player, chin, cheeks, jaw, lips, noseHeight, noseWidth, earSize FROM players_face_appearance WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlayerFaceAppearance $obj */
            $obj = new ChildPlayerFaceAppearance();
            $obj->hydrate($row);
            PlayerFaceAppearanceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlayerFaceAppearance|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the player column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayer(1234); // WHERE player = 1234
     * $query->filterByPlayer(array(12, 34)); // WHERE player IN (12, 34)
     * $query->filterByPlayer(array('min' => 12)); // WHERE player > 12
     * </code>
     *
     * @param     mixed $player The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByPlayer($player = null, $comparison = null)
    {
        if (is_array($player)) {
            $useMinMax = false;
            if (isset($player['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_PLAYER, $player['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($player['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_PLAYER, $player['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_PLAYER, $player, $comparison);
    }

    /**
     * Filter the query on the chin column
     *
     * Example usage:
     * <code>
     * $query->filterByChin(1234); // WHERE chin = 1234
     * $query->filterByChin(array(12, 34)); // WHERE chin IN (12, 34)
     * $query->filterByChin(array('min' => 12)); // WHERE chin > 12
     * </code>
     *
     * @param     mixed $chin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByChin($chin = null, $comparison = null)
    {
        if (is_array($chin)) {
            $useMinMax = false;
            if (isset($chin['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHIN, $chin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chin['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHIN, $chin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHIN, $chin, $comparison);
    }

    /**
     * Filter the query on the cheeks column
     *
     * Example usage:
     * <code>
     * $query->filterByCheeks(1234); // WHERE cheeks = 1234
     * $query->filterByCheeks(array(12, 34)); // WHERE cheeks IN (12, 34)
     * $query->filterByCheeks(array('min' => 12)); // WHERE cheeks > 12
     * </code>
     *
     * @param     mixed $cheeks The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByCheeks($cheeks = null, $comparison = null)
    {
        if (is_array($cheeks)) {
            $useMinMax = false;
            if (isset($cheeks['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHEEKS, $cheeks['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cheeks['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHEEKS, $cheeks['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_CHEEKS, $cheeks, $comparison);
    }

    /**
     * Filter the query on the jaw column
     *
     * Example usage:
     * <code>
     * $query->filterByJaw(1234); // WHERE jaw = 1234
     * $query->filterByJaw(array(12, 34)); // WHERE jaw IN (12, 34)
     * $query->filterByJaw(array('min' => 12)); // WHERE jaw > 12
     * </code>
     *
     * @param     mixed $jaw The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByJaw($jaw = null, $comparison = null)
    {
        if (is_array($jaw)) {
            $useMinMax = false;
            if (isset($jaw['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_JAW, $jaw['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jaw['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_JAW, $jaw['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_JAW, $jaw, $comparison);
    }

    /**
     * Filter the query on the lips column
     *
     * Example usage:
     * <code>
     * $query->filterByLips(1234); // WHERE lips = 1234
     * $query->filterByLips(array(12, 34)); // WHERE lips IN (12, 34)
     * $query->filterByLips(array('min' => 12)); // WHERE lips > 12
     * </code>
     *
     * @param     mixed $lips The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByLips($lips = null, $comparison = null)
    {
        if (is_array($lips)) {
            $useMinMax = false;
            if (isset($lips['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_LIPS, $lips['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lips['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_LIPS, $lips['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_LIPS, $lips, $comparison);
    }

    /**
     * Filter the query on the noseHeight column
     *
     * Example usage:
     * <code>
     * $query->filterByNoseHeight(1234); // WHERE noseHeight = 1234
     * $query->filterByNoseHeight(array(12, 34)); // WHERE noseHeight IN (12, 34)
     * $query->filterByNoseHeight(array('min' => 12)); // WHERE noseHeight > 12
     * </code>
     *
     * @param     mixed $noseHeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByNoseHeight($noseHeight = null, $comparison = null)
    {
        if (is_array($noseHeight)) {
            $useMinMax = false;
            if (isset($noseHeight['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEHEIGHT, $noseHeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($noseHeight['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEHEIGHT, $noseHeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEHEIGHT, $noseHeight, $comparison);
    }

    /**
     * Filter the query on the noseWidth column
     *
     * Example usage:
     * <code>
     * $query->filterByNoseWidth(1234); // WHERE noseWidth = 1234
     * $query->filterByNoseWidth(array(12, 34)); // WHERE noseWidth IN (12, 34)
     * $query->filterByNoseWidth(array('min' => 12)); // WHERE noseWidth > 12
     * </code>
     *
     * @param     mixed $noseWidth The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByNoseWidth($noseWidth = null, $comparison = null)
    {
        if (is_array($noseWidth)) {
            $useMinMax = false;
            if (isset($noseWidth['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEWIDTH, $noseWidth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($noseWidth['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEWIDTH, $noseWidth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_NOSEWIDTH, $noseWidth, $comparison);
    }

    /**
     * Filter the query on the earSize column
     *
     * Example usage:
     * <code>
     * $query->filterByEarSize(1234); // WHERE earSize = 1234
     * $query->filterByEarSize(array(12, 34)); // WHERE earSize IN (12, 34)
     * $query->filterByEarSize(array('min' => 12)); // WHERE earSize > 12
     * </code>
     *
     * @param     mixed $earSize The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function filterByEarSize($earSize = null, $comparison = null)
    {
        if (is_array($earSize)) {
            $useMinMax = false;
            if (isset($earSize['min'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_EARSIZE, $earSize['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($earSize['max'])) {
                $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_EARSIZE, $earSize['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_EARSIZE, $earSize, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlayerFaceAppearance $playerFaceAppearance Object to remove from the list of results
     *
     * @return $this|ChildPlayerFaceAppearanceQuery The current query, for fluid interface
     */
    public function prune($playerFaceAppearance = null)
    {
        if ($playerFaceAppearance) {
            $this->addUsingAlias(PlayerFaceAppearanceTableMap::COL_ID, $playerFaceAppearance->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the players_face_appearance table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerFaceAppearanceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlayerFaceAppearanceTableMap::clearInstancePool();
            PlayerFaceAppearanceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerFaceAppearanceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlayerFaceAppearanceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlayerFaceAppearanceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlayerFaceAppearanceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlayerFaceAppearanceQuery
