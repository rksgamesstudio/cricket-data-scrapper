<?php

namespace Base;

use \Teams as ChildTeams;
use \TeamsQuery as ChildTeamsQuery;
use \Exception;
use \PDO;
use Map\TeamsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'teams' table.
 *
 *
 *
 * @method     ChildTeamsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTeamsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildTeamsQuery orderByShortName($order = Criteria::ASC) Order by the short_name column
 * @method     ChildTeamsQuery orderByWikiUrl($order = Criteria::ASC) Order by the wiki_url column
 * @method     ChildTeamsQuery orderByTableSelector($order = Criteria::ASC) Order by the table_selector column
 * @method     ChildTeamsQuery orderByPlayerName($order = Criteria::ASC) Order by the player_name column
 * @method     ChildTeamsQuery orderByPlayerBattingHand($order = Criteria::ASC) Order by the player_batting_hand column
 * @method     ChildTeamsQuery orderByPlayerBowlingHand($order = Criteria::ASC) Order by the player_bowling_hand column
 * @method     ChildTeamsQuery orderByPlayerDob($order = Criteria::ASC) Order by the player_dob column
 * @method     ChildTeamsQuery orderByLogo($order = Criteria::ASC) Order by the logo column
 * @method     ChildTeamsQuery orderByCountry($order = Criteria::ASC) Order by the country column
 * @method     ChildTeamsQuery orderByBatting($order = Criteria::ASC) Order by the batting column
 * @method     ChildTeamsQuery orderByBowling($order = Criteria::ASC) Order by the bowling column
 * @method     ChildTeamsQuery orderByFielding($order = Criteria::ASC) Order by the fielding column
 * @method     ChildTeamsQuery orderByLeague($order = Criteria::ASC) Order by the league column
 * @method     ChildTeamsQuery orderByIsInternational($order = Criteria::ASC) Order by the is_international column
 * @method     ChildTeamsQuery orderByTeamType($order = Criteria::ASC) Order by the team_type column
 * @method     ChildTeamsQuery orderByStadiumID($order = Criteria::ASC) Order by the stadium_id column
 *
 * @method     ChildTeamsQuery groupById() Group by the id column
 * @method     ChildTeamsQuery groupByName() Group by the name column
 * @method     ChildTeamsQuery groupByShortName() Group by the short_name column
 * @method     ChildTeamsQuery groupByWikiUrl() Group by the wiki_url column
 * @method     ChildTeamsQuery groupByTableSelector() Group by the table_selector column
 * @method     ChildTeamsQuery groupByPlayerName() Group by the player_name column
 * @method     ChildTeamsQuery groupByPlayerBattingHand() Group by the player_batting_hand column
 * @method     ChildTeamsQuery groupByPlayerBowlingHand() Group by the player_bowling_hand column
 * @method     ChildTeamsQuery groupByPlayerDob() Group by the player_dob column
 * @method     ChildTeamsQuery groupByLogo() Group by the logo column
 * @method     ChildTeamsQuery groupByCountry() Group by the country column
 * @method     ChildTeamsQuery groupByBatting() Group by the batting column
 * @method     ChildTeamsQuery groupByBowling() Group by the bowling column
 * @method     ChildTeamsQuery groupByFielding() Group by the fielding column
 * @method     ChildTeamsQuery groupByLeague() Group by the league column
 * @method     ChildTeamsQuery groupByIsInternational() Group by the is_international column
 * @method     ChildTeamsQuery groupByTeamType() Group by the team_type column
 * @method     ChildTeamsQuery groupByStadiumID() Group by the stadium_id column
 *
 * @method     ChildTeamsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTeamsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTeamsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTeamsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTeamsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTeamsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTeams findOne(ConnectionInterface $con = null) Return the first ChildTeams matching the query
 * @method     ChildTeams findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTeams matching the query, or a new ChildTeams object populated from the query conditions when no match is found
 *
 * @method     ChildTeams findOneById(int $id) Return the first ChildTeams filtered by the id column
 * @method     ChildTeams findOneByName(string $name) Return the first ChildTeams filtered by the name column
 * @method     ChildTeams findOneByShortName(string $short_name) Return the first ChildTeams filtered by the short_name column
 * @method     ChildTeams findOneByWikiUrl(string $wiki_url) Return the first ChildTeams filtered by the wiki_url column
 * @method     ChildTeams findOneByTableSelector(string $table_selector) Return the first ChildTeams filtered by the table_selector column
 * @method     ChildTeams findOneByPlayerName(string $player_name) Return the first ChildTeams filtered by the player_name column
 * @method     ChildTeams findOneByPlayerBattingHand(string $player_batting_hand) Return the first ChildTeams filtered by the player_batting_hand column
 * @method     ChildTeams findOneByPlayerBowlingHand(string $player_bowling_hand) Return the first ChildTeams filtered by the player_bowling_hand column
 * @method     ChildTeams findOneByPlayerDob(string $player_dob) Return the first ChildTeams filtered by the player_dob column
 * @method     ChildTeams findOneByLogo(string $logo) Return the first ChildTeams filtered by the logo column
 * @method     ChildTeams findOneByCountry(string $country) Return the first ChildTeams filtered by the country column
 * @method     ChildTeams findOneByBatting(int $batting) Return the first ChildTeams filtered by the batting column
 * @method     ChildTeams findOneByBowling(int $bowling) Return the first ChildTeams filtered by the bowling column
 * @method     ChildTeams findOneByFielding(int $fielding) Return the first ChildTeams filtered by the fielding column
 * @method     ChildTeams findOneByLeague(int $league) Return the first ChildTeams filtered by the league column
 * @method     ChildTeams findOneByIsInternational(int $is_international) Return the first ChildTeams filtered by the is_international column
 * @method     ChildTeams findOneByTeamType(string $team_type) Return the first ChildTeams filtered by the team_type column
 * @method     ChildTeams findOneByStadiumID(int $stadium_id) Return the first ChildTeams filtered by the stadium_id column *

 * @method     ChildTeams requirePk($key, ConnectionInterface $con = null) Return the ChildTeams by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOne(ConnectionInterface $con = null) Return the first ChildTeams matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeams requireOneById(int $id) Return the first ChildTeams filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByName(string $name) Return the first ChildTeams filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByShortName(string $short_name) Return the first ChildTeams filtered by the short_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByWikiUrl(string $wiki_url) Return the first ChildTeams filtered by the wiki_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByTableSelector(string $table_selector) Return the first ChildTeams filtered by the table_selector column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByPlayerName(string $player_name) Return the first ChildTeams filtered by the player_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByPlayerBattingHand(string $player_batting_hand) Return the first ChildTeams filtered by the player_batting_hand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByPlayerBowlingHand(string $player_bowling_hand) Return the first ChildTeams filtered by the player_bowling_hand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByPlayerDob(string $player_dob) Return the first ChildTeams filtered by the player_dob column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByLogo(string $logo) Return the first ChildTeams filtered by the logo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByCountry(string $country) Return the first ChildTeams filtered by the country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByBatting(int $batting) Return the first ChildTeams filtered by the batting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByBowling(int $bowling) Return the first ChildTeams filtered by the bowling column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByFielding(int $fielding) Return the first ChildTeams filtered by the fielding column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByLeague(int $league) Return the first ChildTeams filtered by the league column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByIsInternational(int $is_international) Return the first ChildTeams filtered by the is_international column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByTeamType(string $team_type) Return the first ChildTeams filtered by the team_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTeams requireOneByStadiumID(int $stadium_id) Return the first ChildTeams filtered by the stadium_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTeams[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTeams objects based on current ModelCriteria
 * @method     ChildTeams[]|ObjectCollection findById(int $id) Return ChildTeams objects filtered by the id column
 * @method     ChildTeams[]|ObjectCollection findByName(string $name) Return ChildTeams objects filtered by the name column
 * @method     ChildTeams[]|ObjectCollection findByShortName(string $short_name) Return ChildTeams objects filtered by the short_name column
 * @method     ChildTeams[]|ObjectCollection findByWikiUrl(string $wiki_url) Return ChildTeams objects filtered by the wiki_url column
 * @method     ChildTeams[]|ObjectCollection findByTableSelector(string $table_selector) Return ChildTeams objects filtered by the table_selector column
 * @method     ChildTeams[]|ObjectCollection findByPlayerName(string $player_name) Return ChildTeams objects filtered by the player_name column
 * @method     ChildTeams[]|ObjectCollection findByPlayerBattingHand(string $player_batting_hand) Return ChildTeams objects filtered by the player_batting_hand column
 * @method     ChildTeams[]|ObjectCollection findByPlayerBowlingHand(string $player_bowling_hand) Return ChildTeams objects filtered by the player_bowling_hand column
 * @method     ChildTeams[]|ObjectCollection findByPlayerDob(string $player_dob) Return ChildTeams objects filtered by the player_dob column
 * @method     ChildTeams[]|ObjectCollection findByLogo(string $logo) Return ChildTeams objects filtered by the logo column
 * @method     ChildTeams[]|ObjectCollection findByCountry(string $country) Return ChildTeams objects filtered by the country column
 * @method     ChildTeams[]|ObjectCollection findByBatting(int $batting) Return ChildTeams objects filtered by the batting column
 * @method     ChildTeams[]|ObjectCollection findByBowling(int $bowling) Return ChildTeams objects filtered by the bowling column
 * @method     ChildTeams[]|ObjectCollection findByFielding(int $fielding) Return ChildTeams objects filtered by the fielding column
 * @method     ChildTeams[]|ObjectCollection findByLeague(int $league) Return ChildTeams objects filtered by the league column
 * @method     ChildTeams[]|ObjectCollection findByIsInternational(int $is_international) Return ChildTeams objects filtered by the is_international column
 * @method     ChildTeams[]|ObjectCollection findByTeamType(string $team_type) Return ChildTeams objects filtered by the team_type column
 * @method     ChildTeams[]|ObjectCollection findByStadiumID(int $stadium_id) Return ChildTeams objects filtered by the stadium_id column
 * @method     ChildTeams[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TeamsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TeamsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Teams', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTeamsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTeamsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTeamsQuery) {
            return $criteria;
        }
        $query = new ChildTeamsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTeams|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TeamsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TeamsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTeams A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, short_name, wiki_url, table_selector, player_name, player_batting_hand, player_bowling_hand, player_dob, logo, country, batting, bowling, fielding, league, is_international, team_type, stadium_id FROM teams WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTeams $obj */
            $obj = new ChildTeams();
            $obj->hydrate($row);
            TeamsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTeams|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TeamsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TeamsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the short_name column
     *
     * Example usage:
     * <code>
     * $query->filterByShortName('fooValue');   // WHERE short_name = 'fooValue'
     * $query->filterByShortName('%fooValue%', Criteria::LIKE); // WHERE short_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shortName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByShortName($shortName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shortName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_SHORT_NAME, $shortName, $comparison);
    }

    /**
     * Filter the query on the wiki_url column
     *
     * Example usage:
     * <code>
     * $query->filterByWikiUrl('fooValue');   // WHERE wiki_url = 'fooValue'
     * $query->filterByWikiUrl('%fooValue%', Criteria::LIKE); // WHERE wiki_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wikiUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByWikiUrl($wikiUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wikiUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_WIKI_URL, $wikiUrl, $comparison);
    }

    /**
     * Filter the query on the table_selector column
     *
     * Example usage:
     * <code>
     * $query->filterByTableSelector('fooValue');   // WHERE table_selector = 'fooValue'
     * $query->filterByTableSelector('%fooValue%', Criteria::LIKE); // WHERE table_selector LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tableSelector The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByTableSelector($tableSelector = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tableSelector)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_TABLE_SELECTOR, $tableSelector, $comparison);
    }

    /**
     * Filter the query on the player_name column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerName('fooValue');   // WHERE player_name = 'fooValue'
     * $query->filterByPlayerName('%fooValue%', Criteria::LIKE); // WHERE player_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playerName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPlayerName($playerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playerName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_PLAYER_NAME, $playerName, $comparison);
    }

    /**
     * Filter the query on the player_batting_hand column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerBattingHand('fooValue');   // WHERE player_batting_hand = 'fooValue'
     * $query->filterByPlayerBattingHand('%fooValue%', Criteria::LIKE); // WHERE player_batting_hand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playerBattingHand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPlayerBattingHand($playerBattingHand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playerBattingHand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_PLAYER_BATTING_HAND, $playerBattingHand, $comparison);
    }

    /**
     * Filter the query on the player_bowling_hand column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerBowlingHand('fooValue');   // WHERE player_bowling_hand = 'fooValue'
     * $query->filterByPlayerBowlingHand('%fooValue%', Criteria::LIKE); // WHERE player_bowling_hand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playerBowlingHand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPlayerBowlingHand($playerBowlingHand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playerBowlingHand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_PLAYER_BOWLING_HAND, $playerBowlingHand, $comparison);
    }

    /**
     * Filter the query on the player_dob column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerDob('fooValue');   // WHERE player_dob = 'fooValue'
     * $query->filterByPlayerDob('%fooValue%', Criteria::LIKE); // WHERE player_dob LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playerDob The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByPlayerDob($playerDob = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playerDob)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_PLAYER_DOB, $playerDob, $comparison);
    }

    /**
     * Filter the query on the logo column
     *
     * Example usage:
     * <code>
     * $query->filterByLogo('fooValue');   // WHERE logo = 'fooValue'
     * $query->filterByLogo('%fooValue%', Criteria::LIKE); // WHERE logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByLogo($logo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_LOGO, $logo, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%', Criteria::LIKE); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the batting column
     *
     * Example usage:
     * <code>
     * $query->filterByBatting(1234); // WHERE batting = 1234
     * $query->filterByBatting(array(12, 34)); // WHERE batting IN (12, 34)
     * $query->filterByBatting(array('min' => 12)); // WHERE batting > 12
     * </code>
     *
     * @param     mixed $batting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByBatting($batting = null, $comparison = null)
    {
        if (is_array($batting)) {
            $useMinMax = false;
            if (isset($batting['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_BATTING, $batting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($batting['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_BATTING, $batting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_BATTING, $batting, $comparison);
    }

    /**
     * Filter the query on the bowling column
     *
     * Example usage:
     * <code>
     * $query->filterByBowling(1234); // WHERE bowling = 1234
     * $query->filterByBowling(array(12, 34)); // WHERE bowling IN (12, 34)
     * $query->filterByBowling(array('min' => 12)); // WHERE bowling > 12
     * </code>
     *
     * @param     mixed $bowling The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByBowling($bowling = null, $comparison = null)
    {
        if (is_array($bowling)) {
            $useMinMax = false;
            if (isset($bowling['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_BOWLING, $bowling['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowling['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_BOWLING, $bowling['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_BOWLING, $bowling, $comparison);
    }

    /**
     * Filter the query on the fielding column
     *
     * Example usage:
     * <code>
     * $query->filterByFielding(1234); // WHERE fielding = 1234
     * $query->filterByFielding(array(12, 34)); // WHERE fielding IN (12, 34)
     * $query->filterByFielding(array('min' => 12)); // WHERE fielding > 12
     * </code>
     *
     * @param     mixed $fielding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByFielding($fielding = null, $comparison = null)
    {
        if (is_array($fielding)) {
            $useMinMax = false;
            if (isset($fielding['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_FIELDING, $fielding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fielding['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_FIELDING, $fielding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_FIELDING, $fielding, $comparison);
    }

    /**
     * Filter the query on the league column
     *
     * Example usage:
     * <code>
     * $query->filterByLeague(1234); // WHERE league = 1234
     * $query->filterByLeague(array(12, 34)); // WHERE league IN (12, 34)
     * $query->filterByLeague(array('min' => 12)); // WHERE league > 12
     * </code>
     *
     * @param     mixed $league The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByLeague($league = null, $comparison = null)
    {
        if (is_array($league)) {
            $useMinMax = false;
            if (isset($league['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_LEAGUE, $league['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($league['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_LEAGUE, $league['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_LEAGUE, $league, $comparison);
    }

    /**
     * Filter the query on the is_international column
     *
     * Example usage:
     * <code>
     * $query->filterByIsInternational(1234); // WHERE is_international = 1234
     * $query->filterByIsInternational(array(12, 34)); // WHERE is_international IN (12, 34)
     * $query->filterByIsInternational(array('min' => 12)); // WHERE is_international > 12
     * </code>
     *
     * @param     mixed $isInternational The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByIsInternational($isInternational = null, $comparison = null)
    {
        if (is_array($isInternational)) {
            $useMinMax = false;
            if (isset($isInternational['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_IS_INTERNATIONAL, $isInternational['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isInternational['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_IS_INTERNATIONAL, $isInternational['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_IS_INTERNATIONAL, $isInternational, $comparison);
    }

    /**
     * Filter the query on the team_type column
     *
     * Example usage:
     * <code>
     * $query->filterByTeamType('fooValue');   // WHERE team_type = 'fooValue'
     * $query->filterByTeamType('%fooValue%', Criteria::LIKE); // WHERE team_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $teamType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByTeamType($teamType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($teamType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_TEAM_TYPE, $teamType, $comparison);
    }

    /**
     * Filter the query on the stadium_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStadiumID(1234); // WHERE stadium_id = 1234
     * $query->filterByStadiumID(array(12, 34)); // WHERE stadium_id IN (12, 34)
     * $query->filterByStadiumID(array('min' => 12)); // WHERE stadium_id > 12
     * </code>
     *
     * @param     mixed $stadiumID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function filterByStadiumID($stadiumID = null, $comparison = null)
    {
        if (is_array($stadiumID)) {
            $useMinMax = false;
            if (isset($stadiumID['min'])) {
                $this->addUsingAlias(TeamsTableMap::COL_STADIUM_ID, $stadiumID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stadiumID['max'])) {
                $this->addUsingAlias(TeamsTableMap::COL_STADIUM_ID, $stadiumID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TeamsTableMap::COL_STADIUM_ID, $stadiumID, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTeams $teams Object to remove from the list of results
     *
     * @return $this|ChildTeamsQuery The current query, for fluid interface
     */
    public function prune($teams = null)
    {
        if ($teams) {
            $this->addUsingAlias(TeamsTableMap::COL_ID, $teams->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the teams table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TeamsTableMap::clearInstancePool();
            TeamsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TeamsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TeamsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TeamsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TeamsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TeamsQuery
