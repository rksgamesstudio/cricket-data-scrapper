<?php

namespace Base;

use \BattingQuery as ChildBattingQuery;
use \Exception;
use \PDO;
use Map\BattingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'batting' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Batting implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\BattingTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the player field.
     *
     * @var        int
     */
    protected $player;

    /**
     * The value for the shotpower field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotpower;

    /**
     * The value for the shotaccurecy field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotaccurecy;

    /**
     * The value for the shotjudgement field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotjudgement;

    /**
     * The value for the shotspeed field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotspeed;

    /**
     * The value for the shotmedium field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotmedium;

    /**
     * The value for the shotspinner field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shotspinner;

    /**
     * The value for the runningspeed field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $runningspeed;

    /**
     * The value for the shortballstrength field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $shortballstrength;

    /**
     * The value for the lengthballstrength field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $lengthballstrength;

    /**
     * The value for the fullballstrength field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $fullballstrength;

    /**
     * The value for the temprament field.
     *
     * Note: this column has a database default value of: 90
     * @var        int
     */
    protected $temprament;

    /**
     * The value for the aggression field.
     *
     * @var        int
     */
    protected $aggression;

    /**
     * The value for the straightstrength field.
     *
     * @var        int
     */
    protected $straightstrength;

    /**
     * The value for the squarestrength field.
     *
     * @var        int
     */
    protected $squarestrength;

    /**
     * The value for the average field.
     *
     * @var        int
     */
    protected $average;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->shotpower = 90;
        $this->shotaccurecy = 90;
        $this->shotjudgement = 90;
        $this->shotspeed = 90;
        $this->shotmedium = 90;
        $this->shotspinner = 90;
        $this->runningspeed = 90;
        $this->shortballstrength = 90;
        $this->lengthballstrength = 90;
        $this->fullballstrength = 90;
        $this->temprament = 90;
    }

    /**
     * Initializes internal state of Base\Batting object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Batting</code> instance.  If
     * <code>obj</code> is an instance of <code>Batting</code>, delegates to
     * <code>equals(Batting)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Batting The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [player] column value.
     *
     * @return int
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Get the [shotpower] column value.
     *
     * @return int
     */
    public function getShotpower()
    {
        return $this->shotpower;
    }

    /**
     * Get the [shotaccurecy] column value.
     *
     * @return int
     */
    public function getShotaccurecy()
    {
        return $this->shotaccurecy;
    }

    /**
     * Get the [shotjudgement] column value.
     *
     * @return int
     */
    public function getShotjudgement()
    {
        return $this->shotjudgement;
    }

    /**
     * Get the [shotspeed] column value.
     *
     * @return int
     */
    public function getShotspeed()
    {
        return $this->shotspeed;
    }

    /**
     * Get the [shotmedium] column value.
     *
     * @return int
     */
    public function getShotmedium()
    {
        return $this->shotmedium;
    }

    /**
     * Get the [shotspinner] column value.
     *
     * @return int
     */
    public function getShotspinner()
    {
        return $this->shotspinner;
    }

    /**
     * Get the [runningspeed] column value.
     *
     * @return int
     */
    public function getRunningspeed()
    {
        return $this->runningspeed;
    }

    /**
     * Get the [shortballstrength] column value.
     *
     * @return int
     */
    public function getShortballstrength()
    {
        return $this->shortballstrength;
    }

    /**
     * Get the [lengthballstrength] column value.
     *
     * @return int
     */
    public function getLengthballstrength()
    {
        return $this->lengthballstrength;
    }

    /**
     * Get the [fullballstrength] column value.
     *
     * @return int
     */
    public function getFullballstrength()
    {
        return $this->fullballstrength;
    }

    /**
     * Get the [temprament] column value.
     *
     * @return int
     */
    public function getTemprament()
    {
        return $this->temprament;
    }

    /**
     * Get the [aggression] column value.
     *
     * @return int
     */
    public function getAggression()
    {
        return $this->aggression;
    }

    /**
     * Get the [straightstrength] column value.
     *
     * @return int
     */
    public function getStraightstrength()
    {
        return $this->straightstrength;
    }

    /**
     * Get the [squarestrength] column value.
     *
     * @return int
     */
    public function getSquarestrength()
    {
        return $this->squarestrength;
    }

    /**
     * Get the [average] column value.
     *
     * @return int
     */
    public function getAverage()
    {
        return $this->average;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[BattingTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [player] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setPlayer($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->player !== $v) {
            $this->player = $v;
            $this->modifiedColumns[BattingTableMap::COL_PLAYER] = true;
        }

        return $this;
    } // setPlayer()

    /**
     * Set the value of [shotpower] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotpower($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotpower !== $v) {
            $this->shotpower = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTPOWER] = true;
        }

        return $this;
    } // setShotpower()

    /**
     * Set the value of [shotaccurecy] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotaccurecy($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotaccurecy !== $v) {
            $this->shotaccurecy = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTACCURECY] = true;
        }

        return $this;
    } // setShotaccurecy()

    /**
     * Set the value of [shotjudgement] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotjudgement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotjudgement !== $v) {
            $this->shotjudgement = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTJUDGEMENT] = true;
        }

        return $this;
    } // setShotjudgement()

    /**
     * Set the value of [shotspeed] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotspeed($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotspeed !== $v) {
            $this->shotspeed = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTSPEED] = true;
        }

        return $this;
    } // setShotspeed()

    /**
     * Set the value of [shotmedium] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotmedium($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotmedium !== $v) {
            $this->shotmedium = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTMEDIUM] = true;
        }

        return $this;
    } // setShotmedium()

    /**
     * Set the value of [shotspinner] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShotspinner($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shotspinner !== $v) {
            $this->shotspinner = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHOTSPINNER] = true;
        }

        return $this;
    } // setShotspinner()

    /**
     * Set the value of [runningspeed] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setRunningspeed($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->runningspeed !== $v) {
            $this->runningspeed = $v;
            $this->modifiedColumns[BattingTableMap::COL_RUNNINGSPEED] = true;
        }

        return $this;
    } // setRunningspeed()

    /**
     * Set the value of [shortballstrength] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setShortballstrength($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shortballstrength !== $v) {
            $this->shortballstrength = $v;
            $this->modifiedColumns[BattingTableMap::COL_SHORTBALLSTRENGTH] = true;
        }

        return $this;
    } // setShortballstrength()

    /**
     * Set the value of [lengthballstrength] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setLengthballstrength($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->lengthballstrength !== $v) {
            $this->lengthballstrength = $v;
            $this->modifiedColumns[BattingTableMap::COL_LENGTHBALLSTRENGTH] = true;
        }

        return $this;
    } // setLengthballstrength()

    /**
     * Set the value of [fullballstrength] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setFullballstrength($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fullballstrength !== $v) {
            $this->fullballstrength = $v;
            $this->modifiedColumns[BattingTableMap::COL_FULLBALLSTRENGTH] = true;
        }

        return $this;
    } // setFullballstrength()

    /**
     * Set the value of [temprament] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setTemprament($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->temprament !== $v) {
            $this->temprament = $v;
            $this->modifiedColumns[BattingTableMap::COL_TEMPRAMENT] = true;
        }

        return $this;
    } // setTemprament()

    /**
     * Set the value of [aggression] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setAggression($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->aggression !== $v) {
            $this->aggression = $v;
            $this->modifiedColumns[BattingTableMap::COL_AGGRESSION] = true;
        }

        return $this;
    } // setAggression()

    /**
     * Set the value of [straightstrength] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setStraightstrength($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->straightstrength !== $v) {
            $this->straightstrength = $v;
            $this->modifiedColumns[BattingTableMap::COL_STRAIGHTSTRENGTH] = true;
        }

        return $this;
    } // setStraightstrength()

    /**
     * Set the value of [squarestrength] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setSquarestrength($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->squarestrength !== $v) {
            $this->squarestrength = $v;
            $this->modifiedColumns[BattingTableMap::COL_SQUARESTRENGTH] = true;
        }

        return $this;
    } // setSquarestrength()

    /**
     * Set the value of [average] column.
     *
     * @param int $v new value
     * @return $this|\Batting The current object (for fluent API support)
     */
    public function setAverage($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->average !== $v) {
            $this->average = $v;
            $this->modifiedColumns[BattingTableMap::COL_AVERAGE] = true;
        }

        return $this;
    } // setAverage()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->shotpower !== 90) {
                return false;
            }

            if ($this->shotaccurecy !== 90) {
                return false;
            }

            if ($this->shotjudgement !== 90) {
                return false;
            }

            if ($this->shotspeed !== 90) {
                return false;
            }

            if ($this->shotmedium !== 90) {
                return false;
            }

            if ($this->shotspinner !== 90) {
                return false;
            }

            if ($this->runningspeed !== 90) {
                return false;
            }

            if ($this->shortballstrength !== 90) {
                return false;
            }

            if ($this->lengthballstrength !== 90) {
                return false;
            }

            if ($this->fullballstrength !== 90) {
                return false;
            }

            if ($this->temprament !== 90) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BattingTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BattingTableMap::translateFieldName('Player', TableMap::TYPE_PHPNAME, $indexType)];
            $this->player = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BattingTableMap::translateFieldName('Shotpower', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotpower = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BattingTableMap::translateFieldName('Shotaccurecy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotaccurecy = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BattingTableMap::translateFieldName('Shotjudgement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotjudgement = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BattingTableMap::translateFieldName('Shotspeed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotspeed = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BattingTableMap::translateFieldName('Shotmedium', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotmedium = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BattingTableMap::translateFieldName('Shotspinner', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shotspinner = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BattingTableMap::translateFieldName('Runningspeed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->runningspeed = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BattingTableMap::translateFieldName('Shortballstrength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shortballstrength = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : BattingTableMap::translateFieldName('Lengthballstrength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lengthballstrength = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : BattingTableMap::translateFieldName('Fullballstrength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fullballstrength = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : BattingTableMap::translateFieldName('Temprament', TableMap::TYPE_PHPNAME, $indexType)];
            $this->temprament = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : BattingTableMap::translateFieldName('Aggression', TableMap::TYPE_PHPNAME, $indexType)];
            $this->aggression = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : BattingTableMap::translateFieldName('Straightstrength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->straightstrength = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : BattingTableMap::translateFieldName('Squarestrength', TableMap::TYPE_PHPNAME, $indexType)];
            $this->squarestrength = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : BattingTableMap::translateFieldName('Average', TableMap::TYPE_PHPNAME, $indexType)];
            $this->average = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 17; // 17 = BattingTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Batting'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BattingTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBattingQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Batting::setDeleted()
     * @see Batting::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBattingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BattingTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BattingTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BattingTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BattingTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(BattingTableMap::COL_PLAYER)) {
            $modifiedColumns[':p' . $index++]  = 'player';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTPOWER)) {
            $modifiedColumns[':p' . $index++]  = 'shotPower';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTACCURECY)) {
            $modifiedColumns[':p' . $index++]  = 'shotAccurecy';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTJUDGEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'shotJudgement';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTSPEED)) {
            $modifiedColumns[':p' . $index++]  = 'shotSpeed';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTMEDIUM)) {
            $modifiedColumns[':p' . $index++]  = 'shotMedium';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTSPINNER)) {
            $modifiedColumns[':p' . $index++]  = 'shotSpinner';
        }
        if ($this->isColumnModified(BattingTableMap::COL_RUNNINGSPEED)) {
            $modifiedColumns[':p' . $index++]  = 'runningSpeed';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHORTBALLSTRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'shortBallStrength';
        }
        if ($this->isColumnModified(BattingTableMap::COL_LENGTHBALLSTRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'lengthBallStrength';
        }
        if ($this->isColumnModified(BattingTableMap::COL_FULLBALLSTRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'fullBallStrength';
        }
        if ($this->isColumnModified(BattingTableMap::COL_TEMPRAMENT)) {
            $modifiedColumns[':p' . $index++]  = 'temprament';
        }
        if ($this->isColumnModified(BattingTableMap::COL_AGGRESSION)) {
            $modifiedColumns[':p' . $index++]  = 'aggression';
        }
        if ($this->isColumnModified(BattingTableMap::COL_STRAIGHTSTRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'straightStrength';
        }
        if ($this->isColumnModified(BattingTableMap::COL_SQUARESTRENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'squareStrength';
        }
        if ($this->isColumnModified(BattingTableMap::COL_AVERAGE)) {
            $modifiedColumns[':p' . $index++]  = 'average';
        }

        $sql = sprintf(
            'INSERT INTO batting (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'player':
                        $stmt->bindValue($identifier, $this->player, PDO::PARAM_INT);
                        break;
                    case 'shotPower':
                        $stmt->bindValue($identifier, $this->shotpower, PDO::PARAM_INT);
                        break;
                    case 'shotAccurecy':
                        $stmt->bindValue($identifier, $this->shotaccurecy, PDO::PARAM_INT);
                        break;
                    case 'shotJudgement':
                        $stmt->bindValue($identifier, $this->shotjudgement, PDO::PARAM_INT);
                        break;
                    case 'shotSpeed':
                        $stmt->bindValue($identifier, $this->shotspeed, PDO::PARAM_INT);
                        break;
                    case 'shotMedium':
                        $stmt->bindValue($identifier, $this->shotmedium, PDO::PARAM_INT);
                        break;
                    case 'shotSpinner':
                        $stmt->bindValue($identifier, $this->shotspinner, PDO::PARAM_INT);
                        break;
                    case 'runningSpeed':
                        $stmt->bindValue($identifier, $this->runningspeed, PDO::PARAM_INT);
                        break;
                    case 'shortBallStrength':
                        $stmt->bindValue($identifier, $this->shortballstrength, PDO::PARAM_INT);
                        break;
                    case 'lengthBallStrength':
                        $stmt->bindValue($identifier, $this->lengthballstrength, PDO::PARAM_INT);
                        break;
                    case 'fullBallStrength':
                        $stmt->bindValue($identifier, $this->fullballstrength, PDO::PARAM_INT);
                        break;
                    case 'temprament':
                        $stmt->bindValue($identifier, $this->temprament, PDO::PARAM_INT);
                        break;
                    case 'aggression':
                        $stmt->bindValue($identifier, $this->aggression, PDO::PARAM_INT);
                        break;
                    case 'straightStrength':
                        $stmt->bindValue($identifier, $this->straightstrength, PDO::PARAM_INT);
                        break;
                    case 'squareStrength':
                        $stmt->bindValue($identifier, $this->squarestrength, PDO::PARAM_INT);
                        break;
                    case 'average':
                        $stmt->bindValue($identifier, $this->average, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BattingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPlayer();
                break;
            case 2:
                return $this->getShotpower();
                break;
            case 3:
                return $this->getShotaccurecy();
                break;
            case 4:
                return $this->getShotjudgement();
                break;
            case 5:
                return $this->getShotspeed();
                break;
            case 6:
                return $this->getShotmedium();
                break;
            case 7:
                return $this->getShotspinner();
                break;
            case 8:
                return $this->getRunningspeed();
                break;
            case 9:
                return $this->getShortballstrength();
                break;
            case 10:
                return $this->getLengthballstrength();
                break;
            case 11:
                return $this->getFullballstrength();
                break;
            case 12:
                return $this->getTemprament();
                break;
            case 13:
                return $this->getAggression();
                break;
            case 14:
                return $this->getStraightstrength();
                break;
            case 15:
                return $this->getSquarestrength();
                break;
            case 16:
                return $this->getAverage();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Batting'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Batting'][$this->hashCode()] = true;
        $keys = BattingTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPlayer(),
            $keys[2] => $this->getShotpower(),
            $keys[3] => $this->getShotaccurecy(),
            $keys[4] => $this->getShotjudgement(),
            $keys[5] => $this->getShotspeed(),
            $keys[6] => $this->getShotmedium(),
            $keys[7] => $this->getShotspinner(),
            $keys[8] => $this->getRunningspeed(),
            $keys[9] => $this->getShortballstrength(),
            $keys[10] => $this->getLengthballstrength(),
            $keys[11] => $this->getFullballstrength(),
            $keys[12] => $this->getTemprament(),
            $keys[13] => $this->getAggression(),
            $keys[14] => $this->getStraightstrength(),
            $keys[15] => $this->getSquarestrength(),
            $keys[16] => $this->getAverage(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Batting
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BattingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Batting
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPlayer($value);
                break;
            case 2:
                $this->setShotpower($value);
                break;
            case 3:
                $this->setShotaccurecy($value);
                break;
            case 4:
                $this->setShotjudgement($value);
                break;
            case 5:
                $this->setShotspeed($value);
                break;
            case 6:
                $this->setShotmedium($value);
                break;
            case 7:
                $this->setShotspinner($value);
                break;
            case 8:
                $this->setRunningspeed($value);
                break;
            case 9:
                $this->setShortballstrength($value);
                break;
            case 10:
                $this->setLengthballstrength($value);
                break;
            case 11:
                $this->setFullballstrength($value);
                break;
            case 12:
                $this->setTemprament($value);
                break;
            case 13:
                $this->setAggression($value);
                break;
            case 14:
                $this->setStraightstrength($value);
                break;
            case 15:
                $this->setSquarestrength($value);
                break;
            case 16:
                $this->setAverage($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BattingTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPlayer($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setShotpower($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setShotaccurecy($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setShotjudgement($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setShotspeed($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setShotmedium($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setShotspinner($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRunningspeed($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setShortballstrength($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLengthballstrength($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFullballstrength($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTemprament($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setAggression($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setStraightstrength($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setSquarestrength($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setAverage($arr[$keys[16]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Batting The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BattingTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BattingTableMap::COL_ID)) {
            $criteria->add(BattingTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(BattingTableMap::COL_PLAYER)) {
            $criteria->add(BattingTableMap::COL_PLAYER, $this->player);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTPOWER)) {
            $criteria->add(BattingTableMap::COL_SHOTPOWER, $this->shotpower);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTACCURECY)) {
            $criteria->add(BattingTableMap::COL_SHOTACCURECY, $this->shotaccurecy);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTJUDGEMENT)) {
            $criteria->add(BattingTableMap::COL_SHOTJUDGEMENT, $this->shotjudgement);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTSPEED)) {
            $criteria->add(BattingTableMap::COL_SHOTSPEED, $this->shotspeed);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTMEDIUM)) {
            $criteria->add(BattingTableMap::COL_SHOTMEDIUM, $this->shotmedium);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHOTSPINNER)) {
            $criteria->add(BattingTableMap::COL_SHOTSPINNER, $this->shotspinner);
        }
        if ($this->isColumnModified(BattingTableMap::COL_RUNNINGSPEED)) {
            $criteria->add(BattingTableMap::COL_RUNNINGSPEED, $this->runningspeed);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SHORTBALLSTRENGTH)) {
            $criteria->add(BattingTableMap::COL_SHORTBALLSTRENGTH, $this->shortballstrength);
        }
        if ($this->isColumnModified(BattingTableMap::COL_LENGTHBALLSTRENGTH)) {
            $criteria->add(BattingTableMap::COL_LENGTHBALLSTRENGTH, $this->lengthballstrength);
        }
        if ($this->isColumnModified(BattingTableMap::COL_FULLBALLSTRENGTH)) {
            $criteria->add(BattingTableMap::COL_FULLBALLSTRENGTH, $this->fullballstrength);
        }
        if ($this->isColumnModified(BattingTableMap::COL_TEMPRAMENT)) {
            $criteria->add(BattingTableMap::COL_TEMPRAMENT, $this->temprament);
        }
        if ($this->isColumnModified(BattingTableMap::COL_AGGRESSION)) {
            $criteria->add(BattingTableMap::COL_AGGRESSION, $this->aggression);
        }
        if ($this->isColumnModified(BattingTableMap::COL_STRAIGHTSTRENGTH)) {
            $criteria->add(BattingTableMap::COL_STRAIGHTSTRENGTH, $this->straightstrength);
        }
        if ($this->isColumnModified(BattingTableMap::COL_SQUARESTRENGTH)) {
            $criteria->add(BattingTableMap::COL_SQUARESTRENGTH, $this->squarestrength);
        }
        if ($this->isColumnModified(BattingTableMap::COL_AVERAGE)) {
            $criteria->add(BattingTableMap::COL_AVERAGE, $this->average);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBattingQuery::create();
        $criteria->add(BattingTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Batting (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPlayer($this->getPlayer());
        $copyObj->setShotpower($this->getShotpower());
        $copyObj->setShotaccurecy($this->getShotaccurecy());
        $copyObj->setShotjudgement($this->getShotjudgement());
        $copyObj->setShotspeed($this->getShotspeed());
        $copyObj->setShotmedium($this->getShotmedium());
        $copyObj->setShotspinner($this->getShotspinner());
        $copyObj->setRunningspeed($this->getRunningspeed());
        $copyObj->setShortballstrength($this->getShortballstrength());
        $copyObj->setLengthballstrength($this->getLengthballstrength());
        $copyObj->setFullballstrength($this->getFullballstrength());
        $copyObj->setTemprament($this->getTemprament());
        $copyObj->setAggression($this->getAggression());
        $copyObj->setStraightstrength($this->getStraightstrength());
        $copyObj->setSquarestrength($this->getSquarestrength());
        $copyObj->setAverage($this->getAverage());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Batting Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->player = null;
        $this->shotpower = null;
        $this->shotaccurecy = null;
        $this->shotjudgement = null;
        $this->shotspeed = null;
        $this->shotmedium = null;
        $this->shotspinner = null;
        $this->runningspeed = null;
        $this->shortballstrength = null;
        $this->lengthballstrength = null;
        $this->fullballstrength = null;
        $this->temprament = null;
        $this->aggression = null;
        $this->straightstrength = null;
        $this->squarestrength = null;
        $this->average = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BattingTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
