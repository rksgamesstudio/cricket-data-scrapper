<?php

namespace Base;

use \RawStream as ChildRawStream;
use \RawStreamQuery as ChildRawStreamQuery;
use \Exception;
use \PDO;
use Map\RawStreamTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'raw_stream' table.
 *
 *
 *
 * @method     ChildRawStreamQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRawStreamQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRawStreamQuery orderByBattingHand($order = Criteria::ASC) Order by the batting_hand column
 * @method     ChildRawStreamQuery orderByBowlingHand($order = Criteria::ASC) Order by the bowling_hand column
 * @method     ChildRawStreamQuery orderByAge($order = Criteria::ASC) Order by the age column
 * @method     ChildRawStreamQuery orderByTeam($order = Criteria::ASC) Order by the team column
 * @method     ChildRawStreamQuery orderByBowlertype($order = Criteria::ASC) Order by the bowler_type column
 * @method     ChildRawStreamQuery orderBySpintype($order = Criteria::ASC) Order by the spin_type column
 * @method     ChildRawStreamQuery orderByBatting($order = Criteria::ASC) Order by the batting column
 * @method     ChildRawStreamQuery orderByBowling($order = Criteria::ASC) Order by the bowling column
 * @method     ChildRawStreamQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method     ChildRawStreamQuery orderByPrimaryRole($order = Criteria::ASC) Order by the primary_role column
 * @method     ChildRawStreamQuery orderByBattingorder($order = Criteria::ASC) Order by the batting_order column
 * @method     ChildRawStreamQuery orderByRace($order = Criteria::ASC) Order by the race column
 * @method     ChildRawStreamQuery orderByLink($order = Criteria::ASC) Order by the link column
 * @method     ChildRawStreamQuery orderByIsStatsProcessed($order = Criteria::ASC) Order by the is_stats_processed column
 * @method     ChildRawStreamQuery orderByStatsLink($order = Criteria::ASC) Order by the stats_link column
 * @method     ChildRawStreamQuery orderByIsProcessed($order = Criteria::ASC) Order by the is_processed column
 *
 * @method     ChildRawStreamQuery groupById() Group by the id column
 * @method     ChildRawStreamQuery groupByName() Group by the name column
 * @method     ChildRawStreamQuery groupByBattingHand() Group by the batting_hand column
 * @method     ChildRawStreamQuery groupByBowlingHand() Group by the bowling_hand column
 * @method     ChildRawStreamQuery groupByAge() Group by the age column
 * @method     ChildRawStreamQuery groupByTeam() Group by the team column
 * @method     ChildRawStreamQuery groupByBowlertype() Group by the bowler_type column
 * @method     ChildRawStreamQuery groupBySpintype() Group by the spin_type column
 * @method     ChildRawStreamQuery groupByBatting() Group by the batting column
 * @method     ChildRawStreamQuery groupByBowling() Group by the bowling column
 * @method     ChildRawStreamQuery groupByValue() Group by the value column
 * @method     ChildRawStreamQuery groupByPrimaryRole() Group by the primary_role column
 * @method     ChildRawStreamQuery groupByBattingorder() Group by the batting_order column
 * @method     ChildRawStreamQuery groupByRace() Group by the race column
 * @method     ChildRawStreamQuery groupByLink() Group by the link column
 * @method     ChildRawStreamQuery groupByIsStatsProcessed() Group by the is_stats_processed column
 * @method     ChildRawStreamQuery groupByStatsLink() Group by the stats_link column
 * @method     ChildRawStreamQuery groupByIsProcessed() Group by the is_processed column
 *
 * @method     ChildRawStreamQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRawStreamQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRawStreamQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRawStreamQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRawStreamQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRawStreamQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRawStream findOne(ConnectionInterface $con = null) Return the first ChildRawStream matching the query
 * @method     ChildRawStream findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRawStream matching the query, or a new ChildRawStream object populated from the query conditions when no match is found
 *
 * @method     ChildRawStream findOneById(int $id) Return the first ChildRawStream filtered by the id column
 * @method     ChildRawStream findOneByName(string $name) Return the first ChildRawStream filtered by the name column
 * @method     ChildRawStream findOneByBattingHand(string $batting_hand) Return the first ChildRawStream filtered by the batting_hand column
 * @method     ChildRawStream findOneByBowlingHand(string $bowling_hand) Return the first ChildRawStream filtered by the bowling_hand column
 * @method     ChildRawStream findOneByAge(int $age) Return the first ChildRawStream filtered by the age column
 * @method     ChildRawStream findOneByTeam(string $team) Return the first ChildRawStream filtered by the team column
 * @method     ChildRawStream findOneByBowlertype(string $bowler_type) Return the first ChildRawStream filtered by the bowler_type column
 * @method     ChildRawStream findOneBySpintype(string $spin_type) Return the first ChildRawStream filtered by the spin_type column
 * @method     ChildRawStream findOneByBatting(int $batting) Return the first ChildRawStream filtered by the batting column
 * @method     ChildRawStream findOneByBowling(int $bowling) Return the first ChildRawStream filtered by the bowling column
 * @method     ChildRawStream findOneByValue(double $value) Return the first ChildRawStream filtered by the value column
 * @method     ChildRawStream findOneByPrimaryRole(string $primary_role) Return the first ChildRawStream filtered by the primary_role column
 * @method     ChildRawStream findOneByBattingorder(int $batting_order) Return the first ChildRawStream filtered by the batting_order column
 * @method     ChildRawStream findOneByRace(string $race) Return the first ChildRawStream filtered by the race column
 * @method     ChildRawStream findOneByLink(string $link) Return the first ChildRawStream filtered by the link column
 * @method     ChildRawStream findOneByIsStatsProcessed(int $is_stats_processed) Return the first ChildRawStream filtered by the is_stats_processed column
 * @method     ChildRawStream findOneByStatsLink(string $stats_link) Return the first ChildRawStream filtered by the stats_link column
 * @method     ChildRawStream findOneByIsProcessed(int $is_processed) Return the first ChildRawStream filtered by the is_processed column *

 * @method     ChildRawStream requirePk($key, ConnectionInterface $con = null) Return the ChildRawStream by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOne(ConnectionInterface $con = null) Return the first ChildRawStream matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStream requireOneById(int $id) Return the first ChildRawStream filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByName(string $name) Return the first ChildRawStream filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBattingHand(string $batting_hand) Return the first ChildRawStream filtered by the batting_hand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBowlingHand(string $bowling_hand) Return the first ChildRawStream filtered by the bowling_hand column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByAge(int $age) Return the first ChildRawStream filtered by the age column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByTeam(string $team) Return the first ChildRawStream filtered by the team column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBowlertype(string $bowler_type) Return the first ChildRawStream filtered by the bowler_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneBySpintype(string $spin_type) Return the first ChildRawStream filtered by the spin_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBatting(int $batting) Return the first ChildRawStream filtered by the batting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBowling(int $bowling) Return the first ChildRawStream filtered by the bowling column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByValue(double $value) Return the first ChildRawStream filtered by the value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByPrimaryRole(string $primary_role) Return the first ChildRawStream filtered by the primary_role column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByBattingorder(int $batting_order) Return the first ChildRawStream filtered by the batting_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByRace(string $race) Return the first ChildRawStream filtered by the race column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByLink(string $link) Return the first ChildRawStream filtered by the link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByIsStatsProcessed(int $is_stats_processed) Return the first ChildRawStream filtered by the is_stats_processed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByStatsLink(string $stats_link) Return the first ChildRawStream filtered by the stats_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStream requireOneByIsProcessed(int $is_processed) Return the first ChildRawStream filtered by the is_processed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStream[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRawStream objects based on current ModelCriteria
 * @method     ChildRawStream[]|ObjectCollection findById(int $id) Return ChildRawStream objects filtered by the id column
 * @method     ChildRawStream[]|ObjectCollection findByName(string $name) Return ChildRawStream objects filtered by the name column
 * @method     ChildRawStream[]|ObjectCollection findByBattingHand(string $batting_hand) Return ChildRawStream objects filtered by the batting_hand column
 * @method     ChildRawStream[]|ObjectCollection findByBowlingHand(string $bowling_hand) Return ChildRawStream objects filtered by the bowling_hand column
 * @method     ChildRawStream[]|ObjectCollection findByAge(int $age) Return ChildRawStream objects filtered by the age column
 * @method     ChildRawStream[]|ObjectCollection findByTeam(string $team) Return ChildRawStream objects filtered by the team column
 * @method     ChildRawStream[]|ObjectCollection findByBowlertype(string $bowler_type) Return ChildRawStream objects filtered by the bowler_type column
 * @method     ChildRawStream[]|ObjectCollection findBySpintype(string $spin_type) Return ChildRawStream objects filtered by the spin_type column
 * @method     ChildRawStream[]|ObjectCollection findByBatting(int $batting) Return ChildRawStream objects filtered by the batting column
 * @method     ChildRawStream[]|ObjectCollection findByBowling(int $bowling) Return ChildRawStream objects filtered by the bowling column
 * @method     ChildRawStream[]|ObjectCollection findByValue(double $value) Return ChildRawStream objects filtered by the value column
 * @method     ChildRawStream[]|ObjectCollection findByPrimaryRole(string $primary_role) Return ChildRawStream objects filtered by the primary_role column
 * @method     ChildRawStream[]|ObjectCollection findByBattingorder(int $batting_order) Return ChildRawStream objects filtered by the batting_order column
 * @method     ChildRawStream[]|ObjectCollection findByRace(string $race) Return ChildRawStream objects filtered by the race column
 * @method     ChildRawStream[]|ObjectCollection findByLink(string $link) Return ChildRawStream objects filtered by the link column
 * @method     ChildRawStream[]|ObjectCollection findByIsStatsProcessed(int $is_stats_processed) Return ChildRawStream objects filtered by the is_stats_processed column
 * @method     ChildRawStream[]|ObjectCollection findByStatsLink(string $stats_link) Return ChildRawStream objects filtered by the stats_link column
 * @method     ChildRawStream[]|ObjectCollection findByIsProcessed(int $is_processed) Return ChildRawStream objects filtered by the is_processed column
 * @method     ChildRawStream[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RawStreamQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RawStreamQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RawStream', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRawStreamQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRawStreamQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRawStreamQuery) {
            return $criteria;
        }
        $query = new ChildRawStreamQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRawStream|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RawStreamTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RawStreamTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRawStream A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, batting_hand, bowling_hand, age, team, bowler_type, spin_type, batting, bowling, value, primary_role, batting_order, race, link, is_stats_processed, stats_link, is_processed FROM raw_stream WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRawStream $obj */
            $obj = new ChildRawStream();
            $obj->hydrate($row);
            RawStreamTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRawStream|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RawStreamTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RawStreamTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the batting_hand column
     *
     * Example usage:
     * <code>
     * $query->filterByBattingHand('fooValue');   // WHERE batting_hand = 'fooValue'
     * $query->filterByBattingHand('%fooValue%', Criteria::LIKE); // WHERE batting_hand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $battingHand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBattingHand($battingHand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($battingHand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BATTING_HAND, $battingHand, $comparison);
    }

    /**
     * Filter the query on the bowling_hand column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlingHand('fooValue');   // WHERE bowling_hand = 'fooValue'
     * $query->filterByBowlingHand('%fooValue%', Criteria::LIKE); // WHERE bowling_hand LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bowlingHand The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBowlingHand($bowlingHand = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bowlingHand)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BOWLING_HAND, $bowlingHand, $comparison);
    }

    /**
     * Filter the query on the age column
     *
     * Example usage:
     * <code>
     * $query->filterByAge(1234); // WHERE age = 1234
     * $query->filterByAge(array(12, 34)); // WHERE age IN (12, 34)
     * $query->filterByAge(array('min' => 12)); // WHERE age > 12
     * </code>
     *
     * @param     mixed $age The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByAge($age = null, $comparison = null)
    {
        if (is_array($age)) {
            $useMinMax = false;
            if (isset($age['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_AGE, $age['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($age['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_AGE, $age['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_AGE, $age, $comparison);
    }

    /**
     * Filter the query on the team column
     *
     * Example usage:
     * <code>
     * $query->filterByTeam('fooValue');   // WHERE team = 'fooValue'
     * $query->filterByTeam('%fooValue%', Criteria::LIKE); // WHERE team LIKE '%fooValue%'
     * </code>
     *
     * @param     string $team The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByTeam($team = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($team)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_TEAM, $team, $comparison);
    }

    /**
     * Filter the query on the bowler_type column
     *
     * Example usage:
     * <code>
     * $query->filterByBowlertype('fooValue');   // WHERE bowler_type = 'fooValue'
     * $query->filterByBowlertype('%fooValue%', Criteria::LIKE); // WHERE bowler_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bowlertype The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBowlertype($bowlertype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bowlertype)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BOWLER_TYPE, $bowlertype, $comparison);
    }

    /**
     * Filter the query on the spin_type column
     *
     * Example usage:
     * <code>
     * $query->filterBySpintype('fooValue');   // WHERE spin_type = 'fooValue'
     * $query->filterBySpintype('%fooValue%', Criteria::LIKE); // WHERE spin_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $spintype The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterBySpintype($spintype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($spintype)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_SPIN_TYPE, $spintype, $comparison);
    }

    /**
     * Filter the query on the batting column
     *
     * Example usage:
     * <code>
     * $query->filterByBatting(1234); // WHERE batting = 1234
     * $query->filterByBatting(array(12, 34)); // WHERE batting IN (12, 34)
     * $query->filterByBatting(array('min' => 12)); // WHERE batting > 12
     * </code>
     *
     * @param     mixed $batting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBatting($batting = null, $comparison = null)
    {
        if (is_array($batting)) {
            $useMinMax = false;
            if (isset($batting['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BATTING, $batting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($batting['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BATTING, $batting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BATTING, $batting, $comparison);
    }

    /**
     * Filter the query on the bowling column
     *
     * Example usage:
     * <code>
     * $query->filterByBowling(1234); // WHERE bowling = 1234
     * $query->filterByBowling(array(12, 34)); // WHERE bowling IN (12, 34)
     * $query->filterByBowling(array('min' => 12)); // WHERE bowling > 12
     * </code>
     *
     * @param     mixed $bowling The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBowling($bowling = null, $comparison = null)
    {
        if (is_array($bowling)) {
            $useMinMax = false;
            if (isset($bowling['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BOWLING, $bowling['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bowling['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BOWLING, $bowling['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BOWLING, $bowling, $comparison);
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue(1234); // WHERE value = 1234
     * $query->filterByValue(array(12, 34)); // WHERE value IN (12, 34)
     * $query->filterByValue(array('min' => 12)); // WHERE value > 12
     * </code>
     *
     * @param     mixed $value The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (is_array($value)) {
            $useMinMax = false;
            if (isset($value['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_VALUE, $value['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($value['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_VALUE, $value['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the primary_role column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimaryRole('fooValue');   // WHERE primary_role = 'fooValue'
     * $query->filterByPrimaryRole('%fooValue%', Criteria::LIKE); // WHERE primary_role LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primaryRole The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByPrimaryRole($primaryRole = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primaryRole)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_PRIMARY_ROLE, $primaryRole, $comparison);
    }

    /**
     * Filter the query on the batting_order column
     *
     * Example usage:
     * <code>
     * $query->filterByBattingorder(1234); // WHERE batting_order = 1234
     * $query->filterByBattingorder(array(12, 34)); // WHERE batting_order IN (12, 34)
     * $query->filterByBattingorder(array('min' => 12)); // WHERE batting_order > 12
     * </code>
     *
     * @param     mixed $battingorder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByBattingorder($battingorder = null, $comparison = null)
    {
        if (is_array($battingorder)) {
            $useMinMax = false;
            if (isset($battingorder['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BATTING_ORDER, $battingorder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($battingorder['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_BATTING_ORDER, $battingorder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_BATTING_ORDER, $battingorder, $comparison);
    }

    /**
     * Filter the query on the race column
     *
     * Example usage:
     * <code>
     * $query->filterByRace('fooValue');   // WHERE race = 'fooValue'
     * $query->filterByRace('%fooValue%', Criteria::LIKE); // WHERE race LIKE '%fooValue%'
     * </code>
     *
     * @param     string $race The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByRace($race = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($race)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_RACE, $race, $comparison);
    }

    /**
     * Filter the query on the link column
     *
     * Example usage:
     * <code>
     * $query->filterByLink('fooValue');   // WHERE link = 'fooValue'
     * $query->filterByLink('%fooValue%', Criteria::LIKE); // WHERE link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $link The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByLink($link = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($link)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_LINK, $link, $comparison);
    }

    /**
     * Filter the query on the is_stats_processed column
     *
     * Example usage:
     * <code>
     * $query->filterByIsStatsProcessed(1234); // WHERE is_stats_processed = 1234
     * $query->filterByIsStatsProcessed(array(12, 34)); // WHERE is_stats_processed IN (12, 34)
     * $query->filterByIsStatsProcessed(array('min' => 12)); // WHERE is_stats_processed > 12
     * </code>
     *
     * @param     mixed $isStatsProcessed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByIsStatsProcessed($isStatsProcessed = null, $comparison = null)
    {
        if (is_array($isStatsProcessed)) {
            $useMinMax = false;
            if (isset($isStatsProcessed['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_IS_STATS_PROCESSED, $isStatsProcessed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isStatsProcessed['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_IS_STATS_PROCESSED, $isStatsProcessed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_IS_STATS_PROCESSED, $isStatsProcessed, $comparison);
    }

    /**
     * Filter the query on the stats_link column
     *
     * Example usage:
     * <code>
     * $query->filterByStatsLink('fooValue');   // WHERE stats_link = 'fooValue'
     * $query->filterByStatsLink('%fooValue%', Criteria::LIKE); // WHERE stats_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statsLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByStatsLink($statsLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statsLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_STATS_LINK, $statsLink, $comparison);
    }

    /**
     * Filter the query on the is_processed column
     *
     * Example usage:
     * <code>
     * $query->filterByIsProcessed(1234); // WHERE is_processed = 1234
     * $query->filterByIsProcessed(array(12, 34)); // WHERE is_processed IN (12, 34)
     * $query->filterByIsProcessed(array('min' => 12)); // WHERE is_processed > 12
     * </code>
     *
     * @param     mixed $isProcessed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function filterByIsProcessed($isProcessed = null, $comparison = null)
    {
        if (is_array($isProcessed)) {
            $useMinMax = false;
            if (isset($isProcessed['min'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_IS_PROCESSED, $isProcessed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isProcessed['max'])) {
                $this->addUsingAlias(RawStreamTableMap::COL_IS_PROCESSED, $isProcessed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamTableMap::COL_IS_PROCESSED, $isProcessed, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRawStream $rawStream Object to remove from the list of results
     *
     * @return $this|ChildRawStreamQuery The current query, for fluid interface
     */
    public function prune($rawStream = null)
    {
        if ($rawStream) {
            $this->addUsingAlias(RawStreamTableMap::COL_ID, $rawStream->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the raw_stream table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RawStreamTableMap::clearInstancePool();
            RawStreamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RawStreamTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RawStreamTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RawStreamTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RawStreamQuery
