<?php

namespace Base;

use \PlayerTeams as ChildPlayerTeams;
use \PlayerTeamsQuery as ChildPlayerTeamsQuery;
use \Exception;
use \PDO;
use Map\PlayerTeamsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'player_teams' table.
 *
 *
 *
 * @method     ChildPlayerTeamsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPlayerTeamsQuery orderByTeam($order = Criteria::ASC) Order by the team column
 * @method     ChildPlayerTeamsQuery orderByPlayer($order = Criteria::ASC) Order by the player column
 *
 * @method     ChildPlayerTeamsQuery groupById() Group by the id column
 * @method     ChildPlayerTeamsQuery groupByTeam() Group by the team column
 * @method     ChildPlayerTeamsQuery groupByPlayer() Group by the player column
 *
 * @method     ChildPlayerTeamsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlayerTeamsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlayerTeamsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlayerTeamsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlayerTeamsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlayerTeamsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlayerTeams findOne(ConnectionInterface $con = null) Return the first ChildPlayerTeams matching the query
 * @method     ChildPlayerTeams findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlayerTeams matching the query, or a new ChildPlayerTeams object populated from the query conditions when no match is found
 *
 * @method     ChildPlayerTeams findOneById(int $id) Return the first ChildPlayerTeams filtered by the id column
 * @method     ChildPlayerTeams findOneByTeam(int $team) Return the first ChildPlayerTeams filtered by the team column
 * @method     ChildPlayerTeams findOneByPlayer(int $player) Return the first ChildPlayerTeams filtered by the player column *

 * @method     ChildPlayerTeams requirePk($key, ConnectionInterface $con = null) Return the ChildPlayerTeams by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerTeams requireOne(ConnectionInterface $con = null) Return the first ChildPlayerTeams matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerTeams requireOneById(int $id) Return the first ChildPlayerTeams filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerTeams requireOneByTeam(int $team) Return the first ChildPlayerTeams filtered by the team column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlayerTeams requireOneByPlayer(int $player) Return the first ChildPlayerTeams filtered by the player column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlayerTeams[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlayerTeams objects based on current ModelCriteria
 * @method     ChildPlayerTeams[]|ObjectCollection findById(int $id) Return ChildPlayerTeams objects filtered by the id column
 * @method     ChildPlayerTeams[]|ObjectCollection findByTeam(int $team) Return ChildPlayerTeams objects filtered by the team column
 * @method     ChildPlayerTeams[]|ObjectCollection findByPlayer(int $player) Return ChildPlayerTeams objects filtered by the player column
 * @method     ChildPlayerTeams[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlayerTeamsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PlayerTeamsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PlayerTeams', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlayerTeamsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlayerTeamsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlayerTeamsQuery) {
            return $criteria;
        }
        $query = new ChildPlayerTeamsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlayerTeams|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlayerTeamsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlayerTeamsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlayerTeams A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, team, player FROM player_teams WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlayerTeams $obj */
            $obj = new ChildPlayerTeams();
            $obj->hydrate($row);
            PlayerTeamsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlayerTeams|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the team column
     *
     * Example usage:
     * <code>
     * $query->filterByTeam(1234); // WHERE team = 1234
     * $query->filterByTeam(array(12, 34)); // WHERE team IN (12, 34)
     * $query->filterByTeam(array('min' => 12)); // WHERE team > 12
     * </code>
     *
     * @param     mixed $team The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function filterByTeam($team = null, $comparison = null)
    {
        if (is_array($team)) {
            $useMinMax = false;
            if (isset($team['min'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_TEAM, $team['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($team['max'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_TEAM, $team['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerTeamsTableMap::COL_TEAM, $team, $comparison);
    }

    /**
     * Filter the query on the player column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayer(1234); // WHERE player = 1234
     * $query->filterByPlayer(array(12, 34)); // WHERE player IN (12, 34)
     * $query->filterByPlayer(array('min' => 12)); // WHERE player > 12
     * </code>
     *
     * @param     mixed $player The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function filterByPlayer($player = null, $comparison = null)
    {
        if (is_array($player)) {
            $useMinMax = false;
            if (isset($player['min'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_PLAYER, $player['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($player['max'])) {
                $this->addUsingAlias(PlayerTeamsTableMap::COL_PLAYER, $player['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlayerTeamsTableMap::COL_PLAYER, $player, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlayerTeams $playerTeams Object to remove from the list of results
     *
     * @return $this|ChildPlayerTeamsQuery The current query, for fluid interface
     */
    public function prune($playerTeams = null)
    {
        if ($playerTeams) {
            $this->addUsingAlias(PlayerTeamsTableMap::COL_ID, $playerTeams->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the player_teams table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerTeamsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlayerTeamsTableMap::clearInstancePool();
            PlayerTeamsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayerTeamsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlayerTeamsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlayerTeamsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlayerTeamsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlayerTeamsQuery
