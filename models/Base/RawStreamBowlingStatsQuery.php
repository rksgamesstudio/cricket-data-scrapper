<?php

namespace Base;

use \RawStreamBowlingStats as ChildRawStreamBowlingStats;
use \RawStreamBowlingStatsQuery as ChildRawStreamBowlingStatsQuery;
use \Exception;
use \PDO;
use Map\RawStreamBowlingStatsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'raw_stream_bowling_stats' table.
 *
 *
 *
 * @method     ChildRawStreamBowlingStatsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRawStreamBowlingStatsQuery orderByDataSource($order = Criteria::ASC) Order by the data_source column
 * @method     ChildRawStreamBowlingStatsQuery orderByPlayerId($order = Criteria::ASC) Order by the player_id column
 * @method     ChildRawStreamBowlingStatsQuery orderByFormat($order = Criteria::ASC) Order by the format column
 * @method     ChildRawStreamBowlingStatsQuery orderByMatches($order = Criteria::ASC) Order by the matches column
 * @method     ChildRawStreamBowlingStatsQuery orderByInnings($order = Criteria::ASC) Order by the innings column
 * @method     ChildRawStreamBowlingStatsQuery orderByBalls($order = Criteria::ASC) Order by the balls column
 * @method     ChildRawStreamBowlingStatsQuery orderByRuns($order = Criteria::ASC) Order by the runs column
 * @method     ChildRawStreamBowlingStatsQuery orderByWickets($order = Criteria::ASC) Order by the wickets column
 * @method     ChildRawStreamBowlingStatsQuery orderByAverage($order = Criteria::ASC) Order by the average column
 * @method     ChildRawStreamBowlingStatsQuery orderByEconomy($order = Criteria::ASC) Order by the economy column
 * @method     ChildRawStreamBowlingStatsQuery orderByStrikeRate($order = Criteria::ASC) Order by the strike_rate column
 *
 * @method     ChildRawStreamBowlingStatsQuery groupById() Group by the id column
 * @method     ChildRawStreamBowlingStatsQuery groupByDataSource() Group by the data_source column
 * @method     ChildRawStreamBowlingStatsQuery groupByPlayerId() Group by the player_id column
 * @method     ChildRawStreamBowlingStatsQuery groupByFormat() Group by the format column
 * @method     ChildRawStreamBowlingStatsQuery groupByMatches() Group by the matches column
 * @method     ChildRawStreamBowlingStatsQuery groupByInnings() Group by the innings column
 * @method     ChildRawStreamBowlingStatsQuery groupByBalls() Group by the balls column
 * @method     ChildRawStreamBowlingStatsQuery groupByRuns() Group by the runs column
 * @method     ChildRawStreamBowlingStatsQuery groupByWickets() Group by the wickets column
 * @method     ChildRawStreamBowlingStatsQuery groupByAverage() Group by the average column
 * @method     ChildRawStreamBowlingStatsQuery groupByEconomy() Group by the economy column
 * @method     ChildRawStreamBowlingStatsQuery groupByStrikeRate() Group by the strike_rate column
 *
 * @method     ChildRawStreamBowlingStatsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRawStreamBowlingStatsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRawStreamBowlingStatsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRawStreamBowlingStatsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRawStreamBowlingStatsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRawStreamBowlingStatsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRawStreamBowlingStats findOne(ConnectionInterface $con = null) Return the first ChildRawStreamBowlingStats matching the query
 * @method     ChildRawStreamBowlingStats findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRawStreamBowlingStats matching the query, or a new ChildRawStreamBowlingStats object populated from the query conditions when no match is found
 *
 * @method     ChildRawStreamBowlingStats findOneById(int $id) Return the first ChildRawStreamBowlingStats filtered by the id column
 * @method     ChildRawStreamBowlingStats findOneByDataSource(string $data_source) Return the first ChildRawStreamBowlingStats filtered by the data_source column
 * @method     ChildRawStreamBowlingStats findOneByPlayerId(int $player_id) Return the first ChildRawStreamBowlingStats filtered by the player_id column
 * @method     ChildRawStreamBowlingStats findOneByFormat(string $format) Return the first ChildRawStreamBowlingStats filtered by the format column
 * @method     ChildRawStreamBowlingStats findOneByMatches(int $matches) Return the first ChildRawStreamBowlingStats filtered by the matches column
 * @method     ChildRawStreamBowlingStats findOneByInnings(int $innings) Return the first ChildRawStreamBowlingStats filtered by the innings column
 * @method     ChildRawStreamBowlingStats findOneByBalls(int $balls) Return the first ChildRawStreamBowlingStats filtered by the balls column
 * @method     ChildRawStreamBowlingStats findOneByRuns(int $runs) Return the first ChildRawStreamBowlingStats filtered by the runs column
 * @method     ChildRawStreamBowlingStats findOneByWickets(int $wickets) Return the first ChildRawStreamBowlingStats filtered by the wickets column
 * @method     ChildRawStreamBowlingStats findOneByAverage(double $average) Return the first ChildRawStreamBowlingStats filtered by the average column
 * @method     ChildRawStreamBowlingStats findOneByEconomy(double $economy) Return the first ChildRawStreamBowlingStats filtered by the economy column
 * @method     ChildRawStreamBowlingStats findOneByStrikeRate(double $strike_rate) Return the first ChildRawStreamBowlingStats filtered by the strike_rate column *

 * @method     ChildRawStreamBowlingStats requirePk($key, ConnectionInterface $con = null) Return the ChildRawStreamBowlingStats by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOne(ConnectionInterface $con = null) Return the first ChildRawStreamBowlingStats matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStreamBowlingStats requireOneById(int $id) Return the first ChildRawStreamBowlingStats filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByDataSource(string $data_source) Return the first ChildRawStreamBowlingStats filtered by the data_source column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByPlayerId(int $player_id) Return the first ChildRawStreamBowlingStats filtered by the player_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByFormat(string $format) Return the first ChildRawStreamBowlingStats filtered by the format column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByMatches(int $matches) Return the first ChildRawStreamBowlingStats filtered by the matches column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByInnings(int $innings) Return the first ChildRawStreamBowlingStats filtered by the innings column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByBalls(int $balls) Return the first ChildRawStreamBowlingStats filtered by the balls column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByRuns(int $runs) Return the first ChildRawStreamBowlingStats filtered by the runs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByWickets(int $wickets) Return the first ChildRawStreamBowlingStats filtered by the wickets column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByAverage(double $average) Return the first ChildRawStreamBowlingStats filtered by the average column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByEconomy(double $economy) Return the first ChildRawStreamBowlingStats filtered by the economy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRawStreamBowlingStats requireOneByStrikeRate(double $strike_rate) Return the first ChildRawStreamBowlingStats filtered by the strike_rate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRawStreamBowlingStats objects based on current ModelCriteria
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findById(int $id) Return ChildRawStreamBowlingStats objects filtered by the id column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByDataSource(string $data_source) Return ChildRawStreamBowlingStats objects filtered by the data_source column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByPlayerId(int $player_id) Return ChildRawStreamBowlingStats objects filtered by the player_id column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByFormat(string $format) Return ChildRawStreamBowlingStats objects filtered by the format column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByMatches(int $matches) Return ChildRawStreamBowlingStats objects filtered by the matches column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByInnings(int $innings) Return ChildRawStreamBowlingStats objects filtered by the innings column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByBalls(int $balls) Return ChildRawStreamBowlingStats objects filtered by the balls column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByRuns(int $runs) Return ChildRawStreamBowlingStats objects filtered by the runs column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByWickets(int $wickets) Return ChildRawStreamBowlingStats objects filtered by the wickets column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByAverage(double $average) Return ChildRawStreamBowlingStats objects filtered by the average column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByEconomy(double $economy) Return ChildRawStreamBowlingStats objects filtered by the economy column
 * @method     ChildRawStreamBowlingStats[]|ObjectCollection findByStrikeRate(double $strike_rate) Return ChildRawStreamBowlingStats objects filtered by the strike_rate column
 * @method     ChildRawStreamBowlingStats[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RawStreamBowlingStatsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RawStreamBowlingStatsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RawStreamBowlingStats', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRawStreamBowlingStatsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRawStreamBowlingStatsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRawStreamBowlingStatsQuery) {
            return $criteria;
        }
        $query = new ChildRawStreamBowlingStatsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRawStreamBowlingStats|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RawStreamBowlingStatsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RawStreamBowlingStatsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRawStreamBowlingStats A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, data_source, player_id, format, matches, innings, balls, runs, wickets, average, economy, strike_rate FROM raw_stream_bowling_stats WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRawStreamBowlingStats $obj */
            $obj = new ChildRawStreamBowlingStats();
            $obj->hydrate($row);
            RawStreamBowlingStatsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRawStreamBowlingStats|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the data_source column
     *
     * Example usage:
     * <code>
     * $query->filterByDataSource('fooValue');   // WHERE data_source = 'fooValue'
     * $query->filterByDataSource('%fooValue%', Criteria::LIKE); // WHERE data_source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataSource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByDataSource($dataSource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataSource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_DATA_SOURCE, $dataSource, $comparison);
    }

    /**
     * Filter the query on the player_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayerId(1234); // WHERE player_id = 1234
     * $query->filterByPlayerId(array(12, 34)); // WHERE player_id IN (12, 34)
     * $query->filterByPlayerId(array('min' => 12)); // WHERE player_id > 12
     * </code>
     *
     * @param     mixed $playerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByPlayerId($playerId = null, $comparison = null)
    {
        if (is_array($playerId)) {
            $useMinMax = false;
            if (isset($playerId['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_PLAYER_ID, $playerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($playerId['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_PLAYER_ID, $playerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_PLAYER_ID, $playerId, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%', Criteria::LIKE); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_FORMAT, $format, $comparison);
    }

    /**
     * Filter the query on the matches column
     *
     * Example usage:
     * <code>
     * $query->filterByMatches(1234); // WHERE matches = 1234
     * $query->filterByMatches(array(12, 34)); // WHERE matches IN (12, 34)
     * $query->filterByMatches(array('min' => 12)); // WHERE matches > 12
     * </code>
     *
     * @param     mixed $matches The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByMatches($matches = null, $comparison = null)
    {
        if (is_array($matches)) {
            $useMinMax = false;
            if (isset($matches['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_MATCHES, $matches['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($matches['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_MATCHES, $matches['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_MATCHES, $matches, $comparison);
    }

    /**
     * Filter the query on the innings column
     *
     * Example usage:
     * <code>
     * $query->filterByInnings(1234); // WHERE innings = 1234
     * $query->filterByInnings(array(12, 34)); // WHERE innings IN (12, 34)
     * $query->filterByInnings(array('min' => 12)); // WHERE innings > 12
     * </code>
     *
     * @param     mixed $innings The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByInnings($innings = null, $comparison = null)
    {
        if (is_array($innings)) {
            $useMinMax = false;
            if (isset($innings['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_INNINGS, $innings['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($innings['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_INNINGS, $innings['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_INNINGS, $innings, $comparison);
    }

    /**
     * Filter the query on the balls column
     *
     * Example usage:
     * <code>
     * $query->filterByBalls(1234); // WHERE balls = 1234
     * $query->filterByBalls(array(12, 34)); // WHERE balls IN (12, 34)
     * $query->filterByBalls(array('min' => 12)); // WHERE balls > 12
     * </code>
     *
     * @param     mixed $balls The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByBalls($balls = null, $comparison = null)
    {
        if (is_array($balls)) {
            $useMinMax = false;
            if (isset($balls['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_BALLS, $balls['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($balls['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_BALLS, $balls['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_BALLS, $balls, $comparison);
    }

    /**
     * Filter the query on the runs column
     *
     * Example usage:
     * <code>
     * $query->filterByRuns(1234); // WHERE runs = 1234
     * $query->filterByRuns(array(12, 34)); // WHERE runs IN (12, 34)
     * $query->filterByRuns(array('min' => 12)); // WHERE runs > 12
     * </code>
     *
     * @param     mixed $runs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByRuns($runs = null, $comparison = null)
    {
        if (is_array($runs)) {
            $useMinMax = false;
            if (isset($runs['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_RUNS, $runs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($runs['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_RUNS, $runs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_RUNS, $runs, $comparison);
    }

    /**
     * Filter the query on the wickets column
     *
     * Example usage:
     * <code>
     * $query->filterByWickets(1234); // WHERE wickets = 1234
     * $query->filterByWickets(array(12, 34)); // WHERE wickets IN (12, 34)
     * $query->filterByWickets(array('min' => 12)); // WHERE wickets > 12
     * </code>
     *
     * @param     mixed $wickets The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByWickets($wickets = null, $comparison = null)
    {
        if (is_array($wickets)) {
            $useMinMax = false;
            if (isset($wickets['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_WICKETS, $wickets['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wickets['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_WICKETS, $wickets['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_WICKETS, $wickets, $comparison);
    }

    /**
     * Filter the query on the average column
     *
     * Example usage:
     * <code>
     * $query->filterByAverage(1234); // WHERE average = 1234
     * $query->filterByAverage(array(12, 34)); // WHERE average IN (12, 34)
     * $query->filterByAverage(array('min' => 12)); // WHERE average > 12
     * </code>
     *
     * @param     mixed $average The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByAverage($average = null, $comparison = null)
    {
        if (is_array($average)) {
            $useMinMax = false;
            if (isset($average['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_AVERAGE, $average['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($average['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_AVERAGE, $average['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_AVERAGE, $average, $comparison);
    }

    /**
     * Filter the query on the economy column
     *
     * Example usage:
     * <code>
     * $query->filterByEconomy(1234); // WHERE economy = 1234
     * $query->filterByEconomy(array(12, 34)); // WHERE economy IN (12, 34)
     * $query->filterByEconomy(array('min' => 12)); // WHERE economy > 12
     * </code>
     *
     * @param     mixed $economy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByEconomy($economy = null, $comparison = null)
    {
        if (is_array($economy)) {
            $useMinMax = false;
            if (isset($economy['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ECONOMY, $economy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($economy['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ECONOMY, $economy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ECONOMY, $economy, $comparison);
    }

    /**
     * Filter the query on the strike_rate column
     *
     * Example usage:
     * <code>
     * $query->filterByStrikeRate(1234); // WHERE strike_rate = 1234
     * $query->filterByStrikeRate(array(12, 34)); // WHERE strike_rate IN (12, 34)
     * $query->filterByStrikeRate(array('min' => 12)); // WHERE strike_rate > 12
     * </code>
     *
     * @param     mixed $strikeRate The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function filterByStrikeRate($strikeRate = null, $comparison = null)
    {
        if (is_array($strikeRate)) {
            $useMinMax = false;
            if (isset($strikeRate['min'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_STRIKE_RATE, $strikeRate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($strikeRate['max'])) {
                $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_STRIKE_RATE, $strikeRate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_STRIKE_RATE, $strikeRate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRawStreamBowlingStats $rawStreamBowlingStats Object to remove from the list of results
     *
     * @return $this|ChildRawStreamBowlingStatsQuery The current query, for fluid interface
     */
    public function prune($rawStreamBowlingStats = null)
    {
        if ($rawStreamBowlingStats) {
            $this->addUsingAlias(RawStreamBowlingStatsTableMap::COL_ID, $rawStreamBowlingStats->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the raw_stream_bowling_stats table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBowlingStatsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RawStreamBowlingStatsTableMap::clearInstancePool();
            RawStreamBowlingStatsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RawStreamBowlingStatsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RawStreamBowlingStatsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RawStreamBowlingStatsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RawStreamBowlingStatsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RawStreamBowlingStatsQuery
