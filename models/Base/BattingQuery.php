<?php

namespace Base;

use \Batting as ChildBatting;
use \BattingQuery as ChildBattingQuery;
use \Exception;
use \PDO;
use Map\BattingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'batting' table.
 *
 *
 *
 * @method     ChildBattingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBattingQuery orderByPlayer($order = Criteria::ASC) Order by the player column
 * @method     ChildBattingQuery orderByShotpower($order = Criteria::ASC) Order by the shotPower column
 * @method     ChildBattingQuery orderByShotaccurecy($order = Criteria::ASC) Order by the shotAccurecy column
 * @method     ChildBattingQuery orderByShotjudgement($order = Criteria::ASC) Order by the shotJudgement column
 * @method     ChildBattingQuery orderByShotspeed($order = Criteria::ASC) Order by the shotSpeed column
 * @method     ChildBattingQuery orderByShotmedium($order = Criteria::ASC) Order by the shotMedium column
 * @method     ChildBattingQuery orderByShotspinner($order = Criteria::ASC) Order by the shotSpinner column
 * @method     ChildBattingQuery orderByRunningspeed($order = Criteria::ASC) Order by the runningSpeed column
 * @method     ChildBattingQuery orderByShortballstrength($order = Criteria::ASC) Order by the shortBallStrength column
 * @method     ChildBattingQuery orderByLengthballstrength($order = Criteria::ASC) Order by the lengthBallStrength column
 * @method     ChildBattingQuery orderByFullballstrength($order = Criteria::ASC) Order by the fullBallStrength column
 * @method     ChildBattingQuery orderByTemprament($order = Criteria::ASC) Order by the temprament column
 * @method     ChildBattingQuery orderByAggression($order = Criteria::ASC) Order by the aggression column
 * @method     ChildBattingQuery orderByStraightstrength($order = Criteria::ASC) Order by the straightStrength column
 * @method     ChildBattingQuery orderBySquarestrength($order = Criteria::ASC) Order by the squareStrength column
 * @method     ChildBattingQuery orderByAverage($order = Criteria::ASC) Order by the average column
 *
 * @method     ChildBattingQuery groupById() Group by the id column
 * @method     ChildBattingQuery groupByPlayer() Group by the player column
 * @method     ChildBattingQuery groupByShotpower() Group by the shotPower column
 * @method     ChildBattingQuery groupByShotaccurecy() Group by the shotAccurecy column
 * @method     ChildBattingQuery groupByShotjudgement() Group by the shotJudgement column
 * @method     ChildBattingQuery groupByShotspeed() Group by the shotSpeed column
 * @method     ChildBattingQuery groupByShotmedium() Group by the shotMedium column
 * @method     ChildBattingQuery groupByShotspinner() Group by the shotSpinner column
 * @method     ChildBattingQuery groupByRunningspeed() Group by the runningSpeed column
 * @method     ChildBattingQuery groupByShortballstrength() Group by the shortBallStrength column
 * @method     ChildBattingQuery groupByLengthballstrength() Group by the lengthBallStrength column
 * @method     ChildBattingQuery groupByFullballstrength() Group by the fullBallStrength column
 * @method     ChildBattingQuery groupByTemprament() Group by the temprament column
 * @method     ChildBattingQuery groupByAggression() Group by the aggression column
 * @method     ChildBattingQuery groupByStraightstrength() Group by the straightStrength column
 * @method     ChildBattingQuery groupBySquarestrength() Group by the squareStrength column
 * @method     ChildBattingQuery groupByAverage() Group by the average column
 *
 * @method     ChildBattingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBattingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBattingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBattingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBattingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBattingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBatting findOne(ConnectionInterface $con = null) Return the first ChildBatting matching the query
 * @method     ChildBatting findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBatting matching the query, or a new ChildBatting object populated from the query conditions when no match is found
 *
 * @method     ChildBatting findOneById(int $id) Return the first ChildBatting filtered by the id column
 * @method     ChildBatting findOneByPlayer(int $player) Return the first ChildBatting filtered by the player column
 * @method     ChildBatting findOneByShotpower(int $shotPower) Return the first ChildBatting filtered by the shotPower column
 * @method     ChildBatting findOneByShotaccurecy(int $shotAccurecy) Return the first ChildBatting filtered by the shotAccurecy column
 * @method     ChildBatting findOneByShotjudgement(int $shotJudgement) Return the first ChildBatting filtered by the shotJudgement column
 * @method     ChildBatting findOneByShotspeed(int $shotSpeed) Return the first ChildBatting filtered by the shotSpeed column
 * @method     ChildBatting findOneByShotmedium(int $shotMedium) Return the first ChildBatting filtered by the shotMedium column
 * @method     ChildBatting findOneByShotspinner(int $shotSpinner) Return the first ChildBatting filtered by the shotSpinner column
 * @method     ChildBatting findOneByRunningspeed(int $runningSpeed) Return the first ChildBatting filtered by the runningSpeed column
 * @method     ChildBatting findOneByShortballstrength(int $shortBallStrength) Return the first ChildBatting filtered by the shortBallStrength column
 * @method     ChildBatting findOneByLengthballstrength(int $lengthBallStrength) Return the first ChildBatting filtered by the lengthBallStrength column
 * @method     ChildBatting findOneByFullballstrength(int $fullBallStrength) Return the first ChildBatting filtered by the fullBallStrength column
 * @method     ChildBatting findOneByTemprament(int $temprament) Return the first ChildBatting filtered by the temprament column
 * @method     ChildBatting findOneByAggression(int $aggression) Return the first ChildBatting filtered by the aggression column
 * @method     ChildBatting findOneByStraightstrength(int $straightStrength) Return the first ChildBatting filtered by the straightStrength column
 * @method     ChildBatting findOneBySquarestrength(int $squareStrength) Return the first ChildBatting filtered by the squareStrength column
 * @method     ChildBatting findOneByAverage(int $average) Return the first ChildBatting filtered by the average column *

 * @method     ChildBatting requirePk($key, ConnectionInterface $con = null) Return the ChildBatting by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOne(ConnectionInterface $con = null) Return the first ChildBatting matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBatting requireOneById(int $id) Return the first ChildBatting filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByPlayer(int $player) Return the first ChildBatting filtered by the player column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotpower(int $shotPower) Return the first ChildBatting filtered by the shotPower column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotaccurecy(int $shotAccurecy) Return the first ChildBatting filtered by the shotAccurecy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotjudgement(int $shotJudgement) Return the first ChildBatting filtered by the shotJudgement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotspeed(int $shotSpeed) Return the first ChildBatting filtered by the shotSpeed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotmedium(int $shotMedium) Return the first ChildBatting filtered by the shotMedium column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShotspinner(int $shotSpinner) Return the first ChildBatting filtered by the shotSpinner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByRunningspeed(int $runningSpeed) Return the first ChildBatting filtered by the runningSpeed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByShortballstrength(int $shortBallStrength) Return the first ChildBatting filtered by the shortBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByLengthballstrength(int $lengthBallStrength) Return the first ChildBatting filtered by the lengthBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByFullballstrength(int $fullBallStrength) Return the first ChildBatting filtered by the fullBallStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByTemprament(int $temprament) Return the first ChildBatting filtered by the temprament column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByAggression(int $aggression) Return the first ChildBatting filtered by the aggression column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByStraightstrength(int $straightStrength) Return the first ChildBatting filtered by the straightStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneBySquarestrength(int $squareStrength) Return the first ChildBatting filtered by the squareStrength column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBatting requireOneByAverage(int $average) Return the first ChildBatting filtered by the average column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBatting[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBatting objects based on current ModelCriteria
 * @method     ChildBatting[]|ObjectCollection findById(int $id) Return ChildBatting objects filtered by the id column
 * @method     ChildBatting[]|ObjectCollection findByPlayer(int $player) Return ChildBatting objects filtered by the player column
 * @method     ChildBatting[]|ObjectCollection findByShotpower(int $shotPower) Return ChildBatting objects filtered by the shotPower column
 * @method     ChildBatting[]|ObjectCollection findByShotaccurecy(int $shotAccurecy) Return ChildBatting objects filtered by the shotAccurecy column
 * @method     ChildBatting[]|ObjectCollection findByShotjudgement(int $shotJudgement) Return ChildBatting objects filtered by the shotJudgement column
 * @method     ChildBatting[]|ObjectCollection findByShotspeed(int $shotSpeed) Return ChildBatting objects filtered by the shotSpeed column
 * @method     ChildBatting[]|ObjectCollection findByShotmedium(int $shotMedium) Return ChildBatting objects filtered by the shotMedium column
 * @method     ChildBatting[]|ObjectCollection findByShotspinner(int $shotSpinner) Return ChildBatting objects filtered by the shotSpinner column
 * @method     ChildBatting[]|ObjectCollection findByRunningspeed(int $runningSpeed) Return ChildBatting objects filtered by the runningSpeed column
 * @method     ChildBatting[]|ObjectCollection findByShortballstrength(int $shortBallStrength) Return ChildBatting objects filtered by the shortBallStrength column
 * @method     ChildBatting[]|ObjectCollection findByLengthballstrength(int $lengthBallStrength) Return ChildBatting objects filtered by the lengthBallStrength column
 * @method     ChildBatting[]|ObjectCollection findByFullballstrength(int $fullBallStrength) Return ChildBatting objects filtered by the fullBallStrength column
 * @method     ChildBatting[]|ObjectCollection findByTemprament(int $temprament) Return ChildBatting objects filtered by the temprament column
 * @method     ChildBatting[]|ObjectCollection findByAggression(int $aggression) Return ChildBatting objects filtered by the aggression column
 * @method     ChildBatting[]|ObjectCollection findByStraightstrength(int $straightStrength) Return ChildBatting objects filtered by the straightStrength column
 * @method     ChildBatting[]|ObjectCollection findBySquarestrength(int $squareStrength) Return ChildBatting objects filtered by the squareStrength column
 * @method     ChildBatting[]|ObjectCollection findByAverage(int $average) Return ChildBatting objects filtered by the average column
 * @method     ChildBatting[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BattingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BattingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Batting', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBattingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBattingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBattingQuery) {
            return $criteria;
        }
        $query = new ChildBattingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBatting|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BattingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BattingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBatting A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, player, shotPower, shotAccurecy, shotJudgement, shotSpeed, shotMedium, shotSpinner, runningSpeed, shortBallStrength, lengthBallStrength, fullBallStrength, temprament, aggression, straightStrength, squareStrength, average FROM batting WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBatting $obj */
            $obj = new ChildBatting();
            $obj->hydrate($row);
            BattingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBatting|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BattingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BattingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the player column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayer(1234); // WHERE player = 1234
     * $query->filterByPlayer(array(12, 34)); // WHERE player IN (12, 34)
     * $query->filterByPlayer(array('min' => 12)); // WHERE player > 12
     * </code>
     *
     * @param     mixed $player The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByPlayer($player = null, $comparison = null)
    {
        if (is_array($player)) {
            $useMinMax = false;
            if (isset($player['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_PLAYER, $player['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($player['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_PLAYER, $player['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_PLAYER, $player, $comparison);
    }

    /**
     * Filter the query on the shotPower column
     *
     * Example usage:
     * <code>
     * $query->filterByShotpower(1234); // WHERE shotPower = 1234
     * $query->filterByShotpower(array(12, 34)); // WHERE shotPower IN (12, 34)
     * $query->filterByShotpower(array('min' => 12)); // WHERE shotPower > 12
     * </code>
     *
     * @param     mixed $shotpower The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotpower($shotpower = null, $comparison = null)
    {
        if (is_array($shotpower)) {
            $useMinMax = false;
            if (isset($shotpower['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTPOWER, $shotpower['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotpower['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTPOWER, $shotpower['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTPOWER, $shotpower, $comparison);
    }

    /**
     * Filter the query on the shotAccurecy column
     *
     * Example usage:
     * <code>
     * $query->filterByShotaccurecy(1234); // WHERE shotAccurecy = 1234
     * $query->filterByShotaccurecy(array(12, 34)); // WHERE shotAccurecy IN (12, 34)
     * $query->filterByShotaccurecy(array('min' => 12)); // WHERE shotAccurecy > 12
     * </code>
     *
     * @param     mixed $shotaccurecy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotaccurecy($shotaccurecy = null, $comparison = null)
    {
        if (is_array($shotaccurecy)) {
            $useMinMax = false;
            if (isset($shotaccurecy['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTACCURECY, $shotaccurecy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotaccurecy['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTACCURECY, $shotaccurecy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTACCURECY, $shotaccurecy, $comparison);
    }

    /**
     * Filter the query on the shotJudgement column
     *
     * Example usage:
     * <code>
     * $query->filterByShotjudgement(1234); // WHERE shotJudgement = 1234
     * $query->filterByShotjudgement(array(12, 34)); // WHERE shotJudgement IN (12, 34)
     * $query->filterByShotjudgement(array('min' => 12)); // WHERE shotJudgement > 12
     * </code>
     *
     * @param     mixed $shotjudgement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotjudgement($shotjudgement = null, $comparison = null)
    {
        if (is_array($shotjudgement)) {
            $useMinMax = false;
            if (isset($shotjudgement['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTJUDGEMENT, $shotjudgement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotjudgement['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTJUDGEMENT, $shotjudgement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTJUDGEMENT, $shotjudgement, $comparison);
    }

    /**
     * Filter the query on the shotSpeed column
     *
     * Example usage:
     * <code>
     * $query->filterByShotspeed(1234); // WHERE shotSpeed = 1234
     * $query->filterByShotspeed(array(12, 34)); // WHERE shotSpeed IN (12, 34)
     * $query->filterByShotspeed(array('min' => 12)); // WHERE shotSpeed > 12
     * </code>
     *
     * @param     mixed $shotspeed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotspeed($shotspeed = null, $comparison = null)
    {
        if (is_array($shotspeed)) {
            $useMinMax = false;
            if (isset($shotspeed['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTSPEED, $shotspeed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotspeed['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTSPEED, $shotspeed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTSPEED, $shotspeed, $comparison);
    }

    /**
     * Filter the query on the shotMedium column
     *
     * Example usage:
     * <code>
     * $query->filterByShotmedium(1234); // WHERE shotMedium = 1234
     * $query->filterByShotmedium(array(12, 34)); // WHERE shotMedium IN (12, 34)
     * $query->filterByShotmedium(array('min' => 12)); // WHERE shotMedium > 12
     * </code>
     *
     * @param     mixed $shotmedium The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotmedium($shotmedium = null, $comparison = null)
    {
        if (is_array($shotmedium)) {
            $useMinMax = false;
            if (isset($shotmedium['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTMEDIUM, $shotmedium['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotmedium['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTMEDIUM, $shotmedium['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTMEDIUM, $shotmedium, $comparison);
    }

    /**
     * Filter the query on the shotSpinner column
     *
     * Example usage:
     * <code>
     * $query->filterByShotspinner(1234); // WHERE shotSpinner = 1234
     * $query->filterByShotspinner(array(12, 34)); // WHERE shotSpinner IN (12, 34)
     * $query->filterByShotspinner(array('min' => 12)); // WHERE shotSpinner > 12
     * </code>
     *
     * @param     mixed $shotspinner The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShotspinner($shotspinner = null, $comparison = null)
    {
        if (is_array($shotspinner)) {
            $useMinMax = false;
            if (isset($shotspinner['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTSPINNER, $shotspinner['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shotspinner['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHOTSPINNER, $shotspinner['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHOTSPINNER, $shotspinner, $comparison);
    }

    /**
     * Filter the query on the runningSpeed column
     *
     * Example usage:
     * <code>
     * $query->filterByRunningspeed(1234); // WHERE runningSpeed = 1234
     * $query->filterByRunningspeed(array(12, 34)); // WHERE runningSpeed IN (12, 34)
     * $query->filterByRunningspeed(array('min' => 12)); // WHERE runningSpeed > 12
     * </code>
     *
     * @param     mixed $runningspeed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByRunningspeed($runningspeed = null, $comparison = null)
    {
        if (is_array($runningspeed)) {
            $useMinMax = false;
            if (isset($runningspeed['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_RUNNINGSPEED, $runningspeed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($runningspeed['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_RUNNINGSPEED, $runningspeed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_RUNNINGSPEED, $runningspeed, $comparison);
    }

    /**
     * Filter the query on the shortBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByShortballstrength(1234); // WHERE shortBallStrength = 1234
     * $query->filterByShortballstrength(array(12, 34)); // WHERE shortBallStrength IN (12, 34)
     * $query->filterByShortballstrength(array('min' => 12)); // WHERE shortBallStrength > 12
     * </code>
     *
     * @param     mixed $shortballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByShortballstrength($shortballstrength = null, $comparison = null)
    {
        if (is_array($shortballstrength)) {
            $useMinMax = false;
            if (isset($shortballstrength['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shortballstrength['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SHORTBALLSTRENGTH, $shortballstrength, $comparison);
    }

    /**
     * Filter the query on the lengthBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByLengthballstrength(1234); // WHERE lengthBallStrength = 1234
     * $query->filterByLengthballstrength(array(12, 34)); // WHERE lengthBallStrength IN (12, 34)
     * $query->filterByLengthballstrength(array('min' => 12)); // WHERE lengthBallStrength > 12
     * </code>
     *
     * @param     mixed $lengthballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByLengthballstrength($lengthballstrength = null, $comparison = null)
    {
        if (is_array($lengthballstrength)) {
            $useMinMax = false;
            if (isset($lengthballstrength['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lengthballstrength['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_LENGTHBALLSTRENGTH, $lengthballstrength, $comparison);
    }

    /**
     * Filter the query on the fullBallStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByFullballstrength(1234); // WHERE fullBallStrength = 1234
     * $query->filterByFullballstrength(array(12, 34)); // WHERE fullBallStrength IN (12, 34)
     * $query->filterByFullballstrength(array('min' => 12)); // WHERE fullBallStrength > 12
     * </code>
     *
     * @param     mixed $fullballstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByFullballstrength($fullballstrength = null, $comparison = null)
    {
        if (is_array($fullballstrength)) {
            $useMinMax = false;
            if (isset($fullballstrength['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fullballstrength['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_FULLBALLSTRENGTH, $fullballstrength, $comparison);
    }

    /**
     * Filter the query on the temprament column
     *
     * Example usage:
     * <code>
     * $query->filterByTemprament(1234); // WHERE temprament = 1234
     * $query->filterByTemprament(array(12, 34)); // WHERE temprament IN (12, 34)
     * $query->filterByTemprament(array('min' => 12)); // WHERE temprament > 12
     * </code>
     *
     * @param     mixed $temprament The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByTemprament($temprament = null, $comparison = null)
    {
        if (is_array($temprament)) {
            $useMinMax = false;
            if (isset($temprament['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_TEMPRAMENT, $temprament['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($temprament['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_TEMPRAMENT, $temprament['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_TEMPRAMENT, $temprament, $comparison);
    }

    /**
     * Filter the query on the aggression column
     *
     * Example usage:
     * <code>
     * $query->filterByAggression(1234); // WHERE aggression = 1234
     * $query->filterByAggression(array(12, 34)); // WHERE aggression IN (12, 34)
     * $query->filterByAggression(array('min' => 12)); // WHERE aggression > 12
     * </code>
     *
     * @param     mixed $aggression The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByAggression($aggression = null, $comparison = null)
    {
        if (is_array($aggression)) {
            $useMinMax = false;
            if (isset($aggression['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_AGGRESSION, $aggression['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aggression['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_AGGRESSION, $aggression['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_AGGRESSION, $aggression, $comparison);
    }

    /**
     * Filter the query on the straightStrength column
     *
     * Example usage:
     * <code>
     * $query->filterByStraightstrength(1234); // WHERE straightStrength = 1234
     * $query->filterByStraightstrength(array(12, 34)); // WHERE straightStrength IN (12, 34)
     * $query->filterByStraightstrength(array('min' => 12)); // WHERE straightStrength > 12
     * </code>
     *
     * @param     mixed $straightstrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByStraightstrength($straightstrength = null, $comparison = null)
    {
        if (is_array($straightstrength)) {
            $useMinMax = false;
            if (isset($straightstrength['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_STRAIGHTSTRENGTH, $straightstrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($straightstrength['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_STRAIGHTSTRENGTH, $straightstrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_STRAIGHTSTRENGTH, $straightstrength, $comparison);
    }

    /**
     * Filter the query on the squareStrength column
     *
     * Example usage:
     * <code>
     * $query->filterBySquarestrength(1234); // WHERE squareStrength = 1234
     * $query->filterBySquarestrength(array(12, 34)); // WHERE squareStrength IN (12, 34)
     * $query->filterBySquarestrength(array('min' => 12)); // WHERE squareStrength > 12
     * </code>
     *
     * @param     mixed $squarestrength The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterBySquarestrength($squarestrength = null, $comparison = null)
    {
        if (is_array($squarestrength)) {
            $useMinMax = false;
            if (isset($squarestrength['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_SQUARESTRENGTH, $squarestrength['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($squarestrength['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_SQUARESTRENGTH, $squarestrength['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_SQUARESTRENGTH, $squarestrength, $comparison);
    }

    /**
     * Filter the query on the average column
     *
     * Example usage:
     * <code>
     * $query->filterByAverage(1234); // WHERE average = 1234
     * $query->filterByAverage(array(12, 34)); // WHERE average IN (12, 34)
     * $query->filterByAverage(array('min' => 12)); // WHERE average > 12
     * </code>
     *
     * @param     mixed $average The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function filterByAverage($average = null, $comparison = null)
    {
        if (is_array($average)) {
            $useMinMax = false;
            if (isset($average['min'])) {
                $this->addUsingAlias(BattingTableMap::COL_AVERAGE, $average['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($average['max'])) {
                $this->addUsingAlias(BattingTableMap::COL_AVERAGE, $average['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BattingTableMap::COL_AVERAGE, $average, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBatting $batting Object to remove from the list of results
     *
     * @return $this|ChildBattingQuery The current query, for fluid interface
     */
    public function prune($batting = null)
    {
        if ($batting) {
            $this->addUsingAlias(BattingTableMap::COL_ID, $batting->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the batting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BattingTableMap::clearInstancePool();
            BattingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BattingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BattingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BattingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BattingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BattingQuery
