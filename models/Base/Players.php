<?php

namespace Base;

use \PlayersQuery as ChildPlayersQuery;
use \Exception;
use \PDO;
use Map\PlayersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'players' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Players implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PlayersTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the firstname field.
     *
     * @var        string
     */
    protected $firstname;

    /**
     * The value for the lastname field.
     *
     * @var        string
     */
    protected $lastname;

    /**
     * The value for the jerseynumber field.
     *
     * @var        string
     */
    protected $jerseynumber;

    /**
     * The value for the jerseyname field.
     *
     * @var        string
     */
    protected $jerseyname;

    /**
     * The value for the battinghand field.
     *
     * @var        string
     */
    protected $battinghand;

    /**
     * The value for the bowlinghand field.
     *
     * @var        string
     */
    protected $bowlinghand;

    /**
     * The value for the fieldinghand field.
     *
     * @var        string
     */
    protected $fieldinghand;

    /**
     * The value for the modelname field.
     *
     * @var        string
     */
    protected $modelname;

    /**
     * The value for the modeltexture field.
     *
     * @var        string
     */
    protected $modeltexture;

    /**
     * The value for the primaryrole field.
     *
     * Note: this column has a database default value of: 'allRounder'
     * @var        string
     */
    protected $primaryrole;

    /**
     * The value for the battingorder field.
     *
     * @var        int
     */
    protected $battingorder;

    /**
     * The value for the race field.
     *
     * Note: this column has a database default value of: 'indian'
     * @var        string
     */
    protected $race;

    /**
     * The value for the sourceurl field.
     *
     * @var        string
     */
    protected $sourceurl;

    /**
     * The value for the transfervalue field.
     *
     * @var        double
     */
    protected $transfervalue;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->primaryrole = 'allRounder';
        $this->race = 'indian';
    }

    /**
     * Initializes internal state of Base\Players object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Players</code> instance.  If
     * <code>obj</code> is an instance of <code>Players</code>, delegates to
     * <code>equals(Players)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Players The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [firstname] column value.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Get the [lastname] column value.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get the [jerseynumber] column value.
     *
     * @return string
     */
    public function getJerseynumber()
    {
        return $this->jerseynumber;
    }

    /**
     * Get the [jerseyname] column value.
     *
     * @return string
     */
    public function getJerseyname()
    {
        return $this->jerseyname;
    }

    /**
     * Get the [battinghand] column value.
     *
     * @return string
     */
    public function getBattinghand()
    {
        return $this->battinghand;
    }

    /**
     * Get the [bowlinghand] column value.
     *
     * @return string
     */
    public function getBowlinghand()
    {
        return $this->bowlinghand;
    }

    /**
     * Get the [fieldinghand] column value.
     *
     * @return string
     */
    public function getFieldinghand()
    {
        return $this->fieldinghand;
    }

    /**
     * Get the [modelname] column value.
     *
     * @return string
     */
    public function getModelname()
    {
        return $this->modelname;
    }

    /**
     * Get the [modeltexture] column value.
     *
     * @return string
     */
    public function getModeltexture()
    {
        return $this->modeltexture;
    }

    /**
     * Get the [primaryrole] column value.
     *
     * @return string
     */
    public function getPrimaryrole()
    {
        return $this->primaryrole;
    }

    /**
     * Get the [battingorder] column value.
     *
     * @return int
     */
    public function getBattingorder()
    {
        return $this->battingorder;
    }

    /**
     * Get the [race] column value.
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Get the [sourceurl] column value.
     *
     * @return string
     */
    public function getSourceurl()
    {
        return $this->sourceurl;
    }

    /**
     * Get the [transfervalue] column value.
     *
     * @return double
     */
    public function getTransfervalue()
    {
        return $this->transfervalue;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PlayersTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [firstname] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->firstname !== $v) {
            $this->firstname = $v;
            $this->modifiedColumns[PlayersTableMap::COL_FIRSTNAME] = true;
        }

        return $this;
    } // setFirstname()

    /**
     * Set the value of [lastname] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lastname !== $v) {
            $this->lastname = $v;
            $this->modifiedColumns[PlayersTableMap::COL_LASTNAME] = true;
        }

        return $this;
    } // setLastname()

    /**
     * Set the value of [jerseynumber] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setJerseynumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->jerseynumber !== $v) {
            $this->jerseynumber = $v;
            $this->modifiedColumns[PlayersTableMap::COL_JERSEYNUMBER] = true;
        }

        return $this;
    } // setJerseynumber()

    /**
     * Set the value of [jerseyname] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setJerseyname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->jerseyname !== $v) {
            $this->jerseyname = $v;
            $this->modifiedColumns[PlayersTableMap::COL_JERSEYNAME] = true;
        }

        return $this;
    } // setJerseyname()

    /**
     * Set the value of [battinghand] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setBattinghand($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->battinghand !== $v) {
            $this->battinghand = $v;
            $this->modifiedColumns[PlayersTableMap::COL_BATTINGHAND] = true;
        }

        return $this;
    } // setBattinghand()

    /**
     * Set the value of [bowlinghand] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setBowlinghand($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bowlinghand !== $v) {
            $this->bowlinghand = $v;
            $this->modifiedColumns[PlayersTableMap::COL_BOWLINGHAND] = true;
        }

        return $this;
    } // setBowlinghand()

    /**
     * Set the value of [fieldinghand] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setFieldinghand($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fieldinghand !== $v) {
            $this->fieldinghand = $v;
            $this->modifiedColumns[PlayersTableMap::COL_FIELDINGHAND] = true;
        }

        return $this;
    } // setFieldinghand()

    /**
     * Set the value of [modelname] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setModelname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modelname !== $v) {
            $this->modelname = $v;
            $this->modifiedColumns[PlayersTableMap::COL_MODELNAME] = true;
        }

        return $this;
    } // setModelname()

    /**
     * Set the value of [modeltexture] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setModeltexture($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modeltexture !== $v) {
            $this->modeltexture = $v;
            $this->modifiedColumns[PlayersTableMap::COL_MODELTEXTURE] = true;
        }

        return $this;
    } // setModeltexture()

    /**
     * Set the value of [primaryrole] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setPrimaryrole($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->primaryrole !== $v) {
            $this->primaryrole = $v;
            $this->modifiedColumns[PlayersTableMap::COL_PRIMARYROLE] = true;
        }

        return $this;
    } // setPrimaryrole()

    /**
     * Set the value of [battingorder] column.
     *
     * @param int $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setBattingorder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->battingorder !== $v) {
            $this->battingorder = $v;
            $this->modifiedColumns[PlayersTableMap::COL_BATTINGORDER] = true;
        }

        return $this;
    } // setBattingorder()

    /**
     * Set the value of [race] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setRace($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->race !== $v) {
            $this->race = $v;
            $this->modifiedColumns[PlayersTableMap::COL_RACE] = true;
        }

        return $this;
    } // setRace()

    /**
     * Set the value of [sourceurl] column.
     *
     * @param string $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setSourceurl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sourceurl !== $v) {
            $this->sourceurl = $v;
            $this->modifiedColumns[PlayersTableMap::COL_SOURCEURL] = true;
        }

        return $this;
    } // setSourceurl()

    /**
     * Set the value of [transfervalue] column.
     *
     * @param double $v new value
     * @return $this|\Players The current object (for fluent API support)
     */
    public function setTransfervalue($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->transfervalue !== $v) {
            $this->transfervalue = $v;
            $this->modifiedColumns[PlayersTableMap::COL_TRANSFERVALUE] = true;
        }

        return $this;
    } // setTransfervalue()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->primaryrole !== 'allRounder') {
                return false;
            }

            if ($this->race !== 'indian') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PlayersTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PlayersTableMap::translateFieldName('Firstname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->firstname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PlayersTableMap::translateFieldName('Lastname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lastname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PlayersTableMap::translateFieldName('Jerseynumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jerseynumber = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PlayersTableMap::translateFieldName('Jerseyname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jerseyname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PlayersTableMap::translateFieldName('Battinghand', TableMap::TYPE_PHPNAME, $indexType)];
            $this->battinghand = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PlayersTableMap::translateFieldName('Bowlinghand', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bowlinghand = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PlayersTableMap::translateFieldName('Fieldinghand', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fieldinghand = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PlayersTableMap::translateFieldName('Modelname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modelname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PlayersTableMap::translateFieldName('Modeltexture', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modeltexture = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PlayersTableMap::translateFieldName('Primaryrole', TableMap::TYPE_PHPNAME, $indexType)];
            $this->primaryrole = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PlayersTableMap::translateFieldName('Battingorder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->battingorder = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PlayersTableMap::translateFieldName('Race', TableMap::TYPE_PHPNAME, $indexType)];
            $this->race = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PlayersTableMap::translateFieldName('Sourceurl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sourceurl = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PlayersTableMap::translateFieldName('Transfervalue', TableMap::TYPE_PHPNAME, $indexType)];
            $this->transfervalue = (null !== $col) ? (double) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = PlayersTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Players'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlayersTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPlayersQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Players::setDeleted()
     * @see Players::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPlayersQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlayersTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PlayersTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PlayersTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PlayersTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PlayersTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'firstName';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'lastName';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_JERSEYNUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'jerseyNumber';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_JERSEYNAME)) {
            $modifiedColumns[':p' . $index++]  = 'jerseyName';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BATTINGHAND)) {
            $modifiedColumns[':p' . $index++]  = 'battingHand';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BOWLINGHAND)) {
            $modifiedColumns[':p' . $index++]  = 'bowlingHand';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_FIELDINGHAND)) {
            $modifiedColumns[':p' . $index++]  = 'fieldingHand';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_MODELNAME)) {
            $modifiedColumns[':p' . $index++]  = 'modelName';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_MODELTEXTURE)) {
            $modifiedColumns[':p' . $index++]  = 'modelTexture';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_PRIMARYROLE)) {
            $modifiedColumns[':p' . $index++]  = 'primaryRole';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BATTINGORDER)) {
            $modifiedColumns[':p' . $index++]  = 'battingOrder';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_RACE)) {
            $modifiedColumns[':p' . $index++]  = 'race';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_SOURCEURL)) {
            $modifiedColumns[':p' . $index++]  = 'sourceUrl';
        }
        if ($this->isColumnModified(PlayersTableMap::COL_TRANSFERVALUE)) {
            $modifiedColumns[':p' . $index++]  = 'transferValue';
        }

        $sql = sprintf(
            'INSERT INTO players (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'firstName':
                        $stmt->bindValue($identifier, $this->firstname, PDO::PARAM_STR);
                        break;
                    case 'lastName':
                        $stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
                        break;
                    case 'jerseyNumber':
                        $stmt->bindValue($identifier, $this->jerseynumber, PDO::PARAM_STR);
                        break;
                    case 'jerseyName':
                        $stmt->bindValue($identifier, $this->jerseyname, PDO::PARAM_STR);
                        break;
                    case 'battingHand':
                        $stmt->bindValue($identifier, $this->battinghand, PDO::PARAM_STR);
                        break;
                    case 'bowlingHand':
                        $stmt->bindValue($identifier, $this->bowlinghand, PDO::PARAM_STR);
                        break;
                    case 'fieldingHand':
                        $stmt->bindValue($identifier, $this->fieldinghand, PDO::PARAM_STR);
                        break;
                    case 'modelName':
                        $stmt->bindValue($identifier, $this->modelname, PDO::PARAM_STR);
                        break;
                    case 'modelTexture':
                        $stmt->bindValue($identifier, $this->modeltexture, PDO::PARAM_STR);
                        break;
                    case 'primaryRole':
                        $stmt->bindValue($identifier, $this->primaryrole, PDO::PARAM_STR);
                        break;
                    case 'battingOrder':
                        $stmt->bindValue($identifier, $this->battingorder, PDO::PARAM_INT);
                        break;
                    case 'race':
                        $stmt->bindValue($identifier, $this->race, PDO::PARAM_STR);
                        break;
                    case 'sourceUrl':
                        $stmt->bindValue($identifier, $this->sourceurl, PDO::PARAM_STR);
                        break;
                    case 'transferValue':
                        $stmt->bindValue($identifier, $this->transfervalue, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PlayersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getFirstname();
                break;
            case 2:
                return $this->getLastname();
                break;
            case 3:
                return $this->getJerseynumber();
                break;
            case 4:
                return $this->getJerseyname();
                break;
            case 5:
                return $this->getBattinghand();
                break;
            case 6:
                return $this->getBowlinghand();
                break;
            case 7:
                return $this->getFieldinghand();
                break;
            case 8:
                return $this->getModelname();
                break;
            case 9:
                return $this->getModeltexture();
                break;
            case 10:
                return $this->getPrimaryrole();
                break;
            case 11:
                return $this->getBattingorder();
                break;
            case 12:
                return $this->getRace();
                break;
            case 13:
                return $this->getSourceurl();
                break;
            case 14:
                return $this->getTransfervalue();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Players'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Players'][$this->hashCode()] = true;
        $keys = PlayersTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getFirstname(),
            $keys[2] => $this->getLastname(),
            $keys[3] => $this->getJerseynumber(),
            $keys[4] => $this->getJerseyname(),
            $keys[5] => $this->getBattinghand(),
            $keys[6] => $this->getBowlinghand(),
            $keys[7] => $this->getFieldinghand(),
            $keys[8] => $this->getModelname(),
            $keys[9] => $this->getModeltexture(),
            $keys[10] => $this->getPrimaryrole(),
            $keys[11] => $this->getBattingorder(),
            $keys[12] => $this->getRace(),
            $keys[13] => $this->getSourceurl(),
            $keys[14] => $this->getTransfervalue(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Players
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PlayersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Players
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setFirstname($value);
                break;
            case 2:
                $this->setLastname($value);
                break;
            case 3:
                $this->setJerseynumber($value);
                break;
            case 4:
                $this->setJerseyname($value);
                break;
            case 5:
                $this->setBattinghand($value);
                break;
            case 6:
                $this->setBowlinghand($value);
                break;
            case 7:
                $this->setFieldinghand($value);
                break;
            case 8:
                $this->setModelname($value);
                break;
            case 9:
                $this->setModeltexture($value);
                break;
            case 10:
                $this->setPrimaryrole($value);
                break;
            case 11:
                $this->setBattingorder($value);
                break;
            case 12:
                $this->setRace($value);
                break;
            case 13:
                $this->setSourceurl($value);
                break;
            case 14:
                $this->setTransfervalue($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PlayersTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setFirstname($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLastname($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setJerseynumber($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setJerseyname($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setBattinghand($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBowlinghand($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setFieldinghand($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setModelname($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setModeltexture($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPrimaryrole($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setBattingorder($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setRace($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setSourceurl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTransfervalue($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Players The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PlayersTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PlayersTableMap::COL_ID)) {
            $criteria->add(PlayersTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_FIRSTNAME)) {
            $criteria->add(PlayersTableMap::COL_FIRSTNAME, $this->firstname);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_LASTNAME)) {
            $criteria->add(PlayersTableMap::COL_LASTNAME, $this->lastname);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_JERSEYNUMBER)) {
            $criteria->add(PlayersTableMap::COL_JERSEYNUMBER, $this->jerseynumber);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_JERSEYNAME)) {
            $criteria->add(PlayersTableMap::COL_JERSEYNAME, $this->jerseyname);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BATTINGHAND)) {
            $criteria->add(PlayersTableMap::COL_BATTINGHAND, $this->battinghand);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BOWLINGHAND)) {
            $criteria->add(PlayersTableMap::COL_BOWLINGHAND, $this->bowlinghand);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_FIELDINGHAND)) {
            $criteria->add(PlayersTableMap::COL_FIELDINGHAND, $this->fieldinghand);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_MODELNAME)) {
            $criteria->add(PlayersTableMap::COL_MODELNAME, $this->modelname);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_MODELTEXTURE)) {
            $criteria->add(PlayersTableMap::COL_MODELTEXTURE, $this->modeltexture);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_PRIMARYROLE)) {
            $criteria->add(PlayersTableMap::COL_PRIMARYROLE, $this->primaryrole);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_BATTINGORDER)) {
            $criteria->add(PlayersTableMap::COL_BATTINGORDER, $this->battingorder);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_RACE)) {
            $criteria->add(PlayersTableMap::COL_RACE, $this->race);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_SOURCEURL)) {
            $criteria->add(PlayersTableMap::COL_SOURCEURL, $this->sourceurl);
        }
        if ($this->isColumnModified(PlayersTableMap::COL_TRANSFERVALUE)) {
            $criteria->add(PlayersTableMap::COL_TRANSFERVALUE, $this->transfervalue);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPlayersQuery::create();
        $criteria->add(PlayersTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Players (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setFirstname($this->getFirstname());
        $copyObj->setLastname($this->getLastname());
        $copyObj->setJerseynumber($this->getJerseynumber());
        $copyObj->setJerseyname($this->getJerseyname());
        $copyObj->setBattinghand($this->getBattinghand());
        $copyObj->setBowlinghand($this->getBowlinghand());
        $copyObj->setFieldinghand($this->getFieldinghand());
        $copyObj->setModelname($this->getModelname());
        $copyObj->setModeltexture($this->getModeltexture());
        $copyObj->setPrimaryrole($this->getPrimaryrole());
        $copyObj->setBattingorder($this->getBattingorder());
        $copyObj->setRace($this->getRace());
        $copyObj->setSourceurl($this->getSourceurl());
        $copyObj->setTransfervalue($this->getTransfervalue());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Players Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->firstname = null;
        $this->lastname = null;
        $this->jerseynumber = null;
        $this->jerseyname = null;
        $this->battinghand = null;
        $this->bowlinghand = null;
        $this->fieldinghand = null;
        $this->modelname = null;
        $this->modeltexture = null;
        $this->primaryrole = null;
        $this->battingorder = null;
        $this->race = null;
        $this->sourceurl = null;
        $this->transfervalue = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PlayersTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
