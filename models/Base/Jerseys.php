<?php

namespace Base;

use \JerseysQuery as ChildJerseysQuery;
use \Exception;
use \PDO;
use Map\JerseysTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'jerseys' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Jerseys implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\JerseysTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the team_id field.
     *
     * @var        int
     */
    protected $team_id;

    /**
     * The value for the shirt_color field.
     *
     * @var        string
     */
    protected $shirt_color;

    /**
     * The value for the shirt_pattern_color field.
     *
     * @var        string
     */
    protected $shirt_pattern_color;

    /**
     * The value for the shirt_sleve_color field.
     *
     * @var        string
     */
    protected $shirt_sleve_color;

    /**
     * The value for the pant_color field.
     *
     * @var        string
     */
    protected $pant_color;

    /**
     * The value for the cap_color field.
     *
     * @var        string
     */
    protected $cap_color;

    /**
     * The value for the studio_logo_color field.
     *
     * @var        string
     */
    protected $studio_logo_color;

    /**
     * The value for the number_color field.
     *
     * @var        string
     */
    protected $number_color;

    /**
     * The value for the pant_pattern_color field.
     *
     * @var        string
     */
    protected $pant_pattern_color;

    /**
     * The value for the shirt_line_color field.
     *
     * @var        string
     */
    protected $shirt_line_color;

    /**
     * The value for the pant_texture field.
     *
     * @var        string
     */
    protected $pant_texture;

    /**
     * The value for the shoe_color field.
     *
     * @var        string
     */
    protected $shoe_color;

    /**
     * The value for the shoe_sole_color field.
     *
     * @var        string
     */
    protected $shoe_sole_color;

    /**
     * The value for the socks_color field.
     *
     * @var        string
     */
    protected $socks_color;

    /**
     * The value for the shirt_texture field.
     *
     * @var        string
     */
    protected $shirt_texture;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\Jerseys object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Jerseys</code> instance.  If
     * <code>obj</code> is an instance of <code>Jerseys</code>, delegates to
     * <code>equals(Jerseys)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Jerseys The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [team_id] column value.
     *
     * @return int
     */
    public function getTeamId()
    {
        return $this->team_id;
    }

    /**
     * Get the [shirt_color] column value.
     *
     * @return string
     */
    public function getShirtColor()
    {
        return $this->shirt_color;
    }

    /**
     * Get the [shirt_pattern_color] column value.
     *
     * @return string
     */
    public function getShirtPatternColor()
    {
        return $this->shirt_pattern_color;
    }

    /**
     * Get the [shirt_sleve_color] column value.
     *
     * @return string
     */
    public function getShirtSleveColor()
    {
        return $this->shirt_sleve_color;
    }

    /**
     * Get the [pant_color] column value.
     *
     * @return string
     */
    public function getPantColor()
    {
        return $this->pant_color;
    }

    /**
     * Get the [cap_color] column value.
     *
     * @return string
     */
    public function getCapColor()
    {
        return $this->cap_color;
    }

    /**
     * Get the [studio_logo_color] column value.
     *
     * @return string
     */
    public function getStudioLogoColor()
    {
        return $this->studio_logo_color;
    }

    /**
     * Get the [number_color] column value.
     *
     * @return string
     */
    public function getNumberColor()
    {
        return $this->number_color;
    }

    /**
     * Get the [pant_pattern_color] column value.
     *
     * @return string
     */
    public function getPantPatternColor()
    {
        return $this->pant_pattern_color;
    }

    /**
     * Get the [shirt_line_color] column value.
     *
     * @return string
     */
    public function getShirtLineColor()
    {
        return $this->shirt_line_color;
    }

    /**
     * Get the [pant_texture] column value.
     *
     * @return string
     */
    public function getPantTexture()
    {
        return $this->pant_texture;
    }

    /**
     * Get the [shoe_color] column value.
     *
     * @return string
     */
    public function getShoeColor()
    {
        return $this->shoe_color;
    }

    /**
     * Get the [shoe_sole_color] column value.
     *
     * @return string
     */
    public function getShoeSoleColor()
    {
        return $this->shoe_sole_color;
    }

    /**
     * Get the [socks_color] column value.
     *
     * @return string
     */
    public function getSocksColor()
    {
        return $this->socks_color;
    }

    /**
     * Get the [shirt_texture] column value.
     *
     * @return string
     */
    public function getShirtTexture()
    {
        return $this->shirt_texture;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[JerseysTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [team_id] column.
     *
     * @param int $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setTeamId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->team_id !== $v) {
            $this->team_id = $v;
            $this->modifiedColumns[JerseysTableMap::COL_TEAM_ID] = true;
        }

        return $this;
    } // setTeamId()

    /**
     * Set the value of [shirt_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShirtColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shirt_color !== $v) {
            $this->shirt_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHIRT_COLOR] = true;
        }

        return $this;
    } // setShirtColor()

    /**
     * Set the value of [shirt_pattern_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShirtPatternColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shirt_pattern_color !== $v) {
            $this->shirt_pattern_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHIRT_PATTERN_COLOR] = true;
        }

        return $this;
    } // setShirtPatternColor()

    /**
     * Set the value of [shirt_sleve_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShirtSleveColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shirt_sleve_color !== $v) {
            $this->shirt_sleve_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHIRT_SLEVE_COLOR] = true;
        }

        return $this;
    } // setShirtSleveColor()

    /**
     * Set the value of [pant_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setPantColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pant_color !== $v) {
            $this->pant_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_PANT_COLOR] = true;
        }

        return $this;
    } // setPantColor()

    /**
     * Set the value of [cap_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setCapColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cap_color !== $v) {
            $this->cap_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_CAP_COLOR] = true;
        }

        return $this;
    } // setCapColor()

    /**
     * Set the value of [studio_logo_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setStudioLogoColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->studio_logo_color !== $v) {
            $this->studio_logo_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_STUDIO_LOGO_COLOR] = true;
        }

        return $this;
    } // setStudioLogoColor()

    /**
     * Set the value of [number_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setNumberColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->number_color !== $v) {
            $this->number_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_NUMBER_COLOR] = true;
        }

        return $this;
    } // setNumberColor()

    /**
     * Set the value of [pant_pattern_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setPantPatternColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pant_pattern_color !== $v) {
            $this->pant_pattern_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_PANT_PATTERN_COLOR] = true;
        }

        return $this;
    } // setPantPatternColor()

    /**
     * Set the value of [shirt_line_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShirtLineColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shirt_line_color !== $v) {
            $this->shirt_line_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHIRT_LINE_COLOR] = true;
        }

        return $this;
    } // setShirtLineColor()

    /**
     * Set the value of [pant_texture] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setPantTexture($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pant_texture !== $v) {
            $this->pant_texture = $v;
            $this->modifiedColumns[JerseysTableMap::COL_PANT_TEXTURE] = true;
        }

        return $this;
    } // setPantTexture()

    /**
     * Set the value of [shoe_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShoeColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shoe_color !== $v) {
            $this->shoe_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHOE_COLOR] = true;
        }

        return $this;
    } // setShoeColor()

    /**
     * Set the value of [shoe_sole_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShoeSoleColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shoe_sole_color !== $v) {
            $this->shoe_sole_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHOE_SOLE_COLOR] = true;
        }

        return $this;
    } // setShoeSoleColor()

    /**
     * Set the value of [socks_color] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setSocksColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->socks_color !== $v) {
            $this->socks_color = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SOCKS_COLOR] = true;
        }

        return $this;
    } // setSocksColor()

    /**
     * Set the value of [shirt_texture] column.
     *
     * @param string $v new value
     * @return $this|\Jerseys The current object (for fluent API support)
     */
    public function setShirtTexture($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shirt_texture !== $v) {
            $this->shirt_texture = $v;
            $this->modifiedColumns[JerseysTableMap::COL_SHIRT_TEXTURE] = true;
        }

        return $this;
    } // setShirtTexture()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : JerseysTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : JerseysTableMap::translateFieldName('TeamId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->team_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : JerseysTableMap::translateFieldName('ShirtColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shirt_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : JerseysTableMap::translateFieldName('ShirtPatternColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shirt_pattern_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : JerseysTableMap::translateFieldName('ShirtSleveColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shirt_sleve_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : JerseysTableMap::translateFieldName('PantColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pant_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : JerseysTableMap::translateFieldName('CapColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cap_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : JerseysTableMap::translateFieldName('StudioLogoColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->studio_logo_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : JerseysTableMap::translateFieldName('NumberColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->number_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : JerseysTableMap::translateFieldName('PantPatternColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pant_pattern_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : JerseysTableMap::translateFieldName('ShirtLineColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shirt_line_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : JerseysTableMap::translateFieldName('PantTexture', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pant_texture = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : JerseysTableMap::translateFieldName('ShoeColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shoe_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : JerseysTableMap::translateFieldName('ShoeSoleColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shoe_sole_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : JerseysTableMap::translateFieldName('SocksColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->socks_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : JerseysTableMap::translateFieldName('ShirtTexture', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shirt_texture = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = JerseysTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Jerseys'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(JerseysTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildJerseysQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Jerseys::setDeleted()
     * @see Jerseys::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildJerseysQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(JerseysTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                JerseysTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[JerseysTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . JerseysTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(JerseysTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_TEAM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'team_id';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shirt_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_PATTERN_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shirt_pattern_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_SLEVE_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shirt_sleve_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'pant_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_CAP_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'cap_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_STUDIO_LOGO_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'studio_logo_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_NUMBER_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'number_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_PATTERN_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'pant_pattern_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_LINE_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shirt_line_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_TEXTURE)) {
            $modifiedColumns[':p' . $index++]  = 'pant_texture';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHOE_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shoe_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHOE_SOLE_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'shoe_sole_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SOCKS_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'socks_color';
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_TEXTURE)) {
            $modifiedColumns[':p' . $index++]  = 'shirt_texture';
        }

        $sql = sprintf(
            'INSERT INTO jerseys (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'team_id':
                        $stmt->bindValue($identifier, $this->team_id, PDO::PARAM_INT);
                        break;
                    case 'shirt_color':
                        $stmt->bindValue($identifier, $this->shirt_color, PDO::PARAM_STR);
                        break;
                    case 'shirt_pattern_color':
                        $stmt->bindValue($identifier, $this->shirt_pattern_color, PDO::PARAM_STR);
                        break;
                    case 'shirt_sleve_color':
                        $stmt->bindValue($identifier, $this->shirt_sleve_color, PDO::PARAM_STR);
                        break;
                    case 'pant_color':
                        $stmt->bindValue($identifier, $this->pant_color, PDO::PARAM_STR);
                        break;
                    case 'cap_color':
                        $stmt->bindValue($identifier, $this->cap_color, PDO::PARAM_STR);
                        break;
                    case 'studio_logo_color':
                        $stmt->bindValue($identifier, $this->studio_logo_color, PDO::PARAM_STR);
                        break;
                    case 'number_color':
                        $stmt->bindValue($identifier, $this->number_color, PDO::PARAM_STR);
                        break;
                    case 'pant_pattern_color':
                        $stmt->bindValue($identifier, $this->pant_pattern_color, PDO::PARAM_STR);
                        break;
                    case 'shirt_line_color':
                        $stmt->bindValue($identifier, $this->shirt_line_color, PDO::PARAM_STR);
                        break;
                    case 'pant_texture':
                        $stmt->bindValue($identifier, $this->pant_texture, PDO::PARAM_STR);
                        break;
                    case 'shoe_color':
                        $stmt->bindValue($identifier, $this->shoe_color, PDO::PARAM_STR);
                        break;
                    case 'shoe_sole_color':
                        $stmt->bindValue($identifier, $this->shoe_sole_color, PDO::PARAM_STR);
                        break;
                    case 'socks_color':
                        $stmt->bindValue($identifier, $this->socks_color, PDO::PARAM_STR);
                        break;
                    case 'shirt_texture':
                        $stmt->bindValue($identifier, $this->shirt_texture, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = JerseysTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTeamId();
                break;
            case 2:
                return $this->getShirtColor();
                break;
            case 3:
                return $this->getShirtPatternColor();
                break;
            case 4:
                return $this->getShirtSleveColor();
                break;
            case 5:
                return $this->getPantColor();
                break;
            case 6:
                return $this->getCapColor();
                break;
            case 7:
                return $this->getStudioLogoColor();
                break;
            case 8:
                return $this->getNumberColor();
                break;
            case 9:
                return $this->getPantPatternColor();
                break;
            case 10:
                return $this->getShirtLineColor();
                break;
            case 11:
                return $this->getPantTexture();
                break;
            case 12:
                return $this->getShoeColor();
                break;
            case 13:
                return $this->getShoeSoleColor();
                break;
            case 14:
                return $this->getSocksColor();
                break;
            case 15:
                return $this->getShirtTexture();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Jerseys'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Jerseys'][$this->hashCode()] = true;
        $keys = JerseysTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTeamId(),
            $keys[2] => $this->getShirtColor(),
            $keys[3] => $this->getShirtPatternColor(),
            $keys[4] => $this->getShirtSleveColor(),
            $keys[5] => $this->getPantColor(),
            $keys[6] => $this->getCapColor(),
            $keys[7] => $this->getStudioLogoColor(),
            $keys[8] => $this->getNumberColor(),
            $keys[9] => $this->getPantPatternColor(),
            $keys[10] => $this->getShirtLineColor(),
            $keys[11] => $this->getPantTexture(),
            $keys[12] => $this->getShoeColor(),
            $keys[13] => $this->getShoeSoleColor(),
            $keys[14] => $this->getSocksColor(),
            $keys[15] => $this->getShirtTexture(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Jerseys
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = JerseysTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Jerseys
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTeamId($value);
                break;
            case 2:
                $this->setShirtColor($value);
                break;
            case 3:
                $this->setShirtPatternColor($value);
                break;
            case 4:
                $this->setShirtSleveColor($value);
                break;
            case 5:
                $this->setPantColor($value);
                break;
            case 6:
                $this->setCapColor($value);
                break;
            case 7:
                $this->setStudioLogoColor($value);
                break;
            case 8:
                $this->setNumberColor($value);
                break;
            case 9:
                $this->setPantPatternColor($value);
                break;
            case 10:
                $this->setShirtLineColor($value);
                break;
            case 11:
                $this->setPantTexture($value);
                break;
            case 12:
                $this->setShoeColor($value);
                break;
            case 13:
                $this->setShoeSoleColor($value);
                break;
            case 14:
                $this->setSocksColor($value);
                break;
            case 15:
                $this->setShirtTexture($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = JerseysTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTeamId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setShirtColor($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setShirtPatternColor($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setShirtSleveColor($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPantColor($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCapColor($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setStudioLogoColor($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setNumberColor($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPantPatternColor($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setShirtLineColor($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setPantTexture($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setShoeColor($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setShoeSoleColor($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setSocksColor($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setShirtTexture($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Jerseys The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(JerseysTableMap::DATABASE_NAME);

        if ($this->isColumnModified(JerseysTableMap::COL_ID)) {
            $criteria->add(JerseysTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_TEAM_ID)) {
            $criteria->add(JerseysTableMap::COL_TEAM_ID, $this->team_id);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHIRT_COLOR, $this->shirt_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_PATTERN_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHIRT_PATTERN_COLOR, $this->shirt_pattern_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_SLEVE_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHIRT_SLEVE_COLOR, $this->shirt_sleve_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_COLOR)) {
            $criteria->add(JerseysTableMap::COL_PANT_COLOR, $this->pant_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_CAP_COLOR)) {
            $criteria->add(JerseysTableMap::COL_CAP_COLOR, $this->cap_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_STUDIO_LOGO_COLOR)) {
            $criteria->add(JerseysTableMap::COL_STUDIO_LOGO_COLOR, $this->studio_logo_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_NUMBER_COLOR)) {
            $criteria->add(JerseysTableMap::COL_NUMBER_COLOR, $this->number_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_PATTERN_COLOR)) {
            $criteria->add(JerseysTableMap::COL_PANT_PATTERN_COLOR, $this->pant_pattern_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_LINE_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHIRT_LINE_COLOR, $this->shirt_line_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_PANT_TEXTURE)) {
            $criteria->add(JerseysTableMap::COL_PANT_TEXTURE, $this->pant_texture);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHOE_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHOE_COLOR, $this->shoe_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHOE_SOLE_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SHOE_SOLE_COLOR, $this->shoe_sole_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SOCKS_COLOR)) {
            $criteria->add(JerseysTableMap::COL_SOCKS_COLOR, $this->socks_color);
        }
        if ($this->isColumnModified(JerseysTableMap::COL_SHIRT_TEXTURE)) {
            $criteria->add(JerseysTableMap::COL_SHIRT_TEXTURE, $this->shirt_texture);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildJerseysQuery::create();
        $criteria->add(JerseysTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Jerseys (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTeamId($this->getTeamId());
        $copyObj->setShirtColor($this->getShirtColor());
        $copyObj->setShirtPatternColor($this->getShirtPatternColor());
        $copyObj->setShirtSleveColor($this->getShirtSleveColor());
        $copyObj->setPantColor($this->getPantColor());
        $copyObj->setCapColor($this->getCapColor());
        $copyObj->setStudioLogoColor($this->getStudioLogoColor());
        $copyObj->setNumberColor($this->getNumberColor());
        $copyObj->setPantPatternColor($this->getPantPatternColor());
        $copyObj->setShirtLineColor($this->getShirtLineColor());
        $copyObj->setPantTexture($this->getPantTexture());
        $copyObj->setShoeColor($this->getShoeColor());
        $copyObj->setShoeSoleColor($this->getShoeSoleColor());
        $copyObj->setSocksColor($this->getSocksColor());
        $copyObj->setShirtTexture($this->getShirtTexture());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Jerseys Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->team_id = null;
        $this->shirt_color = null;
        $this->shirt_pattern_color = null;
        $this->shirt_sleve_color = null;
        $this->pant_color = null;
        $this->cap_color = null;
        $this->studio_logo_color = null;
        $this->number_color = null;
        $this->pant_pattern_color = null;
        $this->shirt_line_color = null;
        $this->pant_texture = null;
        $this->shoe_color = null;
        $this->shoe_sole_color = null;
        $this->socks_color = null;
        $this->shirt_texture = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(JerseysTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
