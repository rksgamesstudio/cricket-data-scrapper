<?php

use Base\RawStreamBowlingStats as BaseRawStreamBowlingStats;

/**
 * Skeleton subclass for representing a row from the 'raw_stream_bowling_stats' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RawStreamBowlingStats extends BaseRawStreamBowlingStats
{
    public function isValidStat() {
        $isValid = true;
        if(!is_numeric(parent::getInnings())) $isValid = false;
        if(!is_numeric(parent::getBalls())) $isValid = false;
        if(!is_numeric(parent::getRuns())) $isValid = false;
        if(!is_numeric(parent::getWickets())) $isValid = false;
        if(!is_numeric(parent::getAverage())) $isValid = false;
        if(!is_numeric(parent::getEconomy())) $isValid = false;
        if(!is_numeric(parent::getStrikeRate())) $isValid = false;

        return $isValid;
    }
}
