<?php

use Base\RawStreamBattingStats as BaseRawStreamBattingStats;

/**
 * Skeleton subclass for representing a row from the 'raw_stream_batting_stats' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RawStreamBattingStats extends BaseRawStreamBattingStats
{
    public function isValidStat() {
        $isValid = true;
        if(!is_numeric(parent::getInnings())) $isValid = false;
        if(!is_numeric(parent::getNotOuts())) $isValid = false;
        if(!is_numeric(parent::getRuns())) $isValid = false;
        if(!is_numeric(parent::getAverage())) $isValid = false;
        if(!is_numeric(parent::getStikeRate())) $isValid = false;
        if(!is_numeric(parent::getCenturies())) $isValid = false;
        if(!is_numeric(parent::getHalfCenturies())) $isValid = false;
        if(!is_numeric(parent::getFours())) $isValid = false;
        if(!is_numeric(parent::getSixes())) $isValid = false;

        return $isValid;
    }

    public function setWikiCenturyCount($value){
        $valueParts = explode('/', $value);
        parent::setCenturies($valueParts[0]);
        parent::setHalfCenturies($valueParts[1]);
    }
}
