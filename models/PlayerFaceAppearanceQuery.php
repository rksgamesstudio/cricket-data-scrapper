<?php

use Base\PlayerFaceAppearanceQuery as BasePlayerFaceAppearanceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'players_face_appearance' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PlayerFaceAppearanceQuery extends BasePlayerFaceAppearanceQuery
{

}
