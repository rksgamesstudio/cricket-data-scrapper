<?php
require "vendor/autoload.php";

$teams = TeamsQuery::create()->find();
// selectSquad($teams[20]);

foreach($teams as $team) {
    selectSquad($team);
}

function selectSquad(Teams $team) 
{
    $missingPositionIndices = array();
    $sparePlayers = array();
    $lineup = array();
    $sparePlayers = RawStreamQuery::create()
        ->filterByTeam($team->getId())
        ->filterByBattingorder(null)
        ->find()->getArrayCopy();
        
    for($i=0; $i < 11; $i++)
    {
        //check if player exists
        $suitablePlayers = RawStreamQuery::create()
            ->filterByTeam($team->getId())
            ->filterByBattingorder($i+1)
            ->orderByValue('desc')
            ->find()->getArrayCopy();

        if (!count($suitablePlayers)) {
            array_push($missingPositionIndices, $i);
        } else {
            // print_r($suitablePlayers);
            //select one player from selection
            $lineup[$i] = $suitablePlayers[0];
            $sparePlayers = array_merge($sparePlayers, array_slice($suitablePlayers, 1));
        }
    }

    if(count($missingPositionIndices)) {
        //loop through and pick remaining players
        for($i=0; $i < count($missingPositionIndices); $i++) {
            $pos = $missingPositionIndices[$i];
            $lineup[$pos] = pickSparePlayers($pos, $sparePlayers);
            $sparePlayers = removePlayerFromPool($sparePlayers, $lineup[$pos]);
        }
    }
    $lineup = array_values($lineup);

    //check if keeper is picked, if not replace one of the top order batsman and select a keeper from spares
    $hasKeeper = false;
    foreach($lineup as $selectedPlayer) {
        if($selectedPlayer->getPrimaryRole() == 'keeper') {
            $hasKeeper = true;
            break;
        }
    }

    if(!$hasKeeper) {
        echo "Replacing keeper \n";
        $elemenatedPlayer = getLowestValuedBatsman($lineup);
        $replacementPlayer = getWicketKeeper($sparePlayers);

        for($i = 0; $i<count($lineup); $i++) {
            if($lineup[$i]->getId() == $elemenatedPlayer->getId()) {
                $lineup[$i] = $replacementPlayer;
            }
        }

        $lineup = array_values($lineup);
        $sparePlayers = removePlayerFromPool($sparePlayers, $replacementPlayer);
        array_push($sparePlayers, $elemenatedPlayer);
    }

    echo "Team name " . $team->getName() . ", Plyaer count ".count($lineup)." spare players ". count($sparePlayers)."\n";
    // print_r($lineup);

    for($i=0; $i<count($lineup); $i++) {
        echo "saving order " . ($i +1 ). " name -> " .$lineup[$i] ->getName() ." \n";
        $lineup[$i]->setBattingOrder($i + 1);
        $lineup[$i]->save();
    }

    for($i=0; $i < count($sparePlayers); $i++) {
        $sparePlayers[$i]->setBattingOrder(null);
        $sparePlayers[$i]->save();
    }
}

function pickSparePlayers($index, $sparePlayers) {
    echo "picking spare at $index \n";
    $roleToPosition = array(
        'batsmen' => array('min' => 1, 'max' => 4),
        'allRounder'=>array('min' => 5, 'max' => 8),
        'bowler'=>array('min' => 9, 'max' => 11)
    );
    $position = $index + 1;
    $selectedRole = '';
    //decide suitable role for current index
    foreach($roleToPosition as $role => $indices) {
        if($indices['min']>= $position && $indices['max'] <= $position) {
            $selectedRole = $role;
            break;
        }
    }

    //select 
    $candidates = array();

    foreach($sparePlayers as $player){
        if($player->getPrimaryRole() == $role) {
            array_push($candidates, $player);
        }
    }

    //check if there are not enough batsmen or bowlers 
    if(count($candidates) == 0 && $role != 'allRounder') {
        foreach($sparePlayers as $player){
            if($player->getPrimaryRole() == 'allRounder') {
                array_push($candidates, $player);
            }
        }
    }

    //sort candidates by value
    usort($candidates, function($a, $b) {
        return $a->getValue() <=> $b->getValue();
    });
    


    return $candidates[count($candidates) - 1];
}

function removePlayerFromPool($sparePlayers, $player) {
    // print_r($sparePlayers);
    echo "Removing ".$player->getName()." from spares \n";
    for($i = 0; $i < count($sparePlayers); $i++) {
        if($sparePlayers[$i]->getId() == $player->getId()){
            unset($sparePlayers[$i]);
            break;
        }
    }

    return array_values($sparePlayers);
}

function getLowestValuedBatsman($lineup) {
    $currentBatsmen = array_slice($lineup, 0, 7);
    //sort batsmen by value
    usort($currentBatsmen, function($a, $b) {
        return $a->getValue() <=> $b->getValue();
    });


    //first batsman
    return $currentBatsmen[0];
    
}

function getWicketKeeper($sparePlayers) {
    
    $keepers = array();
    //select keeper
    foreach($sparePlayers as $player) {
        if($player->getPrimaryRole() == 'keeper')
        {
            array_push($keepers, $player);
        }
    }

    //sort keepers by value
    usort($keepers, function($a, $b) {
        return $a->getValue() <=> $b->getValue();
    });

    $replacementPlayer = $keepers[count($keepers) - 1];
    return $replacementPlayer;
}
?>