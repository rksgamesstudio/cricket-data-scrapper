<?php

require "vendor/autoload.php";
require "clear-data-tables.php";
$time_start = microtime(true); 
$teamEntries = TeamsQuery::create()->find();
foreach($teamEntries as $key => $teamEntry) {
    echo "Processing team index ".$key." -> " . $teamEntry->getName() . "\n";
    $teamParserConfig = new PlayerParserConfig;
    $teamParserConfig->url = $teamEntry->getWikiUrl();
    $teamParserConfig->tableSelector = $teamEntry->getTableSelector();
    $teamParserConfig->nameSelector = $teamEntry->getPlayerName();
    $teamParserConfig->battingSelector = $teamEntry->getPlayerBattingHand();
    $teamParserConfig->bowlingSelector = $teamEntry->getPlayerBowlingHand();
    $teamParserConfig->teamId = $teamEntry->getId();
    $insertedPlayers = PlayerParser::parsePlayers($teamParserConfig);
    echo "Total Players inserted - " . count($insertedPlayers) . "\n";
}
$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;
echo "Total Execution Time: ".$execution_time." Mins \n";
?>